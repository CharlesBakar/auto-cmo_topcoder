/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool;

import cmo.autotool.models.UserRole;
import cmo.autotool.models.dto.ServiceContext;
import cmo.autotool.models.dto.User;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Base test context.
 *
 * @author bannie2942
 * @version 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-context.xml")
public abstract class AbstractCMOTest {

    /**
     * The date format.
     */
    protected static final DateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * The jdbc template.
     */
    @Autowired
    protected JdbcTemplate template;

    /**
     * Set up the test.
     *
     * @throws Exception to Junit
     */
    @Before
    public void setup() throws Exception {
        clearDB();
        setupDB();
        // setup mock user
        User user = new User();
        user.setCorpId("tester");
        UserRole role = new UserRole();
        role.setId(1);
        role.setName("role1");
        user.setRole(role);
        ServiceContext.setUser(user);
    }

    /**
     * Clean up the test context.
     *
     * @throws Exception to Junit
     */
    @After
    public void teardown() throws Exception {
        clearDB();
    }

    /**
     * Clear DB.
     *
     * @throws Exception to Junit
     */
    private void clearDB() throws Exception {
        executeSQL("src/test/resources/clean_test.sql");
    }

    /**
     * Setup DB.
     *
     * @throws Exception to Junit
     */
    private void setupDB() throws Exception {
        executeSQL("src/test/resources/test_data.sql");
    }

    /**
     * Execute sql from file.
     *
     * @param path the file path
     * @throws Exception to Junit
     */
    private void executeSQL(String path) throws Exception {
        File file = new File(path);
        FileReader reader = new FileReader(file);
        BufferedReader bf = new BufferedReader(reader);
        String str;
        try {
            while ((str = bf.readLine()) != null) {
                if (!str.trim().isEmpty() || !str.trim().startsWith("--")) {
                    template.execute(str.replace(";", ""));
                }
            }
        } finally {
            bf.close();
            reader.close();
        }
    }
}
