/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.services.impl;

import cmo.autotool.AbstractCMOTest;
import cmo.autotool.models.CMOSetting;
import cmo.autotool.services.CMOSettingService;
import java.util.Date;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Unit tests for {@link CMOSettingService}.
 *
 * <p>Change log 1.1:
 * <ul>
 *   <li>Update test methods.</li>
 * </ul>
 * </p>
 * @author bannie2492
 * @version 1.1
 */
public class CMOSettingServiceTest extends AbstractCMOTest {

    /**
     * The business date service for testing.
     */
    @Autowired
    private CMOSettingService cmoSettingService;

    /**
     * Setting and getting CMO setting should works fine.
     * @throws Exception to JUnit
     */
    @Test
    public void testGetSetCMOSetting() throws Exception {
        String today = FORMAT.format(new Date());
        // get the default cmo setting
        CMOSetting result = cmoSettingService.getCurrentCMOSetting();
        assertNull("Failed to get cmo setting.", result.getTimestamp());
        assertEquals("Failed to get cmo setting.", 40.0d, result.getMarketValueThreshold(), 0.000001);
        // insert new cmo setting
        CMOSetting cmoSetting = new CMOSetting();
        cmoSetting.setMarketValueThreshold(2.0);
        cmoSettingService.setCMOSetting(cmoSetting);
        assertTrue("Failed to insert cmo setting.", cmoSetting.getId() > 0);
        result = cmoSettingService.getCurrentCMOSetting();
        assertEquals("Failed to insert cmo setting.", today, FORMAT.format(result.getTimestamp()));
        assertEquals("Failed to insert cmo setting.", 2.0d, result.getMarketValueThreshold(), 0.000001);
    }
}
