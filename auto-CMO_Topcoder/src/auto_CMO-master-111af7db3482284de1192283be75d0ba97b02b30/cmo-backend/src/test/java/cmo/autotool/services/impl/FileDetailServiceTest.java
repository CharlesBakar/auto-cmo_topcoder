/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.services.impl;

import cmo.autotool.AbstractCMOTest;
import cmo.autotool.exceptions.EntityNotFoundException;
import cmo.autotool.models.FileDetail;
import cmo.autotool.models.FileStatus;
import cmo.autotool.services.FileDetailService;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Unit tests for {@link FileDetailService}.
 *
 * @author bannie2942
 * @version 1.0
 */
public class FileDetailServiceTest extends AbstractCMOTest {

    /**
     * The file detail service for testing.
     */
    @Autowired
    private FileDetailService fileDetailService;

    /**
     * Create file detail should work fine.
     * @throws Exception to JUnit
     */
    @Test
    public void testCreate() throws Exception {
        Calendar c = Calendar.getInstance();
        FileDetail fileDetail = new FileDetail();
        fileDetail.setFilePath("path");
        fileDetail.setTotalRecordCount(1);
        fileDetail.setSuccessRecordCount(2);
        fileDetail.setFailureRecordCount(3);
        fileDetail.setIgnoredRecordCount(4);
        c.set(2015, 11, 20);
        fileDetail.setStartTime(c.getTime());
        c.set(2015, 11, 30);
        fileDetail.setFinishedTime(c.getTime());
        fileDetail.setFileStatus(FileStatus.SUCCESS);
        fileDetailService.create(fileDetail);
        String today = FORMAT.format(new Date());
        assertTrue("Failed to create file detail", fileDetail.getId() > 0);
        assertEquals("Failed to create file detail", "path", fileDetail.getFilePath());
        assertEquals("Failed to create file detail", 1, fileDetail.getTotalRecordCount());
        assertEquals("Failed to create file detail", 2, fileDetail.getSuccessRecordCount());
        assertEquals("Failed to create file detail", 3, fileDetail.getFailureRecordCount());
        assertEquals("Failed to create file detail", 4, fileDetail.getIgnoredRecordCount());
        assertEquals("Failed to create file detail", "2015-12-20", FORMAT.format(fileDetail.getStartTime()));
        assertEquals("Failed to create file detail", "2015-12-30", FORMAT.format(fileDetail.getFinishedTime()));
        assertEquals("Failed to create file detail", today, FORMAT.format(fileDetail.getCreatedTS()));
        assertEquals("Failed to create file detail", today, FORMAT.format(fileDetail.getUpdatedTS()));
        assertEquals("Failed to create file detail", "tester", fileDetail.getCreatedByNM());
        assertEquals("Failed to create file detail", "tester", fileDetail.getUpdatedByNM());
        assertEquals("Failed to create file detail", FileStatus.SUCCESS, fileDetail.getFileStatus());
    }

    /**
     * Update file detail should work fine
     * @throws Exception to JUnit
     */
    @Test
    public void testUpdate() throws Exception {
        Calendar c = Calendar.getInstance();
        FileDetail fileDetail = new FileDetail();
        fileDetail.setFilePath("path");
        fileDetail.setTotalRecordCount(1);
        fileDetail.setSuccessRecordCount(2);
        fileDetail.setFailureRecordCount(3);
        fileDetail.setIgnoredRecordCount(4);
        c.set(2015, 11, 20);
        fileDetail.setStartTime(c.getTime());
        c.set(2015, 11, 30);
        fileDetail.setFinishedTime(c.getTime());
        fileDetail.setFileStatus(FileStatus.FAILED);
        fileDetail.setId(5);
        fileDetailService.update(fileDetail);
        List<FileDetail> result = fileDetailService.getFileDetails();
        boolean found = false;
        String today = FORMAT.format(new Date());
        for (FileDetail file : result) {
            if (file.getId() == 5) {
                found = true;
                // found the updated file
                assertEquals("Failed to update file detail", "path", file.getFilePath());
                assertEquals("Failed to update file detail", 1, file.getTotalRecordCount());
                assertEquals("Failed to update file detail", 2, file.getSuccessRecordCount());
                assertEquals("Failed to update file detail", 3, file.getFailureRecordCount());
                assertEquals("Failed to update file detail", 4, file.getIgnoredRecordCount());
                assertEquals("Failed to update file detail", "2015-12-20", FORMAT.format(file.getStartTime()));
                assertEquals("Failed to update file detail", "2015-12-30", FORMAT.format(file.getFinishedTime()));
                assertEquals("Failed to update file detail", "2015-12-15", FORMAT.format(file.getCreatedTS()));
                assertEquals("Failed to update file detail", today, FORMAT.format(file.getUpdatedTS()));
                assertEquals("Failed to update file detail", "TEST", file.getCreatedByNM());
                assertEquals("Failed to update file detail", "tester", file.getUpdatedByNM());
                assertEquals("Failed to update file detail", FileStatus.FAILED, file.getFileStatus());
            }
        }
        if (!found) {
            fail("Failed to update file detail.");
        }
    }

    /**
     * Updating a non-exist file should return exception.
     * @throws Exception to JUnit
     */
    @Test(expected = EntityNotFoundException.class)
    public void testUpdate2() throws Exception {
        List<FileDetail> details = fileDetailService.getFileDetails();
        FileDetail detail = details.get(0);
        detail.setId(123);
        fileDetailService.update(detail);
    }

    /**
     * Get file details should return desired result.
     * @throws Exception to JUnit
     */
    @Test
    public void testGetFileDetails() throws Exception {
        List<FileDetail> result = fileDetailService.getFileDetails();
        for (FileDetail file : result) {
            // the max count is 3
            if (file.getId() < 4) {
                fail("should get the latest file details.");
            }
            long id = file.getId();
            assertEquals("Failed to get file detail", id * 4 - 3, file.getTotalRecordCount());
            assertEquals("Failed to get file detail", id * 4 - 2, file.getSuccessRecordCount());
            assertEquals("Failed to get file detail", id * 4 - 1, file.getFailureRecordCount());
            assertEquals("Failed to get file detail", id * 4, file.getIgnoredRecordCount());
            assertEquals("Failed to get file detail", "path", file.getFilePath());
            assertEquals("Failed to get file detail", FileStatus.SUCCESS, file.getFileStatus());
            assertEquals("Failed to get file detail", "2015-12-" + (id + 10), FORMAT.format(file.getStartTime()));
            assertEquals("Failed to get file detail", "2015-12-" + (id + 10), FORMAT.format(file.getFinishedTime()));
            assertEquals("Failed to get file detail", "2015-12-" + (id + 10), FORMAT.format(file.getCreatedTS()));
            assertEquals("Failed to get file detail", "2015-12-" + (id + 10), FORMAT.format(file.getUpdatedTS()));
            assertEquals("Failed to get file detail", "TEST", file.getCreatedByNM());
            assertEquals("Failed to get file detail", "TEST", file.getUpdatedByNM());
        }
    }
}
