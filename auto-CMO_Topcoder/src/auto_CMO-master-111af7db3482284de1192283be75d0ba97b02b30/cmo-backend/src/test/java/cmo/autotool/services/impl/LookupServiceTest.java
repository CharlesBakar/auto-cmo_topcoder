/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.services.impl;

import cmo.autotool.AbstractCMOTest;
import cmo.autotool.exceptions.EntityNotFoundException;
import cmo.autotool.models.SecurityStatus;
import cmo.autotool.models.UserRole;
import cmo.autotool.services.LookupService;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Unit tests for {@link LookupService}.
 *
 * <p>Change log 1.1:
 * <ul>
 *   <li>Update test methods.</li>
 * </ul>
 * </p>
 * @author bannie2492
 * @version 1.1
 */
public class LookupServiceTest extends AbstractCMOTest {

    /**
     * The lookup service for testing.
     */
    @Autowired
    private LookupService lookupService;

    /**
     * Get all security statuses should be successful.
     *
     * @throws Exception to JUnit
     */
    @Test
    public void testGetAllSecurityStatuses() throws Exception {
        List<SecurityStatus> statuses = lookupService.getAllSecurityStatuses();
        assertEquals("Failed to get security statuses", 5, statuses.size());
        List<Map<String, Object>> result = template.queryForList("select * from security_status");
        for (Map<String, Object> map : result) {
            long id = ((BigDecimal) map.get("ID")).longValue();
            String name = (String) map.get("SECURITY_STATUS_NAME");
            boolean found = false;
            for (SecurityStatus s : statuses) {
                if (s.getId() == id) {
                    assertEquals("Failed to get security statuses", name, s.getName());
                    found = true;
                    break;
                }
            }
            if (!found) {
                fail("Failed to get security statuses");
            }
        }
    }

    /**
     * Get all user roles should be successful.
     *
     * @throws Exception to JUnit
     */
    @Test
    public void testGetAllUserRoles() throws Exception {
        List<UserRole> roles = lookupService.getAllUserRoles();
        assertEquals("Failed to get user roles", 4, roles.size());
        List<Map<String, Object>> result = template.queryForList("select * from user_role");
        for (Map<String, Object> map : result) {
            long id = ((BigDecimal) map.get("ID")).longValue();
            String name = (String) map.get("NAME");
            boolean found = false;
            for (UserRole r : roles) {
                if (r.getId() == id) {
                    assertEquals("Failed to get user role", name, r.getName());
                    found = true;
                    break;
                }
            }
            if (!found) {
                fail("Failed to get user role");
            }
        }
    }

    /**
     * Add new status should be successful.
     *
     * @throws Exception to JUnit
     */
    @Test
    public void testUpdateSecurityStatuses() throws Exception {
        List<SecurityStatus> statuses = lookupService.getAllSecurityStatuses();
        SecurityStatus status = new SecurityStatus();
        status.setName("status5");
        statuses.add(status);
        lookupService.updateSecurityStatuses(statuses);
        statuses = lookupService.getAllSecurityStatuses();
        assertEquals("Failed to update security statuses", 6, statuses.size());
        boolean found = false;
        for (SecurityStatus s : statuses) {
            if (s.getName().equals("status5")) {
                found = true;
                break;
            }
        }
        if (!found) {
            fail("Failed to update security statuses");
        }
    }

    /**
     * Update existing status should be successful.
     *
     * @throws Exception to JUnit
     */
    @Test
    public void testUpdateSecurityStatuses2() throws Exception {
        List<SecurityStatus> statuses = lookupService.getAllSecurityStatuses();
        boolean set = false;
        for (SecurityStatus s : statuses) {
            if (s.getId() == 4) {
                s.setName("status5");
                set = true;
                break;
            }
        }
        if (!set) {
            fail("Failed to update security statuses.");
        }
        lookupService.updateSecurityStatuses(statuses);
        statuses = lookupService.getAllSecurityStatuses();
        assertEquals("Failed to update security statuses", 5, statuses.size());
        boolean found = false;
        for (SecurityStatus s : statuses) {
            if (s.getId() == 4) {
                found = true;
                assertEquals("Failed to update security statuses.", "status5", s.getName());
                break;
            }
        }
        if (!found) {
            fail("Failed to update security statuses.");
        }
    }

    /**
     * Delete existing statuses should be successful.
     *
     * @throws Exception to JUnit
     */
    @Test
    public void testUpdateSecurityStatuses3() throws Exception {
        SecurityStatus ss = new SecurityStatus();
        ss.setId(100);
        ss.setName("FOLLOW UP");
        ss.setDefaultStatus(true);
        lookupService.updateSecurityStatuses(Arrays.asList(ss));
        List<SecurityStatus> statuses = lookupService.getAllSecurityStatuses();
        assertEquals("Failed to update security statuses", 1, statuses.size());
    }

    /**
     * If the status can not be found, EntityNotFoundException should be thrown.
     *
     * @throws Exception to JUnit
     */
    @Test(expected = EntityNotFoundException.class)
    public void testUpdateSecurityStatuses4() throws Exception {
        SecurityStatus status = new SecurityStatus();
        status.setName("status5");
        status.setId(6);
        lookupService.updateSecurityStatuses(Arrays.asList(status));
    }

}
