/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.services.impl;

import cmo.autotool.AbstractCMOTest;
import cmo.autotool.services.AuthorizationService;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Unit tests for {@link AuthorizationService}.
 *
 * @author bannie2942
 * @version 1.0
 */
public class AuthorizationServiceTest extends AbstractCMOTest {

    /**
     * The business date service for testing.
     */
    @Autowired
    private AuthorizationService authorizationService;

    /**
     * Authorization should return expected value.
     * @throws Exception to JUnit
     */
    @Test
    public void testIsAuthorized() throws Exception {
        boolean result = authorizationService.isAuthorized("action1", 1);
        assertEquals("Failed to authorize actions.", true, result);
        result = authorizationService.isAuthorized("action1", 2);
        assertEquals("Failed to authorize actions.", false, result);
    }
}
