/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool;

import cmo.autotool.exceptions.ConfigurationException;
import org.junit.Test;

/**
 * Unit tests for {@link Helper}.
 *
 * @author bannie2942
 * @version 1.0
 */
public class HelperTest {

    /**
     * Test checkPositive for long value data.
     */
    @Test
    public void testCheckPositiveValue() {
        Helper.checkPositive(1, "name");
    }

    /**
     * Test checkPositive for long value data. The value is not positive, IAE should be thrown.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testCheckPositiveLongValueFailure() {
        Helper.checkPositive(0, "name");
    }

    /**
     * Test checkState for true value.
     */
    @Test
    public void testCheckState() {
        Helper.checkState(true, "message");
    }

    /**
     * Test checkState for false value. IAE should be thrown.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testCheckStateFailure() {
        Helper.checkState(false, "message");
    }

    /**
     * Test checkNull for non-null value.
     */
    @Test
    public void testCheckNull() {
        Helper.checkNull("aaa", "aaa");
    }

    /**
     * Test checkNull for null value. IAE should be thrown.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testCheckNullFailure() {
        Helper.checkNull(null, "null");
    }

    /**
     * Test checkNullOrEmpty for non-null value.
     */
    @Test
    public void testCheckNullorEmpty() {
        Helper.checkNullOrEmpty("aaa", "aaa");
    }

    /**
     * Test checkNullOrEmpty for null value. IAE should be thrown.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testCheckNullorEmpty2() {
        Helper.checkNullOrEmpty(null, "null");
    }

    /**
     * Test checkNullOrEmpty for empty value. IAE should be thrown.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testCheckNullorEmpty3() {
        Helper.checkNullOrEmpty(" ", "null");
    }

    /**
     * Test checkPositive for long value data.
     */
    @Test
    public void testCheckConfigPositiveValue() {
        Helper.checkConfigPositive(1, "name");
    }

    /**
     * Test checkPositive for long value data. The value is not positive,
     * ConfigurationException should be thrown.
     */
    @Test(expected = ConfigurationException.class)
    public void testCheckConfigPositiveLongValueFailure() {
        Helper.checkConfigPositive(0, "name");
    }

    /**
     * Test checkConfigState for true value.
     */
    @Test
    public void testCheckConfigState() {
        Helper.checkConfigState(true, "message");
    }

    /**
     * Test checkConfigState for false value. ConfigurationException should be thrown.
     */
    @Test(expected = ConfigurationException.class)
    public void testCheckConfigStateFailure() {
        Helper.checkConfigState(false, "message");
    }

    /**
     * Test checkConfigNotNull for non-null value.
     */
    @Test
    public void testCheckConfigNull() {
        Helper.checkConfigNotNull("aaa", "aaa");
    }

    /**
     * Test checkConfigNotNull for null value. ConfigurationException should be thrown.
     */
    @Test(expected = ConfigurationException.class)
    public void ConfigurationException() {
        Helper.checkConfigNotNull(null, "null");
    }

    /**
     * Test checkConfigNotEmpty for non-null value.
     */
    @Test
    public void testCheckConfigNullorEmpty() {
        Helper.checkConfigNotEmpty("aaa", "aaa");
    }

    /**
     * Test checkConfigNotEmpty for null value. ConfigurationException should be thrown.
     */
    @Test(expected = ConfigurationException.class)
    public void testCheckConfigNullorEmpty2() {
        Helper.checkConfigNotEmpty(null, "null");
    }

    /**
     * Test checkConfigNotEmpty for empty value. ConfigurationException should be thrown.
     */
    @Test(expected = ConfigurationException.class)
    public void testCheckConfigNullorEmpty3() {
        Helper.checkConfigNotEmpty(" ", "null");
    }
}
