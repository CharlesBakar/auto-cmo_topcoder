/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.services.impl;

import cmo.autotool.AbstractCMOTest;
import cmo.autotool.services.BusinessDateService;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Unit tests for {@link BusinessDateService}.
 *
 * @author bannie2942
 * @version 1.0
 */
public class BusinessDateServiceTest extends AbstractCMOTest {

    /**
     * The business date service for testing.
     */
    @Autowired
    private BusinessDateService businessDateService;

    /**
     * Get last business date should return the last date.
     * @throws Exception to JUnit
     */
    @Test
    public void testGetLastBusinessDate() throws Exception {
        Date date = businessDateService.getLastBusinessDate();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        assertEquals("Failed to get last business date", 2015, c.get(Calendar.YEAR));
        assertEquals("Failed to get last business date", 11, c.get(Calendar.MONTH));
        assertEquals("Failed to get last business date", 19, c.get(Calendar.DATE));
    }

    /**
     * For a range, the business date range should be return inclusively.
     * @throws Exception to JUnit
     */
    @Test
    public void testGetBusinessDates() throws Exception {
        Calendar c = Calendar.getInstance();
        c.set(2015, 11, 16, 0, 0, 0);
        c.set(Calendar.MILLISECOND, 0);
        Date startDate = c.getTime();
        c.set(2015, 11, 18);
        Date endDate = c.getTime();
        List<Date> result = businessDateService.getBusinessDates(startDate, endDate);
        assertEquals("Failed to get business date", 3, result.size());
        assertEquals("Failed to get business date", "2015-12-16", FORMAT.format(result.get(0)));
        assertEquals("Failed to get business date", "2015-12-17", FORMAT.format(result.get(1)));
        assertEquals("Failed to get business date", "2015-12-18", FORMAT.format(result.get(2)));
    }
}
