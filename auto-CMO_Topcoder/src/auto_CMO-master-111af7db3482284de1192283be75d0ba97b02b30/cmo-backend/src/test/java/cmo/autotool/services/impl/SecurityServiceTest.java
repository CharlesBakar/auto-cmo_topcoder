/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.services.impl;

import cmo.autotool.AbstractCMOTest;
import cmo.autotool.exceptions.EntityNotFoundException;
import cmo.autotool.models.Category9920;
import cmo.autotool.models.Security;
import cmo.autotool.models.SecurityDailyValue;
import cmo.autotool.models.SecurityDetail;
import cmo.autotool.models.SecurityStatus;
import cmo.autotool.models.dto.Comment;
import cmo.autotool.models.dto.SearchResult;
import cmo.autotool.models.dto.SecuritySearchCriteria;
import cmo.autotool.models.dto.ServiceContext;
import cmo.autotool.models.dto.SortType;
import cmo.autotool.services.SecurityService;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Unit tests for {@link SecurityService}.
 *
 * <p>Change log 1.1:
 * <ul>
 *   <li>Update test methods.</li>
 * </ul>
 * </p>
 * <p>Change log 1.2:
 * <ul>
 *   <li>Update test methods.</li>
 * </ul>
 * </p>
 * @author bannie2492
 * @version 1.2
 */
public class SecurityServiceTest extends AbstractCMOTest {

    /**
     * The business date service for testing.
     */
    @Autowired
    private SecurityService securityService;

    /**
     * Search with default criteria, no business date is set.
     *
     * @throws Exception to JUnit
     */
    @Test
    public void testSearch() throws Exception {
        String today = FORMAT.format(new Date());
        // search with no business date set, last business date(2015-12-19) should be used.
        SecuritySearchCriteria criteria = new SecuritySearchCriteria();
        SearchResult<Security> result = securityService.search(criteria);
        assertEquals("Failed to search securities.", 1, result.getTotalPages());
        assertEquals("Failed to search securities.", 1, result.getTotalRecords());
        Security security = result.getItems().get(0);
        assertEquals("Failed to search securities.", "001", security.getCusip());
        assertEquals("Failed to search securities.", today, FORMAT.format(security.getCreatedTS()));
        assertEquals("Failed to search securities.", "test", security.getCreatedByNM());
        assertEquals("Failed to search securities.", today, FORMAT.format(security.getUpdatedTS()));
        assertEquals("Failed to search securities.", "test", security.getUpdatedByNM());
        SecurityDailyValue sdv = security.getSecurityDailyValue();
        assertEquals("Failed to set security daily value.", 1, sdv.getSecurityId());
        assertEquals("Failed to set security daily value.", "2015-12-19", FORMAT.format(sdv.getBusinessDate()));
        assertEquals("Failed to set security daily value.", 1.5, sdv.getMarketValuePrice(), 0.000001);
        assertEquals("Failed to set security daily value.", 2, sdv.getDaysStale());
        assertEquals("Failed to set security daily value.", "STATUS2", sdv.getSecurityStatus().getName());
        assertEquals("Failed to set security daily value.", "comment5", sdv.getComment());
        assertEquals("Failed to set security daily value.", today, FORMAT.format(sdv.getCreatedTS()));
        assertEquals("Failed to set security daily value.", "test", sdv.getCreatedByNM());
        assertEquals("Failed to set security daily value.", today, FORMAT.format(sdv.getUpdatedTS()));
        assertEquals("Failed to set security daily value.", "test", sdv.getUpdatedByNM());
    }

    /**
     * Search with business date specified. The business date in the search criteria should be used.
     * @throws Exception to JUnit
     */
    @Test
    public void testSearch2() throws Exception {
        SecuritySearchCriteria criteria = new SecuritySearchCriteria();
        Calendar c = Calendar.getInstance();
        c.set(2015, 11, 15, 0, 0, 0);
        String date = FORMAT.format(c.getTime());
        c.set(Calendar.MILLISECOND, 0);
        criteria.setBusinessDate(c.getTime());
        SearchResult<Security> result = securityService.search(criteria);
        assertEquals("Failed to search securities.", 1, result.getTotalPages());
        assertEquals("Failed to search securities.", 2, result.getTotalRecords());
        result.getItems().stream().forEach(s -> {
            if (s.getId() != 1 && s.getId() != 2) {
                fail("Failed to search securities");
            }
            assertEquals("Failed to set security daily value.", date,
                    FORMAT.format(s.getSecurityDailyValue().getBusinessDate()));
            SecurityDetail d = s.getSecurityDetails().get(0);
            long id = d.getId();
            assertEquals("Failed to populate security detail.", s.getId(), d.getSecurityId());
            assertEquals("Failed to populate security detail.", "1000" + id, d.getFundNumber());
            assertEquals("Failed to populate security detail.", date, FORMAT.format(d.getTradeDate()));
            assertEquals("Failed to populate security detail.", date, FORMAT.format(d.getSettleDate()));
            assertEquals("Failed to populate security detail.", date, FORMAT.format(d.getFyrBeginDate()));
            assertEquals("Failed to populate security detail.", date, FORMAT.format(d.getFyrEndDate()));
            assertEquals("Failed to populate security detail.", Category9920.NEW_9920, d.getCategory9920());
            assertEquals("Failed to populate security detail.", "reason" + id, d.getReason9920());
            assertEquals("Failed to populate security detail.", "type" + id, d.getSecType());
            assertEquals("Failed to populate security detail.", "ind" + id, d.getIoPoInd());
            assertEquals("Failed to populate security detail.", "tax_lot_id" + id, d.getTaxLotId());
            assertEquals("Failed to populate security detail.", id, d.getCouponRate(), 0.00001);
            assertEquals("Failed to populate security detail.", id, d.getQuantityPar(), 0.00001);
            assertEquals("Failed to populate security detail.", id, d.getAmortizedCostPrice(), 0.00001);
            assertEquals("Failed to populate security detail.", id, d.getCurrentBookCost(), 0.00001);
            assertEquals("Failed to populate security detail.", id, d.getBaseOrigCost(), 0.00001);
            assertEquals("Failed to populate security detail.", id, d.getMarketValuePrice(), 0.00001);
            assertEquals("Failed to populate security detail.", id, d.getMarketValue(), 0.00001);
            assertEquals("Failed to populate security detail.", id, d.getExchangeRate(), 0.00001);
            assertEquals("Failed to populate security detail.", id, d.getYieldToMaturity(), 0.00001);
            assertEquals("Failed to populate security detail.", id, d.getYieldToWorst(), 0.00001);
            assertEquals("Failed to populate security detail.", id, d.getPurchasePrice(), 0.00001);
            assertEquals("Failed to populate security detail.", "unit" + id, d.getUnitOfCalc());
            assertEquals("Failed to populate security detail.", "USD", d.getCurrencyCode());
            assertEquals("Failed to populate security detail.", s.getCusip(), d.getCusip());
            assertEquals("Failed to populate security detail.", date, FORMAT.format(d.getBusinessDate()));
            assertEquals("Failed to populate security detail.", date, FORMAT.format(d.getCreatedTS()));
            assertEquals("Failed to populate security detail.", "TEST", d.getCreatedByNM());
            assertEquals("Failed to populate security detail.", date, FORMAT.format(d.getUpdatedTS()));
            assertEquals("Failed to populate security detail.", "TEST", d.getUpdatedByNM());
        });
    }

    /**
     * Use key words or other criteria to search data should work.
     * @throws Exception to JUnit
     */
    @Test
    public void testSearch3() throws Exception {
        SecuritySearchCriteria criteria = new SecuritySearchCriteria();
        Calendar c = Calendar.getInstance();
        c.set(2015, 11, 16, 0, 0, 0);
        c.set(Calendar.MILLISECOND, 0);
        criteria.setBusinessDate(c.getTime());
        SearchResult<Security> result = securityService.search(criteria);
        // make sure all details can be retrieved
        assertEquals("Failed to search securities.", 2, result.getItems().get(0).getSecurityDetails().size());
        // search by cusip
        c.set(2015, 11, 15, 0, 0, 0);
        criteria.setBusinessDate(c.getTime());
        criteria.setCusip("001");
        result = securityService.search(criteria);
        assertEquals("Failed to search securities.", 1, result.getItems().size());
        assertEquals("Failed to search securities.", "001", result.getItems().get(0).getCusip());
        // search by fyrBeginDate
        criteria.setCusip(null);
        criteria.setFyrBeginDate(c.getTime());
        result = securityService.search(criteria);
        assertEquals("Failed to search securities.", 2, result.getItems().size());
        // search by fyrEndDate
        criteria.setFyrBeginDate(null);
        criteria.setFyrEndDate(c.getTime());
        result = securityService.search(criteria);
        assertEquals("Failed to search securities.", 2, result.getItems().size());
        // search by days stale
        criteria.setFyrEndDate(null);
        criteria.setDaysStale(3);
        result = securityService.search(criteria);
        assertEquals("Failed to search securities.", 1, result.getItems().size());
        // search by eligibleForMonitoring
        criteria.setDaysStale(null);
        criteria.setEligibleForMonitoring(true);
        result = securityService.search(criteria);
        assertEquals("Failed to search securities.", 2, result.getItems().size());
        criteria.setEligibleForMonitoring(false);
        result = securityService.search(criteria);
        assertEquals("Failed to search securities.", 0, result.getItems().size());
        // search by key word
        criteria.setEligibleForMonitoring(null);
        criteria.setKeyword("t6");
        result = securityService.search(criteria);
        assertEquals("Failed to search securities.", 1, result.getItems().size());
        criteria.setKeyword("01");
        result = securityService.search(criteria);
        assertEquals("Failed to search securities.", 1, result.getItems().size());
        criteria.setKeyword("son");
        result = securityService.search(criteria);
        assertEquals("Failed to search securities.", 2, result.getItems().size());
        // multiple search condition
        criteria.setKeyword(null);
        criteria.setEligibleForMonitoring(true);
        criteria.setCusip("001");
        criteria.setDaysStale(2);
        criteria.setFyrBeginDate(c.getTime());
        criteria.setFyrEndDate(c.getTime());
        result = securityService.search(criteria);
        assertEquals("Failed to search securities.", 1, result.getItems().size());
    }

    /**
     * Sorting and paging should work.
     * @throws Exception to JUnit
     */
    @Test
    public void testSearch4() throws Exception {
        // check sorting and paging
        SecuritySearchCriteria criteria = new SecuritySearchCriteria();
        Calendar c = Calendar.getInstance();
        c.set(2015, 11, 15, 0, 0, 0);
        c.set(Calendar.MILLISECOND, 0);
        criteria.setBusinessDate(c.getTime());
        List<Security> result = securityService.search(criteria).getItems();
        // the result should sort by security status ascending by default
        assertEquals("Failed to search securities.", "MONITORING",
                result.get(0).getSecurityDailyValue().getSecurityStatus().getName());
        // change to descending
        criteria.setSortType(SortType.DESCENDING);
        result = securityService.search(criteria).getItems();
        assertEquals("Failed to search securities.", "STATUS2",
                result.get(0).getSecurityDailyValue().getSecurityStatus().getName());
        // sort by cusip
        criteria.setSortBy("cusip");
        result = securityService.search(criteria).getItems();
        assertEquals("Failed to search securities.", "002",
                result.get(0).getCusip());
        // sort by security status
        criteria.setSortBy("securityStatus");
        result = securityService.search(criteria).getItems();
        assertEquals("Failed to search securities.", "STATUS2",
                result.get(0).getSecurityDailyValue().getSecurityStatus().getName());
        // sort by days stale
        criteria.setSortBy("daysStale");
        result = securityService.search(criteria).getItems();
        assertEquals("Failed to search securities.", 3,
                result.get(0).getSecurityDailyValue().getDaysStale());
        // sort by comment
        criteria.setSortBy("comment");
        result = securityService.search(criteria).getItems();
        assertEquals("Failed to search securities.", "comment6",
                result.get(0).getSecurityDailyValue().getComment());
        // not valid sort column , should use security status instead
        criteria.setSortBy("not valid");
        result = securityService.search(criteria).getItems();
        assertEquals("Failed to search securities.", "STATUS2",
                result.get(0).getSecurityDailyValue().getSecurityStatus().getName());
        // page size check
        criteria.setPageSize(1);
        criteria.setPageNumber(1);
        SearchResult<Security> sr = securityService.search(criteria);
        assertEquals("Failed to search securities.", 2, sr.getTotalPages());
        assertEquals("Failed to search securities.", 2, sr.getTotalRecords());
        assertEquals("Failed to search securities.", "STATUS2",
                sr.getItems().get(0).getSecurityDailyValue().getSecurityStatus().getName());
        // page number check
        criteria.setPageNumber(2);
        sr = securityService.search(criteria);
        assertEquals("Failed to search securities.", 2, sr.getTotalPages());
        assertEquals("Failed to search securities.", 2, sr.getTotalRecords());
        assertEquals("Failed to search securities.", "MONITORING",
                sr.getItems().get(0).getSecurityDailyValue().getSecurityStatus().getName());
    }

    /**
     * Setting comment should work.
     * @throws Exception to JUnit
     */
    @Test
    public void testSetComment() throws Exception {
        securityService.setComment(5, "new_comment");
        SecuritySearchCriteria criteria = new SecuritySearchCriteria();
        List<Security> result = securityService.search(criteria).getItems();
        SecurityDailyValue sdv = result.get(0).getSecurityDailyValue();
        assertEquals("Failed to update comment.", "new_comment", sdv.getComment());
        assertEquals("Failed to update comment.", FORMAT.format(new Date()), FORMAT.format(sdv.getUpdatedTS()));
        assertEquals("Failed to update comment.", ServiceContext.getUser().getCorpId(),
                sdv.getUpdatedByNM());
    }

    /**
     * Setting status with a status which does not exist should return exception.
     * @throws Exception to JUnit
     */
    @Test(expected = EntityNotFoundException.class)
    public void testUpdateSecurityStatus3() throws Exception {
        SecurityStatus ss = new SecurityStatus();
        ss.setId(10);
        securityService.updateSecurityStatus(1, ss);
    }

    /**
     * Setting comment to a non-existed daily value should fail.
     * @throws Exception to JUnit
     */
    @Test(expected = EntityNotFoundException.class)
    public void testSetComment2() throws Exception {
        securityService.setComment(10, "new_comment");
    }

    /**
     * Setting security status to a daily value should work.
     * @throws Exception to JUnit
     */
    @Test
    public void testUpdateSecurityStatus() throws Exception {
        SecurityStatus ss = new SecurityStatus();
        ss.setId(3);
        securityService.updateSecurityStatus(5, ss);
        SecuritySearchCriteria criteria = new SecuritySearchCriteria();
        List<Security> result = securityService.search(criteria).getItems();
        assertEquals("Failed to update security status.", "STATUS3", result.get(0).getSecurityDailyValue()
                .getSecurityStatus().getName());
        SecurityDailyValue sdv = result.get(0).getSecurityDailyValue();
        assertEquals("Failed to update security status.", FORMAT.format(new Date()), FORMAT.format(sdv.getUpdatedTS()));
        assertEquals("Failed to update security status.", ServiceContext.getUser().getCorpId(),
                sdv.getUpdatedByNM());
    }

    /**
     * Setting security status to a non-existed daily value should throw exception.
     * @throws Exception to JUnit
     */
    @Test(expected = EntityNotFoundException.class)
    public void testUpdateSecurityStatus2() throws Exception {
        SecurityStatus ss = new SecurityStatus();
        ss.setId(3);
        securityService.updateSecurityStatus(9, ss);
    }
    
    /**
     * If a security is in used in the application, should return true, else it will return false.
     * @throws Exception to JUnit
     * @since 1.2
     */
    @Test
    public void testIsSecurityInUse() throws Exception {
        assertEquals("Failed to check security status in use.", true, securityService.isSecurityStatusInUse(1));
        assertEquals("Failed to check security status in use.", false, securityService.isSecurityStatusInUse(4));
    }
    
    /**
     * Getting the comments history of a security. Should get the lastest 3 comments according to the setting
     * @throws Exception to JUnit
     * @since 1.2
     */
    @Test
    public void testGetSecurityComments() throws Exception {
        Calendar c = Calendar.getInstance();
        c.set(2015, 11, 19);
        List<Comment> result = securityService.getSecurityComments(1, c.getTime());
        assertEquals("Failed to get security comments.", 3, result.size());
        assertEquals("Failed to get security comments.", "comment5", result.get(0).getCommentText());
        assertEquals("Failed to get security comments.", "comment4", result.get(1).getCommentText());
        assertEquals("Failed to get security comments.", "comment3", result.get(2).getCommentText());
    }
}
