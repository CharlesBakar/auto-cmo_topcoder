/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.services.impl;

import cmo.autotool.AbstractCMOTest;
import cmo.autotool.exceptions.ServiceException;
import cmo.autotool.models.Category9920;
import cmo.autotool.models.FileDetail;
import cmo.autotool.models.FileStatus;
import cmo.autotool.models.Security;
import cmo.autotool.models.SecurityDailyValue;
import cmo.autotool.models.SecurityDetail;
import cmo.autotool.models.dto.SecuritySearchCriteria;
import cmo.autotool.models.dto.ServiceContext;
import cmo.autotool.models.dto.User;
import cmo.autotool.services.DataImportService;
import cmo.autotool.services.FileDetailService;
import cmo.autotool.services.SecurityService;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

/**
 * Unit tests for {@link DataImportService}.
 *
 * <p>Change log 1.1:
 * <ul>
 *   <li>Update test methods.</li>
 * </ul>
 * </p>
 *
 * <p>Change log 1.2:
 * <ul>
 *   <li>Update test methods.</li>
 * </ul>
 * </p>
 * @author bannie2492
 * @version 1.2
 */
public class DataImportServiceTest extends AbstractCMOTest {

    /**
     * The test files.
     */
    private static final String[] TEST_FILES = new String[]{
        "test_file1.csv",
        "test_file2.csv",
        "test_file3.csv",
        "empty1.csv"
    };

    /**
     * The business date service for testing.
     */
    @Autowired
    private DataImportService dataImportService;

    /**
     * The security service.
     */
    @Autowired
    private SecurityService securityService;

    /**
     * The file detail service.
     */
    @Autowired
    private FileDetailService fileDetailService;

    /**
     * Set up the test context.
     * @throws Exception to JUnit
     */
    @Before
    @Override
    public void setup() throws Exception {
        super.setup();
        // copy test files to the resource folder
        for (String filename : TEST_FILES) {
            File existFile = new File("src/test/resources/" + filename);
            if (!existFile.exists()) {
                File file = new File("test_files/" + filename);
                FileChannel inputChannel = null;
                FileChannel outputChannel = null;
                try {
                    inputChannel = new FileInputStream(file).getChannel();
                    outputChannel = new FileOutputStream(existFile).getChannel();
                    outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
                } finally {
                    if (inputChannel != null) {
                        inputChannel.close();
                    }
                    if (outputChannel != null) {
                        outputChannel.close();
                    }
                }
            }
        }
    }

    /**
     * Clean up the test context.
     * @throws Exception to JUnit
     */
    @After
    @Override
    public void teardown() throws Exception {
        for (String filename : TEST_FILES) {
            File existFile = new File("src/test/resources/" + filename);
            if (existFile.exists()) {
                existFile.delete();
            }
        }
    }

    /**
     * Accuracy test of import.
     * All data should be persisted correctly.
     * @throws Exception to JUnit
     */
    @Test
    public void testImportData() throws Exception {
        Calendar c = Calendar.getInstance();
        c.set(2015, 11, 30, 0, 0, 0);
        String date = FORMAT.format(c.getTime());
        String today = FORMAT.format(new Date());
        User user = ServiceContext.getUser();
        c.set(Calendar.MILLISECOND, 0);
        dataImportService.importData("src/test/resources/test_file1.csv", c.getTime());
        SecuritySearchCriteria criteria = new SecuritySearchCriteria();
        criteria.setBusinessDate(c.getTime());
        Security s = securityService.search(criteria).getItems().get(0);
        SecurityDailyValue sdv = s.getSecurityDailyValue();
        SecurityDetail sd = s.getSecurityDetails().get(0);
        // check created security
        assertEquals("Failed to import security detail.", "003", s.getCusip());
        assertEquals("Failed to import security detail.", today, FORMAT.format(s.getCreatedTS()));
        assertEquals("Failed to import security detail.", user.getCorpId(), s.getCreatedByNM());
        assertEquals("Failed to import security detail.", today, FORMAT.format(s.getUpdatedTS()));
        assertEquals("Failed to import security detail.", user.getCorpId(), s.getUpdatedByNM());
        // check created security daily value
        assertEquals("Failed to import security detail.", s.getId(), sdv.getSecurityId());
        assertEquals("Failed to import security detail.", date, FORMAT.format(sdv.getBusinessDate()));
        assertEquals("Failed to import security detail.", sd.getMarketValuePrice(), sdv.getMarketValuePrice(), 0.00001);
        assertEquals("Failed to import security detail.", 1, sdv.getDaysStale());
        assertEquals("Failed to import security detail.", "FOLLOW UP", sdv.getSecurityStatus().getName());
        assertEquals("Failed to import security detail.", null, sdv.getComment());
        assertEquals("Failed to import security detail.", today, FORMAT.format(sdv.getCreatedTS()));
        assertEquals("Failed to import security detail.", user.getCorpId(), sdv.getCreatedByNM());
        assertEquals("Failed to import security detail.", today, FORMAT.format(sdv.getUpdatedTS()));
        assertEquals("Failed to import security detail.", user.getCorpId(), sdv.getUpdatedByNM());
        // check created security detail
        assertEquals("Failed to import security detail.", s.getId(), sd.getSecurityId());
        assertEquals("Failed to import security detail.", "1", sd.getFundNumber());
        assertEquals("Failed to import security detail.", date, FORMAT.format(sd.getBusinessDate()));
        assertEquals("Failed to import security detail.", date, FORMAT.format(sd.getSettleDate()));
        assertEquals("Failed to import security detail.", date, FORMAT.format(sd.getTradeDate()));
        assertEquals("Failed to import security detail.", date, FORMAT.format(sd.getFyrBeginDate()));
        assertEquals("Failed to import security detail.", date, FORMAT.format(sd.getFyrEndDate()));
        assertEquals("Failed to import security detail.", Category9920.NOT_9920, sd.getCategory9920());
        assertEquals("Failed to import security detail.", "A", sd.getReason9920());
        assertEquals("Failed to import security detail.", "B", sd.getSecType());
        assertEquals("Failed to import security detail.", "C", sd.getIoPoInd());
        assertEquals("Failed to import security detail.", "D", sd.getTaxLotId());
        assertEquals("Failed to import security detail.", "E", sd.getUnitOfCalc());
        assertEquals("Failed to import security detail.", "F", sd.getExchangeRateInd());
        assertEquals("Failed to import security detail.", 1.1, sd.getCouponRate(), 0.00001);
        assertEquals("Failed to import security detail.", 1.2, sd.getQuantityPar(), 0.00001);
        assertEquals("Failed to import security detail.", 1.3, sd.getAmortizedCostPrice(), 0.00001);
        assertEquals("Failed to import security detail.", 1.4, sd.getCurrentBookCost(), 0.00001);
        assertEquals("Failed to import security detail.", 1.5, sd.getBaseOrigCost(), 0.00001);
        assertEquals("Failed to import security detail.", 1.6, sd.getMarketValuePrice(), 0.00001);
        assertEquals("Failed to import security detail.", 1.7, sd.getMarketValue(), 0.00001);
        assertEquals("Failed to import security detail.", 1.8, sd.getExchangeRate(), 0.00001);
        assertEquals("Failed to import security detail.", 1.9, sd.getYieldToMaturity(), 0.00001);
        assertEquals("Failed to import security detail.", 2.1, sd.getYieldToWorst(), 0.00001);
        assertEquals("Failed to import security detail.", 2.2, sd.getPurchasePrice(), 0.00001);
        assertEquals("Failed to import security detail.", today, FORMAT.format(sd.getCreatedTS()));
        assertEquals("Failed to import security detail.", user.getCorpId(), sd.getCreatedByNM());
        assertEquals("Failed to import security detail.", today, FORMAT.format(sd.getUpdatedTS()));
        assertEquals("Failed to import security detail.", user.getCorpId(), sd.getUpdatedByNM());
        // check created file detail
        List<FileDetail> fileDetails = fileDetailService.getFileDetails();
        boolean found = false;
        for (FileDetail detail : fileDetails) {
            if (detail.getId() >= 1000) {
                found = true;
                assertEquals("Failed to save file detail.", FileStatus.SUCCESS, detail.getFileStatus());
                assertEquals("Failed to save file detail.", 1, detail.getSuccessRecordCount());
                assertEquals("Failed to save file detail.", 0, detail.getFailureRecordCount());
                assertEquals("Failed to save file detail.", 0, detail.getIgnoredRecordCount());
                assertTrue("Failed to save file detail.", detail.getFilePath().contains("src" + File.separator + "test"
                        + File.separator + "resources" + File.separator + "test_file1.csv"));
                assertEquals("Failed to save file detail.", today, FORMAT.format(detail.getCreatedTS()));
                assertEquals("Failed to save file detail.", user.getCorpId(), detail.getCreatedByNM());
                assertEquals("Failed to save file detail.", today, FORMAT.format(detail.getUpdatedTS()));
                assertEquals("Failed to save file detail.", user.getCorpId(), detail.getUpdatedByNM());
            }
        }
        if (!found) {
            fail("Failed to save file detail.");
        }

    }

    /**
     * Check days stale calculation.
     * The import file has one record with cusid 001.
     * The default market value threshold is 1.8.
     * The existing security detail of 001 has 3 consecutive days that has market value under threshold.
     * @throws Exception to JUnit
     */
    @Test
    public void testImportData2() throws Exception {
        Calendar c = Calendar.getInstance();
        c.set(2015, 11, 30, 0, 0, 0);
        c.set(Calendar.MILLISECOND, 0);
        dataImportService.importData("src/test/resources/test_file2.csv", c.getTime());
        SecuritySearchCriteria criteria = new SecuritySearchCriteria();
        criteria.setBusinessDate(c.getTime());
        Security s = securityService.search(criteria).getItems().get(0);
        SecurityDailyValue sdv = s.getSecurityDailyValue();
        assertEquals("Failed to import security detail.", 6, sdv.getDaysStale());
    }

    /**
     * File not found.
     * @throws Exception to JUnit
     */
    @Test(expected = ServiceException.class)
    public void testImportData3() throws Exception {
        dataImportService.importData("src/test/resources/test_file100", new Date());
    }

    /**
     * Contain no records.
     * Should work without issue and old data of the business date should be removed.
     * @throws Exception to JUnit
     */
    @Test(expected = IllegalArgumentException.class)
    public void testImportData4() throws Exception {
        Calendar c = Calendar.getInstance();
        c.set(2015, 11, 15, 0, 0, 0);
        c.set(Calendar.MILLISECOND, 0);
        dataImportService.importData("src/test/resources/empty1.csv", c.getTime());
    }

    /**
     * Process the file provided in the forum with 18063 records.
     * Should work with no problem.
     * @throws Exception to JUnit
     */
    @Test
    public void testImportData5() throws Exception {
        ReflectionTestUtils.setField(dataImportService, "threadCount", 10);
        testImportBigFile();
    }

    /**
     * Process the file provided in the forum with 18063 records using single thread processing.
     * Should work with no problem.
     * @throws Exception to JUnit
     */
    @Test
    public void testImportData6() throws Exception {
        ReflectionTestUtils.setField(dataImportService, "threadCount", 1);
        testImportBigFile();
    }

    /**
     * Test importing for 18063 records.
     * @throws Exception to JUnit
     */
    private void testImportBigFile() throws Exception {
        dataImportService.importData("src/test/resources/test_file3.csv", new Date());
        int result = template.queryForObject("select count(*) as count from security_detail", Integer.class);
        // 7 original + 18063 records - 3 invalid = 18067
        assertEquals("Failed to import security detail.", 18067, result);
        // check created file detail
        List<FileDetail> fileDetails = fileDetailService.getFileDetails();
        boolean found = false;
        for (FileDetail detail : fileDetails) {
            if (detail.getId() >= 1000) {
                found = true;
                assertEquals("Failed to save file detail.", FileStatus.SUCCESS, detail.getFileStatus());
                assertEquals("Failed to save file detail.", 18060, detail.getSuccessRecordCount());
                assertEquals("Failed to save file detail.", 3, detail.getFailureRecordCount());
                assertEquals("Failed to save file detail.", 0, detail.getIgnoredRecordCount());
            }
        }
        if (!found) {
            fail("Failed to save file detail.");
        }
    }
}
