/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.models.dto;

/**
 * <p>
 * The base search criteria.
 * </p>
 *
 * <p>
 * <strong>Thread Safety: </strong> This class is not thread safe because it is mutable.
 * </p>
 *
 * @author LOY, bannie2942
 * @version 1.0
 */
public abstract class BaseSearchCriteria {

    /**
     * The page size.
     */
    private int pageSize;

    /**
     * The page number.
     */
    private int pageNumber;

    /**
     * The sort by column.
     */
    private String sortBy;

    /**
     * The sort type.
     */
    private SortType sortType;

    /**
     * Default constructor.
     */
    protected BaseSearchCriteria() {
    }

    /**
     * Set pageSize.
     *
     * @param pageSize the pageSize to be set
     */
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * Get pageSize.
     *
     * @return the pageSize
     */
    public int getPageSize() {
        return pageSize;
    }

    /**
     * Set pageNumber.
     *
     * @param pageNumber the pageNumber to be set
     */
    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    /**
     * Get pageNumber.
     *
     * @return the pageNumber
     */
    public int getPageNumber() {
        return pageNumber;
    }

    /**
     * Set sortBy.
     *
     * @param sortBy the sortBy to be set
     */
    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    /**
     * Get sortBy.
     *
     * @return the sortBy
     */
    public String getSortBy() {
        return sortBy;
    }

    /**
     * Set sortType.
     *
     * @param sortType the sortType to be set
     */
    public void setSortType(SortType sortType) {
        this.sortType = sortType;
    }

    /**
     * Get sortType.
     *
     * @return the sortType
     */
    public SortType getSortType() {
        return sortType;
    }
}
