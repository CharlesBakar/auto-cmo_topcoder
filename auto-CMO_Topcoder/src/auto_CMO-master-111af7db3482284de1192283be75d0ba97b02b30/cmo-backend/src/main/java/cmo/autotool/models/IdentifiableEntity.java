/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.models;

import java.io.Serializable;

/**
 * <p>
 * The identifiable entity which holds a ID.
 * </p>
 *
 * <p>
 * <strong>Thread Safety: </strong> This class is not thread safe because it is mutable.
 * </p>
 *
 * @author LOY, bannie2492
 * @version 1.0
 */
public abstract class IdentifiableEntity implements Serializable {

    /**
     * The ID.
     */
    private long id;

    /**
     * Default constructor.
     */
    protected IdentifiableEntity() {
    }

    /**
     * Set id.
     *
     * @param id the id to be set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Get id.
     *
     * @return the id
     */
    public long getId() {
        return id;
    }
}
