/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.controllers;

import cmo.autotool.Helper;
import cmo.autotool.exceptions.ConfigurationException;
import cmo.autotool.exceptions.ServiceException;
import cmo.autotool.models.CMOSetting;
import cmo.autotool.services.CMOSettingService;
import javax.annotation.PostConstruct;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * The controller to expose global setting related APIs.
 *
 * <p>
 * <strong>Thread Safety: </strong>This class effectively thread safe when the following condition is met: this class is
 * initialized by Spring right after construction and its parameters are never changed after that. All entities passed
 * to this class are used by the caller in thread safe manner (accessed from a single thread only).
 * </p>
 *
 * <p>Change log 1.1:
 * <ul>
 *   <li>Add static logger.</li>
 * </ul>
 * </p>
 * @author LOY, bannie2492
 * @version 1.1
 */
@RestController
public class CMOSettingController extends BaseController {

    /**
     * The LOGGER.
     */
    private static final Logger LOGGER = Logger.getLogger(CMOSettingController.class.getPackage().getName());

    /**
     * The class name.
     */
    private static final String CLASS_NAME = CMOSettingController.class.getName();

    /**
     * The CMOSettingService instance.
     *
     * It is initialized with Spring setter dependency injection.
     *
     * It should not be null after initialization.
     */
    @Autowired
    private CMOSettingService cmoSettingService;

    /**
     * Default constructor.
     */
    public CMOSettingController() {
    }

    /**
     * Check the configuration.
     *
     * @throws ConfigurationException if there's any error in configuration.
     */
    @PostConstruct
    public void checkConfiguration() {
        Helper.checkConfigNotNull(cmoSettingService, "cmoSettingService");
    }

    /**
     * Get the current CMO setting.
     *
     * @return The current CMO setting.
     * @throws ServiceException populate the exception from backend
     */
    @RequestMapping(value = "CMOSettings/current",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public CMOSetting getCurrentCMOSetting() throws ServiceException {
        final String signature = CLASS_NAME + "#getCurrentCMOSetting()";

        // Log entrance
        Helper.logEntrance(LOGGER, signature, new String[] {}, new Object[] {});

        CMOSetting result = cmoSettingService.getCurrentCMOSetting();
        Helper.logExit(LOGGER, signature, result);
        return result;
    }

    /**
     * Set the CMO setting.
     *
     * @param cmoSetting the CMO setting
     * @throws ServiceException populate the exception from backend
     */
    @RequestMapping(value = "CMOSettings",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public void setCMOSetting(@RequestBody CMOSetting cmoSetting) throws ServiceException {
        final String signature = CLASS_NAME + "#setCMOSetting(CMOSetting)";

        // Log entrance
        Helper.logEntrance(LOGGER, signature, new String[] {"cmoSetting"}, new Object[] {cmoSetting});

        cmoSettingService.setCMOSetting(cmoSetting);
        Helper.logExit(LOGGER, signature, null);
    }
}

