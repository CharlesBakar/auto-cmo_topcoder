/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.models.dto;

/**
 * <p>
 * The service context that holds thread-local user.
 * </p>
 *
 * <p>
 * <strong>Thread Safety: </strong> The ThreadLocal object is used so this class is thread-safe.
 * </p>
 *
 * @author LOY, bannie2942
 * @version 1.0
 */
public class ServiceContext {

    /**
     * The thread local user.
     */
    private static final ThreadLocal<User> USER = new ThreadLocal<>();

    /**
     * The user name to be used when importing data using Quartz job.
     */
    private static final String QUARTZ_JOB_ID = "Quartz Job";

    /**
     * This is a utility class.
     */
    private ServiceContext() {
    }

    /**
     * Get the thread local user.
     *
     * @return the user
     */
    public static User getUser() {
        User result = USER.get();
        if (result == null) {
            // called from quartz job
            result = new User();
            result.setCorpId(QUARTZ_JOB_ID);
        }
        return result;
    }

    /**
     * Set the thread local user.
     *
     * @param user the user to be set
     */
    public static void setUser(User user) {
        ServiceContext.USER.set(user);
    }
}
