/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.models;

import java.util.Date;

/**
 * <p>
 * The global setting.
 * </p>
 *
 * <p>
 * <strong>Thread Safety: </strong> This class is not thread safe because it is mutable.
 * </p>
 *
 * @author LOY, bannie2492
 * @version 1.0
 */
public class CMOSetting extends IdentifiableEntity {

    /**
     * The limit value.
     */
    private double marketValueThreshold;

    /**
     * The timestamp.
     */
    private Date timestamp;

    /**
     * Default constructor.
     */
    public CMOSetting() {
    }

    /**
     * Set marketValueThreshold.
     *
     * @param marketValueThreshold the marketValueThreshold to be set
     */
    public void setMarketValueThreshold(double marketValueThreshold) {
        this.marketValueThreshold = marketValueThreshold;
    }

    /**
     * Get marketValueThreshold.
     *
     * @return the marketValueThreshold
     */
    public double getMarketValueThreshold() {
        return marketValueThreshold;
    }

    /**
     * Set timestamp.
     *
     * @param timestamp the timestamp to be set
     */
    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Get timestamp.
     *
     * @return the timestamp
     */
    public Date getTimestamp() {
        return timestamp;
    }
}
