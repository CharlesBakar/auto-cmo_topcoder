/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.models;

/**
 * <p>
 * The role.
 * </p>
 *
 * <p>
 * <strong>Thread Safety: </strong> This class is not thread safe because it is mutable.
 * </p>
 *
 * @author LOY, bannie2942
 * @version 1.0
 */
public class UserRole extends LookupEntity {
    /**
     * Default constructor.
     */
     public UserRole() {
     }
}
