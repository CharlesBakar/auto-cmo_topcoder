/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.controllers;

import cmo.autotool.Helper;
import cmo.autotool.exceptions.ConfigurationException;
import cmo.autotool.exceptions.ServiceException;
import cmo.autotool.models.FileDetail;
import cmo.autotool.services.FileDetailService;
import java.util.List;
import javax.annotation.PostConstruct;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * The controller to expose file detail related APIs.
 *
 * <p>
 * <strong>Thread Safety: </strong>This class effectively thread safe when the following condition is met: this class is
 * initialized by Spring right after construction and its parameters are never changed after that. All entities passed
 * to this class are used by the caller in thread safe manner (accessed from a single thread only).
 * </p>
 *
 * <p>Change log 1.1:
 * <ul>
 *   <li>Add static logger.</li>
 * </ul>
 * </p>
 * @author LOY, bannie2492
 * @version 1.1
 */
@RestController
public class FileDetailController extends BaseController {

    /**
     * The LOGGER.
     */
    private static final Logger LOGGER = Logger.getLogger(FileDetailController.class.getPackage().getName());

    /**
     * The class name.
     */
    private static final String CLASS_NAME = FileDetailController.class.getName();

    /**
     * The FileDetailService instance.
     *
     * It is initialized with Spring setter dependency injection.
     *
     * It should not be null after initialization.
     */
    @Autowired
    private FileDetailService fileDetailService;

    /**
     * Default constructor.
     */
    public FileDetailController() {
    }

    /**
     * Check the configuration.
     *
     * @throws ConfigurationException if there's any error in configuration.
     */
    @PostConstruct
    public void checkConfiguration() {
        Helper.checkConfigNotNull(fileDetailService, "fileDetailService");
    }

    /**
     * Get the most recent file details.
     *
     * @return The most recent file details
     * @throws ServiceException populate the exception from backend
     */
    @RequestMapping(value = "fileDetails",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<FileDetail> getFileDetails() throws ServiceException {
        final String signature = CLASS_NAME + "#getFileDetails()";

        // Log entrance
        Helper.logEntrance(LOGGER, signature, new String[] {}, new Object[] {});

        List<FileDetail> result = fileDetailService.getFileDetails();
        Helper.logExit(LOGGER, signature, result);
        return result;
    }
}

