/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.services.impl;

import cmo.autotool.Helper;
import cmo.autotool.exceptions.ConfigurationException;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * This class is a base for all MyBatis-based service implementations provided in this application. It's assumed that
 * subclasses will be initialized with Spring setter dependency injection only.
 *
 * It holds a Log4j logger to perform logging. It also holds injected mapper to access persistence.
 *
 * <p>
 * <strong>Thread Safety: </strong>This class effectively thread safe when the following condition is met: this class is
 * initialized by Spring right after construction and its parameters are never changed after that. All entities passed
 * to this class are used by the caller in thread safe manner (accessed from a single thread only).
 * </p>
 *
 * <p>Change log 1.1:
 * <ul>
 *   <li>Remove super.checkConfiguration.</li>
 * </ul>
 * </p>
 * @author LOY, bannie2942
 * @version 1.1
 */
public abstract class BasePersistenceService {
    /**
     * The CMOMapper instance to be used by subclasses for accessing entities in persistence.
     *
     * It is initialized with Spring setter dependency injection.
     *
     * It should not be null after initialization.
     */
    @Autowired
    private CMOMapper cmoMapper;

    /**
     * Default constructor.
     */
    protected BasePersistenceService() {
    }

    /**
     * Check the configuration.
     *
     * @throws ConfigurationException if there's any error in configuration.
     */
    @PostConstruct
    protected void checkConfiguration() {
        Helper.checkConfigNotNull(cmoMapper, "cmoMapper");
    }

    /**
     * Retrieves the CMOMapper instance to be used by subclasses.
     *
     * @return the CMOMapper instance to be used by subclasses
     */
    protected CMOMapper getCMOMapper() {
        return cmoMapper;
    }
}

