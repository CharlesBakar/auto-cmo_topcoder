/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.jobs;

import cmo.autotool.Helper;
import cmo.autotool.exceptions.ConfigurationException;
import cmo.autotool.exceptions.ServiceException;
import cmo.autotool.services.DataImportService;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.SortedMap;
import java.util.TreeMap;
import javax.annotation.PostConstruct;
import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.util.StringUtils;

/**
 * This class is a Quartz job for import job automatically. It assumes the input file will be uploaded to the specific
 * folder. After processed, the input file will be moved to the processed folder.
 *
 * <p>
 * <strong>Thread Safety: </strong>This class effectively thread safe when the following condition is met: this class is
 * initialized by Spring right after construction and its parameters are never changed after that. All entities passed
 * to this class are used by the caller in thread safe manner (accessed from a single thread only).
 * </p>
 *
 * <p>Change log 1.1:
 * <ul>
 *   <li>Add static logger.</li>
 * </ul>
 * </p>
 * <p>Change log 1.1:
 * <ul>
 *   <li>Update process logic accorinding to client's request.</li>
 * </ul>
 * </p>
 * @author LOY, bannie2492
 * @version 1.2
 */
public class DataImportJob extends QuartzJobBean {

    /**
     * The LOGGER.
     */
    private static final Logger LOGGER = Logger.getLogger(DataImportJob.class);

    /**
     * The class name.
     */
    private static final String CLASS_NAME = DataImportJob.class.getName();

    /**
     * The date format for business dates.
     *
     * NOTE: Not thread safe, only access SimpleDateFormat from a single thread.
     */
    private final DateFormat businessDateFormat = new SimpleDateFormat("yyyyMMdd");

    /**
     * The DataImportService instance.
     *
     * It should not be null after initialization.
     */
    @Autowired
    private DataImportService dataImportService;

    /**
     * The input file folder.
     *
     *
     * Valid value: Not null/empty.
     */
    @Value("#{jobInputFileFolder}")
    private String inputFileFolder;

    /**
     * The folder to process files.
     *
     *
     * Valid value: Not null/empty.
     * @since 1.2
     */
    @Value("#{jobFileTransferFolder}")
    private String transferFolder;

    /**
     * The folder to put files that are failed during processing.
     *
     *
     * Valid value: Not null/empty.
     * @since 1.2
     */
    @Value("#{importFailedFolder}")
    private String failedFolder;

    /**
     * Check the configuration.
     *
     * @throws ConfigurationException if there's any error in configuration.
     */
    @PostConstruct
    public void checkConfiguration() {
        Helper.checkConfigNotNull(LOGGER, "LOGGER");
        Helper.checkConfigNotNull(dataImportService, "dataImportService");
        Helper.checkConfigNotEmpty(inputFileFolder, "inputFileFolder");
        Helper.checkFolder(inputFileFolder, "inputFileFolder");
        Helper.checkFolder(transferFolder, "processsFolder");
        Helper.checkFolder(failedFolder, "failedFolder");
    }

    /**
     * Execute the job.
     * @param jec the job execution context
     * @throws JobExecutionException if any error occurs
     */
    @Override
    protected void executeInternal(JobExecutionContext jec) throws JobExecutionException {
        String signature = CLASS_NAME + "#executeInternal(JobExecutionContext)";
        Helper.logEntrance(LOGGER, signature, new String[] {"jec"}, new Object[] {Helper.NA});
        File folder = new File(inputFileFolder);
        FileFilter ff = new FileFilter() {
            public boolean accept(File f) {
                return f.isFile();
            }
        };
        File[] inputFiles = folder.listFiles(ff);
        LOGGER.info("start scanning files in folder " + inputFileFolder);
        SortedMap<Date, File> inputFileMap = new TreeMap<>();
        for (File inputFile : inputFiles) {
            Date businessDate = getDateFromFileHeader(inputFile);
            if (businessDate != null && inputFileMap.containsKey(businessDate)) {
                logInvalidFile(inputFile, "an input file with the same business date already exists");
                businessDate = null;
            }
            if (businessDate != null) {
                File transferredFile = new File(transferFolder, inputFile.getName());
                try {
                    Files.move(inputFile.toPath(), transferredFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
                    inputFileMap.put(businessDate, transferredFile);
                } catch (IOException e) {
                    LOGGER.error("Failed to move file " + inputFile.getPath() + " to " + transferredFile.getPath(), e);
                }
            } else {
                File failedFile = new File(failedFolder, inputFile.getName());
                try {
                    Files.move(inputFile.toPath(), failedFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
                } catch (IOException e) {
                    LOGGER.error("Failed to move file " + inputFile.getPath() + " to " + failedFile.getPath(), e);
                }
            }
        }
        ServiceException error = null;
        // process inputFiles in order of their business date
        for (Date date : inputFileMap.keySet()) {
            try {
                dataImportService.importData(inputFileMap.get(date).getAbsolutePath(), date);
            } catch (ServiceException e) {
                Helper.logException(LOGGER, signature, e);
                if (error == null) { // Record first exception thrown
                    error = e;
                }
            }
        }
        if (error != null) {
            throw new JobExecutionException("Failed to execute import job", error);
        }
        LOGGER.info("complete scanning files in folder " + inputFileFolder);
        Helper.logExit(LOGGER, signature, null);
    }

    /**
     * Check if a file is valid and, if so, extract the business date
     * @param inputFile the file to check
     * @return business date if files exists, isn't empty and has a valid header, otherwise null
     */
    private Date getDateFromFileHeader(File inputFile) {
        try (BufferedReader br = new BufferedReader(new FileReader(inputFile))) {
            String header = br.readLine();
            // check if header exists
            if (!StringUtils.hasText(header)) {
                logInvalidFile(inputFile, "file has no header");
                return null;
            }
            // check the header is in correct format:
            // "IMAHDR FOCP1     FSIFCMOF 20150327 20150328 045913 000000000000000"
            String[] tokens = header.split("FSIFCMOF ");
            if (tokens.length < 2) {
                logInvalidFile(inputFile, "cannot parse file header");
                return null;
            }
            // get the business date
            String[] dates = tokens[1].split(" ");
            if (dates.length == 0 || !StringUtils.hasText(dates[0])) {
                 logInvalidFile(inputFile, "cannot get the business date from header: date doesn't exist");
                 return null;
            }
            // parse the business date (format: yyyyMMdd)
            Date businessDate = businessDateFormat.parse(dates[0]);
            return businessDate;
        } catch (ParseException e) {
            logInvalidFile(inputFile, "can not get the business date from header: format is wrong", e);
            return null;
        } catch (IOException e) {
            logInvalidFile(inputFile, "can not read the input file", e);
            return null;
        }
    }

    /**
     * Log the error when encountering an invalid file.
     * @param file the input file
     * @param message the message
     * @since 1.2
     */
    private static void logInvalidFile(File file, String message) {
        logInvalidFile(file, message, null);
    }

    /**
     * Log the error when encountering an invalid file.
     * @param file the input file
     * @param message the message
     * @param e (optional) the exception to be logged
     * @since 1.2
     */
    private static void logInvalidFile(File file, String message, Exception e) {
        LOGGER.warn("invalid input file " + file.getAbsolutePath()
                + ", " + message);
        if (e != null) {
            Helper.logException(LOGGER, "DataImportJob#executeInternal", e);
        }
    }

}
