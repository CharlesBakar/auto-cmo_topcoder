/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.models;

/**
 * The lookup entity which holds a unique name.
 */
/**
 * <p>
 * The identifiable entity which holds a ID.
 * </p>
 *
 * <p>
 * <strong>Thread Safety: </strong> This class is not thread safe because it is mutable.
 * </p>
 *
 * @author LOY, bannie2492
 * @version 1.0
 */
public abstract class LookupEntity extends IdentifiableEntity {

    /**
     * The name.
     */
    private String name;

    /**
     * Default constructor.
     */
    protected LookupEntity() {
    }

    /**
     * Set name.
     *
     * @param name the name to be set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }
}
