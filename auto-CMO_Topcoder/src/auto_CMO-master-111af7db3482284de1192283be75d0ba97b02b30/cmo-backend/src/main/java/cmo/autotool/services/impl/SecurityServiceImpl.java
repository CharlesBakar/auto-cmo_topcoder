/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.services.impl;

import cmo.autotool.Helper;
import cmo.autotool.exceptions.ConfigurationException;
import cmo.autotool.exceptions.EntityNotFoundException;
import cmo.autotool.exceptions.PersistenceException;
import cmo.autotool.exceptions.ServiceException;
import cmo.autotool.models.Security;
import cmo.autotool.models.SecurityStatus;
import cmo.autotool.models.dto.Comment;
import cmo.autotool.models.dto.SearchResult;
import cmo.autotool.models.dto.SecuritySearchCriteria;
import cmo.autotool.models.dto.ServiceContext;
import cmo.autotool.services.BusinessDateService;
import cmo.autotool.services.SecurityService;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * This is the implementation of SecurityService.
 *
 * <p>
 * <strong>Thread Safety: </strong>This class effectively thread safe when the following condition is met: this class is
 * initialized by Spring right after construction and its parameters are never changed after that. All entities passed
 * to this class are used by the caller in thread safe manner (accessed from a single thread only).
 * </p>
 *
 * <p>Change log 1.1:
 * <ul>
 *   <li>Add field maxSecurityCommentCount.</li>
 *   <li>Add methods getSecurityComments and isSecurityStatusInUse.</li>
 * </ul>
 * </p>
 * <p>Change log 1.2:
 * <ul>
 *   <li>Add static logger.</li>
 * </ul>
 * </p>
 * @author LOY, bannie2492
 * @version 1.2
 */
@Service
public class SecurityServiceImpl extends BasePersistenceService implements SecurityService {

    /**
     * The logger.
     */
    private static final Logger LOGGER = Logger.getLogger(SecurityServiceImpl.class.getPackage().getName());

    /**
     * The class name.
     */
    private static final String CLASS_NAME = SecurityServiceImpl.class.getName();

    /**
     * The valid sort columns.
     */
    private static final List<String> VALID_SORT_COLUMNS
            = Arrays.asList("securityStatus", "cusip", "comment", "daysStale");

    /**
     * The BusinessDateService instance.
     *
     * It is initialized with Spring setter dependency injection.
     *
     * Valid value: Not null
     */
    @Autowired
    private BusinessDateService businessDateService;

    /**
     * The max count of the comments of the security that will be returned to the frontend.
     *
     * Valid value: Positive
     * @since 1.1
     */
    @Value("#{maxSecurityCommentCount}")
    private int maxSecurityCommentCount;

    /**
     * Empty constructor.
     */
    public SecurityServiceImpl() {
    }

    /**
     * Check the configuration.
     *
     * @throws ConfigurationException if there's any error in configuration.
     */
    @PostConstruct
    public void checkConfiguration() {
        super.checkConfiguration();
        Helper.checkConfigNotNull(businessDateService, "businessDateService");
        Helper.checkConfigPositive(maxSecurityCommentCount, "maxSecurityCommentCount");
    }

    /**
     * Search the securities. Note: if criteria.businessDate is not set, it will be set to last business date by
     * default.
     *
     * @param criteria the search criteria
     * @return the search result.
     * @throws IllegalArgumentException if criteria is null, or criteria.pageNumber &lt; 0, or (criteria.pageSize &ge; 1
     * and criteria.pageNumber &le; 0)
     * @throws PersistenceException if some error occurred while accessing the persistence.
     * @throws ServiceException if any other error occurs.
     */
    public SearchResult<Security> search(SecuritySearchCriteria criteria) throws ServiceException {
        final String signature = CLASS_NAME + "#search(SecuritySearchCriteria)";
        // Log entrance
        Helper.logEntrance(LOGGER, signature, new String[]{"criteria"}, new Object[]{criteria});
        Helper.checkNull(criteria, "criteria");
        Helper.checkState(criteria.getPageNumber() >= 0, "BE_PAGE_NUMBER_NEGATIVE");
        if (criteria.getPageNumber() >= 1) {
            Helper.checkPositive(criteria.getPageSize(), "Page size");
        }
        CMOMapper cmoMapper = getCMOMapper();
        try {
            if (!VALID_SORT_COLUMNS.contains(criteria.getSortBy())) {
                criteria.setSortBy("securityStatus");
            }
            if (criteria.getBusinessDate() == null) {
                criteria.setBusinessDate(businessDateService.getLastBusinessDate());
            }
            if (criteria.getKeyword() != null) {
                criteria.setKeyword("%" + criteria.getKeyword() + "%");
            }
            // search items
            List<Security> items = cmoMapper.searchSecurities(criteria);
            // populate security detail
            for (Security security : items) {
                security.setSecurityDetails(cmoMapper.getSecurityDetails(security.getId(), criteria.getBusinessDate()));
            }
            // build paged result
            SearchResult<Security> result = new SearchResult<>();
            result.setItems(items);
            if (criteria.getPageSize() == 0) {
                result.setTotalPages(1);
                result.setTotalRecords(items.size());
            } else {
                int totalCount = cmoMapper.countSecurities(criteria);
                // set total page
                int totalPages = (totalCount + criteria.getPageSize() - 1) / criteria.getPageSize();
                result.setTotalPages(totalPages);
                result.setTotalRecords(totalCount);
            }
            Helper.logExit(LOGGER, signature, result);
            return result;
        } catch (org.apache.ibatis.exceptions.PersistenceException e) {
            throw Helper.logException(LOGGER, signature,
                    new PersistenceException("BE_DB_ERROR", e));
        }
    }

    /**
     * Set comment to a security.
     *
     * @param securityDailyValueId the security daily value ID
     * @param comment the comment
     * @throws IllegalArgumentException if securityDailyValueId is not positive.
     * @throws EntityNotFoundException if the entity is not found in persistence.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     * @throws ServiceException if any other error occurs.
     */
    @Transactional
    public void setComment(long securityDailyValueId, String comment) throws ServiceException {
        final String signature = CLASS_NAME + "#setComment(long, String)";
        // Log entrance
        Helper.logEntrance(LOGGER, signature, new String[]{"securityDailyValueId", "comment"},
                new Object[]{securityDailyValueId, comment});

        Helper.checkPositive(securityDailyValueId, "securityDailyValueId");
        CMOMapper cmoMapper = getCMOMapper();
        try {
            int result = cmoMapper.updateComment(securityDailyValueId, comment, new Date(),
                    ServiceContext.getUser().getCorpId());
            if (result == 0) {
                throw new EntityNotFoundException("BE_DATA_LOSS",
                    Helper.logException(LOGGER, signature,
                        new EntityNotFoundException("Security daily value with id ["
                        + securityDailyValueId + "] can not be found")));
            }
            Helper.logExit(LOGGER, signature, null);
        } catch (org.apache.ibatis.exceptions.PersistenceException e) {
            throw Helper.logException(LOGGER, signature,
                    new PersistenceException("BE_DB_ERROR", e));
        }
    }

    /**
     * Update security status.
     *
     * @param securityStatus the daily value ID
     * @param securityDailyValueId the security status
     * @throws IllegalArgumentException if securityDailyValueId is not positive or securityStatus is null.
     * @throws EntityNotFoundException if the entity is not found in persistence.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     * @throws ServiceException if any other error occurs.
     */
    @Transactional
    public void updateSecurityStatus(long securityDailyValueId, SecurityStatus securityStatus) throws ServiceException {
        final String signature = CLASS_NAME + "#updateSecurityStatus(long, SecurityStatus)";
        // Log entrance
        Helper.logEntrance(LOGGER, signature, new String[]{"securityDailyValueId", "securityStatus"},
                new Object[]{securityDailyValueId, securityStatus});

        Helper.checkPositive(securityDailyValueId, "securityDailyValueId");
        Helper.checkNull(securityStatus, "securityStatus");
        CMOMapper cmoMapper = getCMOMapper();
        try {
            SecurityStatus existed = cmoMapper.getSecurityStatus(securityStatus.getId());
            if (existed == null) {
                throw new EntityNotFoundException("BE_DATA_LOSS",
                        Helper.logException(LOGGER, signature, new EntityNotFoundException("Security status with id ["
                        + securityStatus.getId() + "] can not be found")));
                
            }
            int rowsAffected = cmoMapper.updateSecurityDailyValueSecurityStatus(securityDailyValueId, securityStatus,
                    new Date(), ServiceContext.getUser().getCorpId());
            if (rowsAffected == 0) {
                throw new EntityNotFoundException("BE_DATA_LOSS",
                Helper.logException(LOGGER, signature, new EntityNotFoundException("Security daily value with id ["
                        + securityDailyValueId + "] can not be found")));
            }
            Helper.logExit(LOGGER, signature, null);
        } catch (org.apache.ibatis.exceptions.PersistenceException e) {
            throw Helper.logException(LOGGER, signature,
                    new PersistenceException("BE_DB_ERROR", e));
        }
    }

    /**
     * Get security comments.
     * @param securityId the security id
     * @param businessDate the business date
     * @return the security comments
     * @throws ServiceException if any other error occurs.
     * @throws IllegalArgumentException if securityId is not positive, businessDate is null
     */
    public List<Comment> getSecurityComments(long securityId, Date businessDate) throws ServiceException {
        final String signature = CLASS_NAME + "#getSecurityComments(long, Date)";
        // Log entrance
        Helper.logEntrance(LOGGER, signature, new String[]{"securityId", "businessDate"},
                new Object[]{securityId, businessDate});

        Helper.checkPositive(securityId, "securityId");
        Helper.checkNull(businessDate, "businessDate");
        CMOMapper cmoMapper = getCMOMapper();
        try {
            List<Comment> result = cmoMapper.getSecurityComments(securityId, businessDate, maxSecurityCommentCount);
            // filter null comments
            result = result.stream().filter(c -> c.getCommentText() != null).collect(Collectors.toList());
            Helper.logExit(LOGGER, signature, result);
            return result;
        } catch (org.apache.ibatis.exceptions.PersistenceException e) {
            throw Helper.logException(LOGGER, signature,
                    new PersistenceException("BE_DB_ERROR", e));
        }
    }

    /**
     * Check if the security status is in used.
     * @param securityStatusId the security status id
     * @return true if the security status is in used
     * @throws IllegalArgumentException if securityStatusId is not positive
     * @throws ServiceException if any other error occurs
     * @since 1.1
     */
    public boolean isSecurityStatusInUse(long securityStatusId) throws ServiceException {
        final String signature = CLASS_NAME + "#isSecurityStatusInUse(long)";
        // Log entrance
        Helper.logEntrance(LOGGER, signature, new String[]{"securityStatusId"},
                new Object[]{securityStatusId});

        Helper.checkPositive(securityStatusId, "securityStatusId");
        CMOMapper cmoMapper = getCMOMapper();
        try {
            boolean result = cmoMapper.getSecurityStatusUsedCount(securityStatusId) > 0;
            Helper.logExit(LOGGER, signature, result);
            return result;
        } catch (org.apache.ibatis.exceptions.PersistenceException e) {
            throw Helper.logException(LOGGER, signature,
                    new PersistenceException("BE_DB_ERROR", e));
        }
    }
}
