/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.controllers;

import cmo.autotool.Helper;
import cmo.autotool.exceptions.ConfigurationException;
import cmo.autotool.exceptions.ServiceException;
import cmo.autotool.services.BusinessDateService;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * The controller to expose business date related APIs.
 *
 * <p>
 * <strong>Thread Safety: </strong>This class effectively thread safe when the following condition is met: this class is
 * initialized by Spring right after construction and its parameters are never changed after that. All entities passed
 * to this class are used by the caller in thread safe manner (accessed from a single thread only).
 * </p>
 *
 * <p>Change log 1.1:
 * <ul>
 *   <li>Add static logger.</li>
 * </ul>
 * </p>
 * @author LOY, bannie2492
 * @version 1.1
 */
@RestController
public class BusinessDateController extends BaseController {

    /**
     * The LOGGER.
     */
    private static final Logger LOGGER = Logger.getLogger(BusinessDateController.class.getPackage().getName());

    /**
     * The class name.
     */
    private static final String CLASS_NAME = BusinessDateController.class.getName();

    /**
     * The BusinessDateService instance.
     *
     * It is initialized with Spring setter dependency injection.
     *
     * It should not be null after initialization.
     */
    @Autowired
    private BusinessDateService businessDateService;

    /**
     * Default constructor.
     */
    public BusinessDateController() {
    }

    /**
     * Check the configuration.
     *
     * @throws ConfigurationException if there's any error in configuration.
     */
    @PostConstruct
    public void checkConfiguration() {
        Helper.checkConfigNotNull(businessDateService, "businessDateService");
    }

    /**
     * Get last business date.
     *
     * @return The last business date.
     * @throws ServiceException populate the exception from backend
     */
    @RequestMapping(value = "businessDates/last",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Date getLastBusinessDate() throws ServiceException {
        final String signature = CLASS_NAME + "#getLastBusinessDate()";

        // Log entrance
        Helper.logEntrance(LOGGER, signature, new String[] {}, new Object[] {});
        Date result = businessDateService.getLastBusinessDate();
        Helper.logExit(LOGGER, signature, result);
        return result;
    }

    /**
     * Get the business dates between the given date range.
     *
     * @param startDate the start date
     * @param endDate the end date
     * @return The business dates between the given date range.
     * @throws ServiceException populate the exception from backend
     */
    @RequestMapping(value = "businessDates",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Date> getBusinessDates(@RequestParam Date startDate, @RequestParam Date endDate)
            throws ServiceException {
        final String signature = CLASS_NAME + "#getBusinessDates(Date)";

        // Log entrance
        Helper.logEntrance(LOGGER, signature, new String[] {"startDate"}, new Object[] {startDate});

        List<Date> result = businessDateService.getBusinessDates(startDate, endDate);
        Helper.logExit(LOGGER, signature, result);
        return result;
    }
}

