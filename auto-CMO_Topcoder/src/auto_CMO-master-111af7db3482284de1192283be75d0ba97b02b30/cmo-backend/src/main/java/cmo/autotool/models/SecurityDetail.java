/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.models;

import java.util.Date;

/**
 * <p>
 * The security detail.
 * </p>
 *
 * <p>
 * <strong>Thread Safety: </strong> This class is not thread safe because it is mutable.
 * </p>
 *
 * <p>Change log 1.1:
 * <ul>
 *   <li>Add new fields.</li>
 * </ul>
 * </p>
 * @author LOY, bannie2492
 * @version 1.1
 */
public class SecurityDetail extends AuditableEntity {

    /**
     * The security ID.
     */
    private long securityId;

    /**
     * The fund number.
     */
    private String fundNumber;

    /**
     * The trade date.
     */
    private Date tradeDate;

    /**
     * The settle date.
     */
    private Date settleDate;

    /**
     * The FYR begin date.
     */
    private Date fyrBeginDate;

    /**
     * The FYR end date.
     */
    private Date fyrEndDate;

    /**
     * The 99-20 category.
     */
    private Category9920 category9920;

    /**
     * The 99-20 reason.
     */
    private String reason9920;

    /**
     * The sec type.
     */
    private String secType;

    /**
     * The IO PO Ind.
     */
    private String ioPoInd;

    /**
     * The tax LOT ID.
     */
    private String taxLotId;

    /**
     * The coupon rate.
     */
    private double couponRate;

    /**
     * The quantity par.
     */
    private double quantityPar;

    /**
     * The amortized cost price.
     */
    private double amortizedCostPrice;

    /**
     * The current book cost.
     */
    private double currentBookCost;

    /**
     * The base orig cost.
     */
    private double baseOrigCost;

    /**
     * The market value price.
     */
    private double marketValuePrice;

    /**
     * The market value.
     */
    private double marketValue;

    /**
     * The unit of calculation.
     */
    private String unitOfCalc;

    /**
     * The currency code.
     */
    private String currencyCode;

    /**
     * The exchange rate.
     */
    private double exchangeRate;

    /**
     * The exchange rate index.
     */
    private String exchangeRateInd;

    /**
     * The yield to maturity.
     */
    private double yieldToMaturity;

    /**
     * The yield to worst.
     */
    private double yieldToWorst;

    /**
     * The purchase price.
     */
    private double purchasePrice;

    /**
     * The business date.
     */
    private Date businessDate;

    /**
     * The CUSIP.
     */
    private String cusip;

    /**
     * The maturity date.
     *
     * @since 1.1
     */
    private Date maturityDate;

    /**
     * The second description.
     *
     * @since 1.1
     */
    private String secDesc;

    /**
     * The redemption value.
     *
     * @since 1.1
     */
    private String redemptionValue;

    /**
     * The TAX LOT L/S Ind.
     *
     * @since 1.1
     */
    private String taxLotLsInd;

    /**
     * The S & P rating.
     *
     * @since 1.1
     */
    private String sandpRating;

    /**
     * The fitch rating.
     *
     * @since 1.1
     */
    private String fitchRating;

    /**
     * The moody rating.
     *
     * @since 1.1
     */
    private String moodyRating;

    /**
     * The user rating.
     *
     * @since 1.1
     */
    private String userRating;

    /**
     * The issue length.
     *
     * @since 1.1
     */
    private String issueLength;

    /**
     * The effect maturity date.
     *
     * @since 1.1
     */
    private Date effectMaturityDate;

    /**
     * The prospect method.
     *
     * @since 1.1
     */
    private String prospectMethod;

    /**
     * Default constructor.
     */
    public SecurityDetail() {
    }

    /**
     * Set securityId.
     *
     * @param securityId the securityId to be set
     */
    public void setSecurityId(long securityId) {
        this.securityId = securityId;
    }

    /**
     * Get securityId.
     *
     * @return the securityId
     */
    public long getSecurityId() {
        return securityId;
    }

    /**
     * Set fundNumber.
     *
     * @param fundNumber the fundNumber to be set
     */
    public void setFundNumber(String fundNumber) {
        this.fundNumber = fundNumber;
    }

    /**
     * Get fundNumber.
     *
     * @return the fundNumber
     */
    public String getFundNumber() {
        return fundNumber;
    }

    /**
     * Set tradeDate.
     *
     * @param tradeDate the tradeDate to be set
     */
    public void setTradeDate(Date tradeDate) {
        this.tradeDate = tradeDate;
    }

    /**
     * Get tradeDate.
     *
     * @return the tradeDate
     */
    public Date getTradeDate() {
        return tradeDate;
    }

    /**
     * Set settleDate.
     *
     * @param settleDate the settleDate to be set
     */
    public void setSettleDate(Date settleDate) {
        this.settleDate = settleDate;
    }

    /**
     * Get settleDate.
     *
     * @return the settleDate
     */
    public Date getSettleDate() {
        return settleDate;
    }

    /**
     * Set fyrBeginDate.
     *
     * @param fyrBeginDate the fyrBeginDate to be set
     */
    public void setFyrBeginDate(Date fyrBeginDate) {
        this.fyrBeginDate = fyrBeginDate;
    }

    /**
     * Get fyrBeginDate.
     *
     * @return the fyrBeginDate
     */
    public Date getFyrBeginDate() {
        return fyrBeginDate;
    }

    /**
     * Set fyrEndDate.
     *
     * @param fyrEndDate the fyrEndDate to be set
     */
    public void setFyrEndDate(Date fyrEndDate) {
        this.fyrEndDate = fyrEndDate;
    }

    /**
     * Get fyrEndDate.
     *
     * @return the fyrEndDate
     */
    public Date getFyrEndDate() {
        return fyrEndDate;
    }

    /**
     * Set category9920.
     *
     * @param category9920 the category9920 to be set
     */
    public void setCategory9920(Category9920 category9920) {
        this.category9920 = category9920;
    }

    /**
     * Get category9920.
     *
     * @return the category9920
     */
    public Category9920 getCategory9920() {
        return category9920;
    }

    /**
     * Set reason9920.
     *
     * @param reason9920 the reason9920 to be set
     */
    public void setReason9920(String reason9920) {
        this.reason9920 = reason9920;
    }

    /**
     * Get reason9920.
     *
     * @return the reason9920
     */
    public String getReason9920() {
        return reason9920;
    }

    /**
     * Set secType.
     *
     * @param secType the secType to be set
     */
    public void setSecType(String secType) {
        this.secType = secType;
    }

    /**
     * Get secType.
     *
     * @return the secType
     */
    public String getSecType() {
        return secType;
    }

    /**
     * Set ioPoInd.
     *
     * @param ioPoInd the ioPoInd to be set
     */
    public void setIoPoInd(String ioPoInd) {
        this.ioPoInd = ioPoInd;
    }

    /**
     * Get ioPoInd.
     *
     * @return the ioPoInd
     */
    public String getIoPoInd() {
        return ioPoInd;
    }

    /**
     * Set taxLotId.
     *
     * @param taxLotId the taxLotId to be set
     */
    public void setTaxLotId(String taxLotId) {
        this.taxLotId = taxLotId;
    }

    /**
     * Get taxLotId.
     *
     * @return the taxLotId
     */
    public String getTaxLotId() {
        return taxLotId;
    }

    /**
     * Set couponRate.
     *
     * @param couponRate the couponRate to be set
     */
    public void setCouponRate(double couponRate) {
        this.couponRate = couponRate;
    }

    /**
     * Get couponRate.
     *
     * @return the couponRate
     */
    public double getCouponRate() {
        return couponRate;
    }

    /**
     * Set quantityPar.
     *
     * @param quantityPar the quantityPar to be set
     */
    public void setQuantityPar(double quantityPar) {
        this.quantityPar = quantityPar;
    }

    /**
     * Get quantityPar.
     *
     * @return the quantityPar
     */
    public double getQuantityPar() {
        return quantityPar;
    }

    /**
     * Set amortizedCostPrice.
     *
     * @param amortizedCostPrice the amortizedCostPrice to be set
     */
    public void setAmortizedCostPrice(double amortizedCostPrice) {
        this.amortizedCostPrice = amortizedCostPrice;
    }

    /**
     * Get amortizedCostPrice.
     *
     * @return the amortizedCostPrice
     */
    public double getAmortizedCostPrice() {
        return amortizedCostPrice;
    }

    /**
     * Set currentBookCost.
     *
     * @param currentBookCost the currentBookCost to be set
     */
    public void setCurrentBookCost(double currentBookCost) {
        this.currentBookCost = currentBookCost;
    }

    /**
     * Get currentBookCost.
     *
     * @return the currentBookCost
     */
    public double getCurrentBookCost() {
        return currentBookCost;
    }

    /**
     * Set baseOrigCost.
     *
     * @param baseOrigCost the baseOrigCost to be set
     */
    public void setBaseOrigCost(double baseOrigCost) {
        this.baseOrigCost = baseOrigCost;
    }

    /**
     * Get baseOrigCost.
     *
     * @return the baseOrigCost
     */
    public double getBaseOrigCost() {
        return baseOrigCost;
    }

    /**
     * Set marketValuePrice.
     *
     * @param marketValuePrice the marketValuePrice to be set
     */
    public void setMarketValuePrice(double marketValuePrice) {
        this.marketValuePrice = marketValuePrice;
    }

    /**
     * Get marketValuePrice.
     *
     * @return the marketValuePrice
     */
    public double getMarketValuePrice() {
        return marketValuePrice;
    }

    /**
     * Set marketValue.
     *
     * @param marketValue the marketValue to be set
     */
    public void setMarketValue(double marketValue) {
        this.marketValue = marketValue;
    }

    /**
     * Get marketValue.
     *
     * @return the marketValue
     */
    public double getMarketValue() {
        return marketValue;
    }

    /**
     * Set unitOfCalc.
     *
     * @param unitOfCalc the unitOfCalc to be set
     */
    public void setUnitOfCalc(String unitOfCalc) {
        this.unitOfCalc = unitOfCalc;
    }

    /**
     * Get unitOfCalc.
     *
     * @return the unitOfCalc
     */
    public String getUnitOfCalc() {
        return unitOfCalc;
    }

    /**
     * Set currencyCode.
     *
     * @param currencyCode the currencyCode to be set
     */
    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    /**
     * Get currencyCode.
     *
     * @return the currencyCode
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Set exchangeRate.
     *
     * @param exchangeRate the exchangeRate to be set
     */
    public void setExchangeRate(double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    /**
     * Get exchangeRate.
     *
     * @return the exchangeRate
     */
    public double getExchangeRate() {
        return exchangeRate;
    }

    /**
     * Set exchangeRateInd.
     *
     * @param exchangeRateInd the exchangeRateInd to be set
     */
    public void setExchangeRateInd(String exchangeRateInd) {
        this.exchangeRateInd = exchangeRateInd;
    }

    /**
     * Get exchangeRateInd.
     *
     * @return the exchangeRateInd
     */
    public String getExchangeRateInd() {
        return exchangeRateInd;
    }

    /**
     * Set yieldToMaturity.
     *
     * @param yieldToMaturity the yieldToMaturity to be set
     */
    public void setYieldToMaturity(double yieldToMaturity) {
        this.yieldToMaturity = yieldToMaturity;
    }

    /**
     * Get yieldToMaturity.
     *
     * @return the yieldToMaturity
     */
    public double getYieldToMaturity() {
        return yieldToMaturity;
    }

    /**
     * Set yieldToWorst.
     *
     * @param yieldToWorst the yieldToWorst to be set
     */
    public void setYieldToWorst(double yieldToWorst) {
        this.yieldToWorst = yieldToWorst;
    }

    /**
     * Get yieldToWorst.
     *
     * @return the yieldToWorst
     */
    public double getYieldToWorst() {
        return yieldToWorst;
    }

    /**
     * Set purchasePrice.
     *
     * @param purchasePrice the purchasePrice to be set
     */
    public void setPurchasePrice(double purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    /**
     * Get purchasePrice.
     *
     * @return the purchasePrice
     */
    public double getPurchasePrice() {
        return purchasePrice;
    }

    /**
     * Set businessDate.
     *
     * @param businessDate the businessDate to be set
     */
    public void setBusinessDate(Date businessDate) {
        this.businessDate = businessDate;
    }

    /**
     * Get businessDate.
     *
     * @return the businessDate
     */
    public Date getBusinessDate() {
        return businessDate;
    }

    /**
     * Set cusip.
     *
     * @param cusip the cusip to be set
     */
    public void setCusip(String cusip) {
        this.cusip = cusip;
    }

    /**
     * Get cusip.
     *
     * @return the cusip
     */
    public String getCusip() {
        return cusip;
    }

    /**
     * Set maturityDate.
     *
     * @param maturityDate the maturityDate to be set
     * @since 1.1
     */
    public void setMaturityDate(Date maturityDate) {
        this.maturityDate = maturityDate;
    }

    /**
     * Get maturityDate.
     *
     * @return the maturityDate
     * @since 1.1
     */
    public Date getMaturityDate() {
        return maturityDate;
    }

    /**
     * Set redemptionValue.
     *
     * @param redemptionValue the redemptionValue to be set
     * @since 1.1
     */
    public void setRedemptionValue(String redemptionValue) {
        this.redemptionValue = redemptionValue;
    }

    /**
     * Get redemptionValue.
     *
     * @return the redemptionValue
     * @since 1.1
     */
    public String getRedemptionValue() {
        return redemptionValue;
    }

    /**
     * Set taxLotLsInd.
     *
     * @param taxLotLsInd the taxLotLsInd to be set
     * @since 1.1
     */
    public void setTaxLotLsInd(String taxLotLsInd) {
        this.taxLotLsInd = taxLotLsInd;
    }

    /**
     * Get taxLotLsInd.
     *
     * @return the taxLotLsInd
     * @since 1.1
     */
    public String getTaxLotLsInd() {
        return taxLotLsInd;
    }

    /**
     * Set fitchRating.
     *
     * @param fitchRating the fitchRating to be set
     * @since 1.1
     */
    public void setFitchRating(String fitchRating) {
        this.fitchRating = fitchRating;
    }

    /**
     * Get fitchRating.
     *
     * @return the fitchRating
     * @since 1.1
     */
    public String getFitchRating() {
        return fitchRating;
    }

    /**
     * Set moodyRating.
     *
     * @param moodyRating the moodyRating to be set
     * @since 1.1
     */
    public void setMoodyRating(String moodyRating) {
        this.moodyRating = moodyRating;
    }

    /**
     * Get moodyRating.
     *
     * @return the moodyRating
     * @since 1.1
     */
    public String getMoodyRating() {
        return moodyRating;
    }

    /**
     * Set userRating.
     *
     * @param userRating the userRating to be set
     * @since 1.1
     */
    public void setUserRating(String userRating) {
        this.userRating = userRating;
    }

    /**
     * Get userRating.
     *
     * @return the userRating
     * @since 1.1
     */
    public String getUserRating() {
        return userRating;
    }

    /**
     * Set issueLength.
     *
     * @param issueLength the issueLength to be set
     * @since 1.1
     */
    public void setIssueLength(String issueLength) {
        this.issueLength = issueLength;
    }

    /**
     * Get issueLength.
     *
     * @return the issueLength
     * @since 1.1
     */
    public String getIssueLength() {
        return issueLength;
    }

    /**
     * Set effectMaturityDate.
     *
     * @param effectMaturityDate the effectMaturityDate to be set
     * @since 1.1
     */
    public void setEffectMaturityDate(Date effectMaturityDate) {
        this.effectMaturityDate = effectMaturityDate;
    }

    /**
     * Get effectMaturityDate.
     *
     * @return the effectMaturityDate
     * @since 1.1
     */
    public Date getEffectMaturityDate() {
        return effectMaturityDate;
    }

    /**
     * Set prospectMethod.
     *
     * @param prospectMethod the prospectMethod to be set
     * @since 1.1
     */
    public void setProspectMethod(String prospectMethod) {
        this.prospectMethod = prospectMethod;
    }

    /**
     * Get prospectMethod.
     *
     * @return the prospectMethod
     * @since 1.1
     */
    public String getProspectMethod() {
        return prospectMethod;
    }

    /**
     * Get secDesc.
     *
     * @return the secDesc
     * @since 1.1
     */
    public String getSecDesc() {
        return secDesc;
    }

    /**
     * Set secDesc.
     *
     * @param secDesc the secDesc to be set
     * @since 1.1
     */
    public void setSecDesc(String secDesc) {
        this.secDesc = secDesc;
    }

    /**
     * Get sandpRating.
     *
     * @return the sandpRating
     * @since 1.1
     */
    public String getSandpRating() {
        return sandpRating;
    }

    /**
     * Set sandpRating.
     *
     * @param sandpRating the sandpRating to be set
     * @since 1.1
     */
    public void setSandpRating(String sandpRating) {
        this.sandpRating = sandpRating;
    }

}
