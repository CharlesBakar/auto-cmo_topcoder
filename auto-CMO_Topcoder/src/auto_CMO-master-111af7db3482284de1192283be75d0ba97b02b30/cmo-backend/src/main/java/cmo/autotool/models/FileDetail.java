/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.models;

import java.util.Date;

/**
 * <p>
 * The file detail.
 * </p>
 *
 * <p>
 * <strong>Thread Safety: </strong> This class is not thread safe because it is mutable.
 * </p>
 *
 * @author LOY, bannie2492
 * @version 1.0
 */
public class FileDetail extends AuditableEntity {

    /**
     * The file path.
     */
    private String filePath;

    /**
     * The total record count.
     */
    private int totalRecordCount;

    /**
     * The success record count.
     */
    private int successRecordCount;

    /**
     * The failure record count.
     */
    private int failureRecordCount;

    /**
     * The ignored record count.
     */
    private int ignoredRecordCount;

    /**
     * The start time.
     */
    private Date startTime;

    /**
     * The finished time.
     */
    private Date finishedTime;

    /**
     * The file status.
     */
    private FileStatus fileStatus;

    /**
     * Default constructor.
     */
    public FileDetail() {
    }

    /**
     * Set filePath.
     *
     * @param filePath the filePath to be set
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * Get filePath.
     *
     * @return the filePath
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * Set totalRecordCount.
     *
     * @param totalRecordCount the totalRecordCount to be set
     */
    public void setTotalRecordCount(int totalRecordCount) {
        this.totalRecordCount = totalRecordCount;
    }

    /**
     * Get totalRecordCount.
     *
     * @return the totalRecordCount
     */
    public int getTotalRecordCount() {
        return totalRecordCount;
    }

    /**
     * Set successRecordCount.
     *
     * @param successRecordCount the successRecordCount to be set
     */
    public void setSuccessRecordCount(int successRecordCount) {
        this.successRecordCount = successRecordCount;
    }

    /**
     * Get successRecordCount.
     *
     * @return the successRecordCount
     */
    public int getSuccessRecordCount() {
        return successRecordCount;
    }

    /**
     * Set failureRecordCount.
     *
     * @param failureRecordCount the failureRecordCount to be set
     */
    public void setFailureRecordCount(int failureRecordCount) {
        this.failureRecordCount = failureRecordCount;
    }

    /**
     * Get failureRecordCount.
     *
     * @return the failureRecordCount
     */
    public int getFailureRecordCount() {
        return failureRecordCount;
    }

    /**
     * Set ignoredRecordCount.
     *
     * @param ignoredRecordCount the ignoredRecordCount to be set
     */
    public void setIgnoredRecordCount(int ignoredRecordCount) {
        this.ignoredRecordCount = ignoredRecordCount;
    }

    /**
     * Get ignoredRecordCount.
     *
     * @return the ignoredRecordCount
     */
    public int getIgnoredRecordCount() {
        return ignoredRecordCount;
    }

    /**
     * Set startTime.
     *
     * @param startTime the startTime to be set
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * Get startTime.
     *
     * @return the startTime
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * Set finishedTime.
     *
     * @param finishedTime the finishedTime to be set
     */
    public void setFinishedTime(Date finishedTime) {
        this.finishedTime = finishedTime;
    }

    /**
     * Get finishedTime.
     *
     * @return the finishedTime
     */
    public Date getFinishedTime() {
        return finishedTime;
    }

    /**
     * Set fileStatus.
     *
     * @param fileStatus the fileStatus to be set
     */
    public void setFileStatus(FileStatus fileStatus) {
        this.fileStatus = fileStatus;
    }

    /**
     * Get fileStatus.
     *
     * @return the fileStatus
     */
    public FileStatus getFileStatus() {
        return fileStatus;
    }
}
