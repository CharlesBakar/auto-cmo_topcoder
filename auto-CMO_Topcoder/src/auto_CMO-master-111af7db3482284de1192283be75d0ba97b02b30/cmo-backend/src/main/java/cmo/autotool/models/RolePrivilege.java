/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.models;

/**
 * <p>
 * The action permission.
 * </p>
 *
 * <p>
 * <strong>Thread Safety: </strong> This class is not thread safe because it is mutable.
 * </p>
 *
 * @author LOY, bannie2492
 * @version 1.0
 */
public class RolePrivilege extends IdentifiableEntity {

    /**
     * The action.
     */
    private String action;

    /**
     * The role.
     */
    private UserRole role;

    /**
     * Default constructor.
     */
    public RolePrivilege() {
    }

    /**
     * Set action.
     *
     * @param action the action to be set
     */
    public void setAction(String action) {
        this.action = action;
    }

    /**
     * Get action.
     *
     * @return the action
     */
    public String getAction() {
        return action;
    }

    /**
     * Set role.
     *
     * @param role the role to be set
     */
    public void setRole(UserRole role) {
        this.role = role;
    }

    /**
     * Get role.
     *
     * @return the role
     */
    public UserRole getRole() {
        return role;
    }
}
