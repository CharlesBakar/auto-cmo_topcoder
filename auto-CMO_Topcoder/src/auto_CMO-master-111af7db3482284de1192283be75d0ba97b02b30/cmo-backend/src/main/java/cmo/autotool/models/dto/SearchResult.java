/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.models.dto;

import java.util.List;

/**
 * <p>
 * The search result.
 * </p>
 *
 * <p>
 * <strong>Thread Safety: </strong> This class is not thread safe because it is mutable.
 * </p>
 *
 * @param <T> the generic type of the item in the search result
 * @author LOY, bannie2942
 * @version 1.0
 */
public class SearchResult<T> {

    /**
     * The total records.
     */
    private int totalRecords;

    /**
     * The total pages.
     */
    private int totalPages;

    /**
     * The items.
     */
    private List<T> items;

    /**
     * Default constructor.
     */
    public SearchResult() {
    }

    /**
     * Get totalRecords.
     *
     * @return the totalRecords
     */
    public int getTotalRecords() {
        return totalRecords;
    }

    /**
     * Set the totalRecords.
     *
     * @param totalRecords the totalRecords to be set
     */
    public void setTotalRecords(int totalRecords) {
        this.totalRecords = totalRecords;
    }

    /**
     * Get totalPages.
     *
     * @return the totalPages
     */
    public int getTotalPages() {
        return totalPages;
    }

    /**
     * Set the totalPages.
     *
     * @param totalPages the totalPages to be set
     */
    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    /**
     * Get items.
     *
     * @return the items
     */
    public List<T> getItems() {
        return items;
    }

    /**
     * Set the items.
     *
     * @param items the items to be set
     */
    public void setItems(List<T> items) {
        this.items = items;
    }
}
