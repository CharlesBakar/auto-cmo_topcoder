/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.services;

import cmo.autotool.exceptions.PersistenceException;
import cmo.autotool.exceptions.ServiceException;
import cmo.autotool.models.CMOSetting;

/**
 * This service provides the operations to manage global setting.
 *
 * <p>
 * <strong>Thread Safety: </strong>Implementations of this interface should be effectively thread safe when entities
 * passed to them are used by the caller in thread safe manner.
 * </p>
 *
 * @author LOY, bannie2942
 * @version 1.0
 */
public interface CMOSettingService {

    /**
     * Get current CMO setting.
     *
     * @return The current CMO setting.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     * @throws ServiceException if any other error occurs.
     */
    public CMOSetting getCurrentCMOSetting() throws ServiceException;

    /**
     * Set CMO setting.
     *
     * @param cmoSetting the CMO setting
     * @throws PersistenceException if some error occurred while accessing the persistence.
     * @throws ServiceException if any other error occurs.
     */
    public void setCMOSetting(CMOSetting cmoSetting) throws ServiceException;
}
