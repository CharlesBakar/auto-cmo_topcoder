/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.models;

/**
 * <p>
 * The security status.
 * </p>
 *
 * <p>
 * <strong>Thread Safety: </strong> This class is not thread safe because it is mutable.
 * </p>
 *
 * <p>Change log 1.1:
 * <ul>
 *   <li>Add field defaultStatus.</li>
 * </ul>
 * </p>
 * @author LOY, bannie2492
 * @version 1.1
 */
public class SecurityStatus extends LookupEntity {

    /**
     * The flag indicating this is the default security status.
     *
     * @since 1.1
     */
    private boolean defaultStatus;

    /**
     * Default constructor.
     */
    public SecurityStatus() {
    }

    /**
     * Get default status.
     *
     * @return the default status
     * @since 1.1
     */
    public boolean isDefaultStatus() {
        return defaultStatus;
    }

    /**
     * Set the default status.
     *
     * @param defaultStatus the defaultStatus to be set
     * @since 1.1
     */
    public void setDefaultStatus(boolean defaultStatus) {
        this.defaultStatus = defaultStatus;
    }

}
