/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.exceptions;

/**
 * <p>
 * This exception is thrown if error occurs when configuring the class.
 * </p>
 * <p>
 * <strong>Thread Safety: </strong> Exception is not thread safe because its base class is not thread safe.
 * </p>
 *
 * @author LOY, bannie2492
 * @version 1.0
 */
public class ConfigurationException extends RuntimeException {

    /**
     * <p>
     * Constructs a new <code>ConfigurationException</code> instance with error message.
     * </p>
     *
     * @param message the exception message
     */
    public ConfigurationException(String message) {
        super(message);
    }

    /**
     * <p>
     * Constructs a new <code>ConfigurationException</code> instance with error message and inner cause.
     * </p>
     *
     * @param message the exception message
     * @param cause the cause of the exception
     */
    public ConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }
}
