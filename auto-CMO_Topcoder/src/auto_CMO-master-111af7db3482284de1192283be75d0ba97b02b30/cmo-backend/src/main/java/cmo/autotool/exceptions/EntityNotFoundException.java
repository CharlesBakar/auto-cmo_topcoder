/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.exceptions;

/**
 * <p>
 * This exception is thrown if corresponding entity is not found.
 * </p>
 *
 * <p>
 * <strong>Thread Safety: </strong> Exception is not thread safe because its base class is not thread safe.
 * </p>
 *
 * @author LOY, bannie2492
 * @version 1.0
 */
public class EntityNotFoundException extends PersistenceException {

    /**
     * <p>
     * Constructs a new <code>EntityNotFoundException</code> instance with error message.
     * </p>
     *
     * @param message the error message.
     */
    public EntityNotFoundException(String message) {
        super(message);
    }

    /**
     * <p>
     * Constructs a new <code>EntityNotFoundException</code> instance with error message and inner cause.
     * </p>
     *
     * @param message the error message.
     * @param cause the inner cause.
     */
    public EntityNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
