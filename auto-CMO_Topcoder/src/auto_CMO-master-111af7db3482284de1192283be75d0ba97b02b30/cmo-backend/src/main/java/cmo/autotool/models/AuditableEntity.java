/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.models;

import java.util.Date;

/**
 * <p>
 * The auditable entity.
 * </p>
 *
 * <p>
 * <strong>Thread Safety: </strong> This class is not thread safe because it is mutable.
 * </p>
 *
 * @author LOY, bannie2492
 * @version 1.0
 */
public abstract class AuditableEntity extends IdentifiableEntity {

    /**
     * The created timestamp.
     */
    private Date createdTS;

    /**
     * The user who creates the entity.
     */
    private String createdByNM;

    /**
     * The updated timestamp
     */
    private Date updatedTS;

    /**
     * The user who updates the entity.
     */
    private String updatedByNM;

    /**
     * Default constructor.
     */
    public AuditableEntity() {
    }

    /**
     * Set createdTS.
     *
     * @param createdTS the createdTS to be set
     */
    public void setCreatedTS(Date createdTS) {
        this.createdTS = createdTS;
    }

    /**
     * Get createdTS.
     *
     * @return the createdTS
     */
    public Date getCreatedTS() {
        return createdTS;
    }

    /**
     * Set createdByNM.
     *
     * @param createdByNM the createdByNM to be set
     */
    public void setCreatedByNM(String createdByNM) {
        this.createdByNM = createdByNM;
    }

    /**
     * Get createdByNM.
     *
     * @return the createdByNM
     */
    public String getCreatedByNM() {
        return createdByNM;
    }

    /**
     * Set updatedTS.
     *
     * @param updatedTS the updatedTS to be set
     */
    public void setUpdatedTS(Date updatedTS) {
        this.updatedTS = updatedTS;
    }

    /**
     * Get updatedTS.
     *
     * @return the updatedTS
     */
    public Date getUpdatedTS() {
        return updatedTS;
    }

    /**
     * Set updatedByNM.
     *
     * @param updatedByNM the updatedByNM to be set
     */
    public void setUpdatedByNM(String updatedByNM) {
        this.updatedByNM = updatedByNM;
    }

    /**
     * Get updatedByNM.
     *
     * @return the updatedByNM
     */
    public String getUpdatedByNM() {
        return updatedByNM;
    }
}
