/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.exceptionresolvers;

import cmo.autotool.Helper;
import cmo.autotool.exceptions.EntityNotFoundException;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;


/**
 * This is a subclass of SimpleMappingExceptionResolver which resolve exceptions to HTTP status code.
 *
 * <p>
 * <strong>Thread Safety: </strong>This class effectively thread safe when the following condition is met: this class is
 * initialized by Spring right after construction and its parameters are never changed after that. All entities passed
 * to this class are used by the caller in thread safe manner (accessed from a single thread only).
 * </p>
 *
 * <p>Change log 1.1:
 * <ul>
 *   <li>Add static logger.</li>
 * </ul>
 * </p>
 * @author LOY, bannie2492
 * @version 1.1
 */
@Component
public class AJAXExceptionResolver extends SimpleMappingExceptionResolver {

    /**
     * The LOGGER.
     */
    private static final Logger LOGGER = Logger.getLogger(AJAXExceptionResolver.class.getPackage().getName());

    /**
     * The class name.
     */
    private static final String CLASS_NAME = AJAXExceptionResolver.class.getName();

    /**
     * Default constructor.
     *
     * Impl Notes:
     * do nothing
     */
    public AJAXExceptionResolver() {
    }


    /**
     * Resolves the exception.
     * @param request the request
     * @param response the response
     * @param handler the handler
     * @param exception the exception
     * @return always null
     */
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler,
        Exception exception) {
        final String signature = CLASS_NAME + "#resolveException(request, response, handler, exception)";
        // Log entrance
        Helper.logEntrance(LOGGER, signature, new String[] {"request", "response", "handler", "exception"},
                new Object[] {Helper.NA, Helper.NA, Helper.NA, exception.getMessage()});
        try {
            if (exception instanceof IllegalArgumentException) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            } else if (exception instanceof EntityNotFoundException) {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            } else {
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            }
            if (exception.getMessage() != null) {
                response.getWriter().write(exception.getMessage());
            }
        } catch (IOException e) {
            // Log and ignore here
            Helper.logException(LOGGER, signature, e);
        }
        Helper.logExit(LOGGER, signature, null);
        return new ModelAndView();
    }
}

