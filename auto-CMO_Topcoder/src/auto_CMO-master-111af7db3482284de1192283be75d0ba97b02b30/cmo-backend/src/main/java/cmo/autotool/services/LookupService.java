/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.services;

import cmo.autotool.exceptions.EntityNotFoundException;
import cmo.autotool.exceptions.PersistenceException;
import cmo.autotool.exceptions.ServiceException;
import cmo.autotool.models.SecurityStatus;
import cmo.autotool.models.UserRole;
import java.util.List;

/**
 * This service provides the operations to manage lookup values.
 *
 * <p>
 * <strong>Thread Safety: </strong>Implementations of this interface should be effectively thread safe when entities
 * passed to them are used by the caller in thread safe manner.
 * </p>
 *
 * @author LOY, bannie2942
 * @version 1.0
 */
public interface LookupService {
    /**
     * Get all security statuses.
     *
     * @throws PersistenceException if some error occurred while accessing the persistence.
     * @throws ServiceException if any other error occurs.
     * @return All security statuses.
     */
    public List<SecurityStatus> getAllSecurityStatuses() throws ServiceException;

    /**
     * Get all user roles.
     *
     * @throws PersistenceException if some error occurred while accessing the persistence.
     * @throws ServiceException if any other error occurs.
     * @return All user roles
     */
    public List<UserRole> getAllUserRoles() throws ServiceException;

    /**
     * Update security statuses.
     *
     * @throws IllegalArgumentException if securityStatuses is null/empty, or contains null.
     * @throws EntityNotFoundException if the entity is not found in persistence.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     * @throws ServiceException if any other error occurs.
     * @param securityStatuses the security statuses
     */
    public void updateSecurityStatuses(List<SecurityStatus> securityStatuses) throws ServiceException;
}

