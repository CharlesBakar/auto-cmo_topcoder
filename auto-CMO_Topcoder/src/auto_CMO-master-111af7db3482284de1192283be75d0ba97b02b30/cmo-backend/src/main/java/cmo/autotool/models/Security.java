/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.models;

import java.util.List;

/**
 * <p>
 * The security.
 * </p>
 *
 * <p>
 * <strong>Thread Safety: </strong> This class is not thread safe because it is mutable.
 * </p>
 *
 * @author LOY, bannie2942
 * @version 1.0
 */
public class Security extends AuditableEntity {

    /**
     * The CUSIP.
     */
    private String cusip;

    /**
     * The security daily value for a specific business date.
     */
    private SecurityDailyValue securityDailyValue;

    /**
     * The security details for a specific business date.
     */
    private List<SecurityDetail> securityDetails;

    /**
     * Default constructor.
     */
    public Security() {
    }

    /**
     * Set cusip.
     *
     * @param cusip the cusip to be set
     */
    public void setCusip(String cusip) {
        this.cusip = cusip;
    }

    /**
     * Get cusip.
     *
     * @return the cusip
     */
    public String getCusip() {
        return cusip;
    }

    /**
     * Set securityDailyValue.
     *
     * @param securityDailyValue the securityDailyValue to be set
     */
    public void setSecurityDailyValue(SecurityDailyValue securityDailyValue) {
        this.securityDailyValue = securityDailyValue;
    }

    /**
     * Get securityDailyValue.
     *
     * @return the securityDailyValue
     */
    public SecurityDailyValue getSecurityDailyValue() {
        return securityDailyValue;
    }

    /**
     * Set securityDetails.
     *
     * @param securityDetails the securityDetails to be set
     */
    public void setSecurityDetails(List<SecurityDetail> securityDetails) {
        this.securityDetails = securityDetails;
    }

    /**
     * Get securityDetails.
     *
     * @return the securityDetails
     */
    public List<SecurityDetail> getSecurityDetails() {
        return securityDetails;
    }
}
