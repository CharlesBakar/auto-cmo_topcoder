/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.services.impl;

import cmo.autotool.Helper;
import cmo.autotool.exceptions.ConfigurationException;
import cmo.autotool.exceptions.PersistenceException;
import cmo.autotool.exceptions.ServiceException;
import cmo.autotool.models.Category9920;
import cmo.autotool.models.FileDetail;
import cmo.autotool.models.FileStatus;
import cmo.autotool.models.Security;
import cmo.autotool.models.SecurityDailyValue;
import cmo.autotool.models.SecurityDetail;
import cmo.autotool.models.SecurityStatus;
import cmo.autotool.models.dto.ServiceContext;
import cmo.autotool.models.dto.User;
import cmo.autotool.services.DataImportService;
import cmo.autotool.services.FileDetailService;
import cmo.autotool.services.CMOSettingService;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import javax.annotation.PostConstruct;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * This is the implementation of DataImportService.
 *
 * <p>
 * <strong>Thread Safety: </strong>This class effectively thread safe when the following condition is met: this class is
 * initialized by Spring right after construction and its parameters are never changed after that. All entities passed
 * to this class are used by the caller in thread safe manner (accessed from a single thread only).
 * </p>
 *
 * <p>Change log 1.1:
 * <ul>
 *   <li>Update field followupStatus to defaultStatus. Update implementation for default security status.</li>
 *   <li>Update method parseSecurityDetail for more fields.</li>
 * </ul>
 * </p>
 * <p>Change log 1.2:
 * <ul>
 *   <li>Remove static field FOLLOWUP_STATUS_NAME.</li>
 *   <li>Throw IAE when no valid records exist.</li>
 *   <li>Add static logger.</li>
 *   <li>Refactor code.</li>
 * </ul>
 * </p>
 * <p>Change log 1.3:
 * <ul>
 *   <li>Add logging information as per request from the client..</li>
 * </ul>
 * </p>
 * @author LOY, bannie2492
 * @version 1.3
*/
@Service
public class DataImportServiceImpl extends BasePersistenceService implements DataImportService {

    /**
     * The LOGGER.
     */
    private static final Logger LOGGER = Logger.getLogger(DataImportServiceImpl.class);

    /**
     * The class name.
     */
    private static final String CLASS_NAME = DataImportServiceImpl.class.getName();

    /**
     * The separator of the fields.
     */
    private static final String FIELD_SEPARATOR = "~";

    /**
     * The column number of the file.
     */
    private static final int COLUMN_NUM = 60;

    /**
     * The FileDetailService instance.
     *
     * Valid value: Not null
     */
    @Autowired
    private FileDetailService fileDetailService;

    /**
     * The CMO setting service.
     *
     * Valid value: Not null
     */
    @Autowired
    private CMOSettingService cmoSettingService;

    /**
     * The thread count.
     *
     * Valid value: Positive.
     */
    @Value("#{threadCount}")
    private int threadCount;

    /**
     * The batch size to insert security details and daily values.
     *
     * Valid value: Positive.
     */
    @Value("#{batchInsertCount}")
    private int batchInsertCount;

    /**
     * The processed input file folder.
     *
     * Valid value: Not null/empty.
     */
    @Value("#{processedInputFileFolder}")
    private String processedInputFileFolder;

    /**
     * The default security status for security detail.
     *
     * Valid value: not null.
     */
    private SecurityStatus defaultStatus;

    /**
     * Empty constructor.
     */
    public DataImportServiceImpl() {
    }

    /**
     * Check the configuration.
     *
     * @throws ConfigurationException if there's any error in configuration.
     */
    @PostConstruct
    public void checkConfiguration() {
        super.checkConfiguration();
        Helper.checkConfigNotNull(fileDetailService, "fileDetailService");
        Helper.checkConfigNotNull(cmoSettingService, "cmoSettingService");
        Helper.checkConfigPositive(threadCount, "threadCount");
        Helper.checkConfigPositive(batchInsertCount, "batchInsertCount");
        Helper.checkConfigNotEmpty(processedInputFileFolder, "processedInputFileFolder");
        Helper.checkFolder(processedInputFileFolder, "processedInputFileFolder");
    }

    /**
     * Import the data from the input file.
     *
     * @param filePath the imported file path
     * @param businessDate the business date
     * @throws IllegalArgumentException if can find the default security status, filePath is null/empty or businessDate
     * is empty
     * @throws ServiceException if any other error occurred during the operation
    */
    public void importData(String filePath, Date businessDate) throws ServiceException {
        final String signature = CLASS_NAME + "#importData(String, Date)";
        Helper.logEntrance(LOGGER, signature, new String[] {"filePath", "businessDate"},
                new Object[] {filePath, businessDate});

        LOGGER.info("Start importing file " + filePath);
        long startTime = System.nanoTime();

        Helper.checkNullOrEmpty(filePath, "filePath");
        Helper.checkNull(businessDate, "businessDate");
        // Get the default security status
        getDefaultStatus();

        final Date today = new Date();
        final User user = ServiceContext.getUser();
        CMOMapper cmoMapper = getCMOMapper();
        ExecutorService executorService = null;
        FileDetail fileDetail = createFileDetail(filePath, today);
        DataImportInfoDTO fileInfo = new DataImportInfoDTO();
        try {
            // parse the file
            fileInfo = parseFile(filePath, businessDate, signature);
            // create any new securities
            int newcount = persistNewSecurities(cmoMapper, fileInfo.getDetails(), today, user);
            if (newcount > 0) {
                LOGGER.info("Persisted " + newcount + " new securities");
            }
            // process the security details and create security daily values
            List<SecurityDailyValue> securityDailyValues =
                    processSecurityDetails(cmoMapper, fileInfo.getDetails(), today, user, businessDate);

            // Calculate daysStale for each SecurityDailyValue
            // Create one task for each value to process, and divide
            // those tasks amongst threadCount threads
            final double marketValueThreshold = cmoSettingService.getCurrentCMOSetting().getMarketValueThreshold();
            executorService = Executors.newFixedThreadPool(threadCount);
            for (final SecurityDailyValue sdv : securityDailyValues) {
                executorService.execute(() -> {
                    calculateStaleDays(sdv, marketValueThreshold, cmoMapper, businessDate);
                });
            }
            executorService.shutdown();
            while (!executorService.awaitTermination(1, TimeUnit.SECONDS)) {
                // Wait for the executor to finish
                // Guaranteed to happen eventually, but let's keep a heartbeat
                LOGGER.debug("Waiting for executor to finish");
            }
            // persist the security daily value and detail
            persistData(cmoMapper, businessDate, fileInfo.getDetails(), today, user, securityDailyValues);
            // mark the file status as complete
            fileDetail.setFileStatus(FileStatus.SUCCESS);
            // Log exit
            Helper.logExit(LOGGER, signature, null);
        } catch (org.apache.ibatis.exceptions.PersistenceException e) {
            fileDetail.setFileStatus(FileStatus.FAILED);
            throw Helper.logException(LOGGER, signature,
                    new PersistenceException("BE_DB_ERROR", e));
        } catch (InterruptedException e) {
            fileDetail.setFileStatus(FileStatus.FAILED);
            throw Helper.logException(LOGGER, signature,
                    new ServiceException("GENERAL_ERROR_MESSAGE", e));
        } catch (ServiceException | IllegalArgumentException e) {
            fileDetail.setFileStatus(FileStatus.FAILED);
            throw e;
        } finally {
            long endTime = System.nanoTime();
            LOGGER.info("Complete importing file " + filePath + ", Summary:");
            LOGGER.info("Process time used: " + getExecutionTime(startTime, endTime));
            LOGGER.info("Number of records processed: " + fileInfo.getTotalRecordCount());
            LOGGER.info("Number of records successfully processed: " + fileInfo.getSuccessRecordCount());
            LOGGER.info("Number of records failed: "
                    + (fileInfo.getTotalRecordCount() - fileInfo.getSuccessRecordCount()));
            // force shutdown of executor
            if (executorService != null && !executorService.isShutdown()) {
                try {
                    executorService.shutdownNow();
                } catch (SecurityException e) {
                    Helper.logException(LOGGER, signature, e);
                    // ignore
                }

            }
            moveAndSaveFileDetail(filePath, fileDetail, fileInfo);
        }
    }
    
    /**
     * Get the time duration of execution.
     * @param start the start time
     * @param end the end time
     * @return the duration
     * @since 1.3
     */
    private static String getExecutionTime(long start, long end) {
        long duration = (end - start) / 1000000000;
        if (duration < 60) {
            return duration + " second";
        }
        long minute = duration / 60;
        return minute + " minute " + (duration - minute * 60) + " second";
    }

    /**
     * Move and save the file detail information.
     * @param filePath the file path
     * @param fileDetail the file detail instance
     * @param fileInfo the file information DTO
     * @throws ServiceException if there is any error performing the task
     * @since 1.2
     */
    private void moveAndSaveFileDetail(String filePath, FileDetail fileDetail, DataImportInfoDTO fileInfo) 
            throws ServiceException {
        // move input file to processed Folder
        File inputFile = new File(filePath);
        File processedFile = new File(processedInputFileFolder, inputFile.getName());
        try {
            Files.move(inputFile.toPath(), processedFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ex) {
            String msg = "Failed to move " + inputFile.getPath() + " to " + processedFile.getPath();
            LOGGER.error(msg, ex);
            throw new ServiceException(msg, ex);
        }
        // update fileDetail
        fileDetail.setFilePath(inputFile.getAbsolutePath());
        fileDetail.setTotalRecordCount(fileInfo.getTotalRecordCount());
        fileDetail.setSuccessRecordCount(fileInfo.getSuccessRecordCount());
        fileDetail.setFailureRecordCount(fileInfo.getTotalRecordCount() - fileInfo.getSuccessRecordCount());
        fileDetail.setIgnoredRecordCount(0);   // Currently, no record will be ignored.
        fileDetail.setFinishedTime(new Date());
        fileDetailService.update(fileDetail);
    }

    /**
     * Calculate how many days a security has been stale, and update
     * the SecurityDailyValue's daysStale value appropriately.
     *
     * @param securityDailyValues the security daily values
     * @param marketValueThreshold the market value threshold
     * @param cmoMapper the mybatis mapper
     * @param businessDate the business date
     * @since 1.2
     */
    private void calculateStaleDays(final SecurityDailyValue sdv,
                                    final double marketValueThreshold,
                                    CMOMapper cmoMapper, Date businessDate) {
        if (sdv.getMarketValuePrice() < marketValueThreshold) {
            // Today's value is bad
            final List<Double> previousMarketValuePrices
                    = cmoMapper.getMarketValuePrices(sdv.getSecurityId(), businessDate);
            // Count back for how many days the security has been stale
            int count = 0;
            for (Double previousMarketValuePrice : previousMarketValuePrices) {
                if (previousMarketValuePrice < marketValueThreshold) {
                    count++;
                } else {
                    break;
                }
            }
            // +1 for today
            count++;
            sdv.setDaysStale(count);
        } else {
            // Today's value is good, reset stale days count
            sdv.setDaysStale(0);
        }
    }

    /**
     * Parse input file.
     * @param filePath the file path
     * @param businessDate the business date
     * @param signature the signature
     * @return the DTO object containing file import information
     * @throws ServiceException if there is any error
     * @since 1.2
     */
    private DataImportInfoDTO parseFile(String filePath, Date businessDate, String signature) throws ServiceException {
        DataImportInfoDTO result = new DataImportInfoDTO();
        int successRecordCount = 0;
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            // parse the data
            List<SecurityDetail> securityDetails = new ArrayList<>();
            int lineCount = 0;
            String line;
            final DateFormat dateFormat1 = new SimpleDateFormat("yyyyMMdd");
            final DateFormat dateFormat2 = new SimpleDateFormat("MM/dd/yyyy");
            while ((line = br.readLine()) != null) {
                lineCount++;
                String[] fields = line.split(FIELD_SEPARATOR);
                if (lineCount <= 2) {
                    // Header line - silently ignore
                    continue;
                } else if (fields.length != COLUMN_NUM) {
                    // Cannot parse line
                    LOGGER.warn(String.format("Failed to process line %d in %s, not enough information to be "
                                + "processed. Please check if there is a data loss in this row.",
                                lineCount, filePath));
                    continue;
                }
                SecurityDetail securityDetail = parseSecurityDetail(fields, filePath, lineCount,
                        dateFormat1, dateFormat2);
                if (securityDetail != null) {
                    securityDetail.setBusinessDate(businessDate);
                    securityDetails.add(securityDetail);
                    successRecordCount++;
                }
            }
            if (successRecordCount == 0) {
                throw Helper.logException(LOGGER, signature,
                        new IllegalArgumentException("BE_IMPORT_NO_VALID_RECORD"));
            }
            result.setDetails(securityDetails);
            result.setSuccessRecordCount(successRecordCount);
            result.setTotalRecordCount(lineCount - 2);
            return result;
        } catch (IOException | SecurityException e) {
            throw Helper.logException(LOGGER, signature,
                    new ServiceException("BE_FILE_IMPORT_ERROR", e));
        }
    }

    /**
     * Process the security details and create security daily values.
     * @param cmoMapper the mybatis mapper
     * @param securityDetails the security details
     * @param currentTime the current time stamp
     * @param user the logged in user
     * @param businessDate the business date
     * @return the created security daily values
     */
    private List<SecurityDailyValue> processSecurityDetails(CMOMapper cmoMapper, List<SecurityDetail> securityDetails,
            final Date currentTime, final User user, Date businessDate) {
        // get security daily values from security details
        List<Security> existingSecurities = cmoMapper.getAllSecurities();
        Map<String, Security> existingSecurityMap = new HashMap<>();
        for (Security security : existingSecurities) {
            existingSecurityMap.put(security.getCusip(), security);
        }
        // Update SecurityDetails with the id of the security and
        // create daily value for each security
        List<SecurityDailyValue> securityDailyValues = new ArrayList<>();
        Set<String> cusips = new HashSet<>();
        securityDetails.stream().forEach(detail -> {
            final String cusip = detail.getCusip();
            Security security = existingSecurityMap.get(cusip);
            if (security == null) {
                throw new IllegalStateException("Security with CUSIP " + cusip + " not found");
            }
            detail.setSecurityId(security.getId());
            if (!cusips.contains(cusip)) {
                cusips.add(cusip);
                SecurityDailyValue securityDailyValue = createSecurityDailyValue(detail, businessDate);
                securityDailyValues.add(securityDailyValue);
            } else {
                LOGGER.debug("Ignoring duplicate security with CUSIP " + cusip);
            }
        });
        return securityDailyValues;
    }

    /**
     * Persist any new securities
     * @param cmoMapper the mybatis mapper
     * @param securityDetails list of security details
     * @return the count of newly-created securities
     */
    private int persistNewSecurities(CMOMapper cmoMapper, List<SecurityDetail> securityDetails, final Date currentTime, final User user) {
        // get security daily values from security details
        List<Security> existingSecurities = cmoMapper.getAllSecurities();
        Set<String> cusips = new HashSet<String>();
        for (Security security : existingSecurities) {
            cusips.add(security.getCusip());
        }
        // if security is not in DB, create it
        int count = cusips.size();
        securityDetails.stream().forEach(detail -> {
            if (!cusips.contains(detail.getCusip())) {
                Security security = createSecurity(detail, currentTime, user);
                cmoMapper.insertSecurity(security);
                cusips.add(detail.getCusip());
            }
        });
        return cusips.size() - count; // How many have been added?
    }

    /**
     * Persist security details and security daily value data.
     *
     * NOTE: Will erase any existing daily values for businessDate,
     * even if persisting new ones fails.
     *
     * @param cmoMapper the mybatis mapper
     * @param businessDate the current business date
     * @param securityDetails the security details
     * @param currentTime the current time
     * @param user the logged in user
     * @param securityDailyValues the security daily values
     * @since 1.2
     */
    private void persistData(CMOMapper cmoMapper, Date businessDate, List<SecurityDetail> securityDetails,
            final Date currentTime, final User user, List<SecurityDailyValue> securityDailyValues) {
        // delete existing data of businessDate in [SECURITY_DETAIL] table and [SECURITY_DAILY_VALUE] table
        cmoMapper.deleteSecurityDetails(businessDate);
        cmoMapper.deleteSecurityDailyValues(businessDate);
        // set auditable fields for securityDetails and securityDailyValues
        securityDetails.stream().forEach(detail -> {
            detail.setCreatedTS(currentTime);
            detail.setCreatedByNM(user.getCorpId());
            detail.setUpdatedTS(currentTime);
            detail.setUpdatedByNM(user.getCorpId());
        });
        securityDailyValues.stream().forEach(sdv -> {
            sdv.setCreatedTS(currentTime);
            sdv.setCreatedByNM(user.getCorpId());
            sdv.setUpdatedTS(currentTime);
            sdv.setUpdatedByNM(user.getCorpId());
        });
        // persist securityDetails and securityDailyValues
        for (int i = 0; i < securityDetails.size(); i += batchInsertCount) {
            int endIndex = i + batchInsertCount <= securityDetails.size() - 1 ? i + batchInsertCount
                    : securityDetails.size();
            List<SecurityDetail> subList = securityDetails.subList(i, endIndex);
            cmoMapper.insertSecurityDetails(subList);
        }
        for (int i = 0; i < securityDailyValues.size(); i += batchInsertCount) {
            int endIndex = i + batchInsertCount <= securityDailyValues.size() - 1 ? i + batchInsertCount
                    : securityDailyValues.size();
            List<SecurityDailyValue> subList = securityDailyValues.subList(i, endIndex);
            cmoMapper.insertSecurityDailyValues(subList);
        }
    }

    /**
     * Get default status.
     * @since 1.2
     */
    private void getDefaultStatus() {
        if (defaultStatus != null) {
            return;
        }

        List<SecurityStatus> statuses = getCMOMapper().getAllSecurityStatuses();
        boolean found = false;
        for (SecurityStatus s : statuses) {
            if (s.isDefaultStatus()) {
                defaultStatus = s;
                found = true;
                break;
            }
        }
        if (!found) {
            Helper.checkState(false, "The default security status can not be found in DB.");
        }
    }

    /**
     * Create security daily value.
     * @param securityDetail the security detail
     * @param businessDate the business date of the security daily value
     * @return the created instance
     * @since 1.2
     */
    private SecurityDailyValue createSecurityDailyValue(SecurityDetail securityDetail, Date businessDate) {
        SecurityDailyValue securityDailyValue = new SecurityDailyValue();
        securityDailyValue.setSecurityId(securityDetail.getSecurityId());
        securityDailyValue.setBusinessDate(businessDate);
        securityDailyValue.setMarketValuePrice(securityDetail.getMarketValuePrice());
        securityDailyValue.setSecurityStatus(defaultStatus);
        return securityDailyValue;
    }

    /**
     * Create security.
     * @param detail the security detail
     * @param currentTime the current time
     * @param user the logged in user
     * @return the security
     * @since 1.2
     */
    private Security createSecurity(SecurityDetail detail, final Date currentTime, final User user) {
        Security security = new Security();
        security.setCusip(detail.getCusip());
        security.setCreatedTS(currentTime);
        security.setUpdatedTS(currentTime);
        security.setCreatedByNM(user.getCorpId());
        security.setUpdatedByNM(user.getCorpId());
        return security;
    }

    /**
     * Create the file detail.
     * @param filePath the file path
     * @param currentTime the current time
     * @return the created fileDetail
     * @throws ServiceException if the creation fails
     * @since 1.2
     */
    private FileDetail createFileDetail(String filePath, final Date currentTime) throws ServiceException {
        FileDetail fileDetail = new FileDetail();
        // Create FileDetail
        fileDetail.setFilePath(filePath);
        fileDetail.setStartTime(currentTime);
        fileDetail.setFileStatus(FileStatus.PROCESSING);
        fileDetail = fileDetailService.create(fileDetail);
        return fileDetail;
    }

    /**
     * Parse security details.
     * @param fields the data array
     * @param filePath the file path
     * @param lineCount the line count
     * @param dateFormat1 the date format for yyyyMMdd
     * @param dateFormat2 the date format for MM/dd/yyyy
     * @return the parsed SecurityDetail entity. If there is any error parsing the file, null is returned.
     */
    private SecurityDetail parseSecurityDetail(String[] fields, String filePath,
            int lineCount, DateFormat dateFormat1, DateFormat dateFormat2) {
        final String signature = "DataImportServiceImpl#parseSecurityDetail";
        int i = 0;
        SecurityDetail d = new SecurityDetail();
        try {
            d.setFundNumber(fields[i]);
            d.setCusip(fields[++i]);
            switch (fields[++i].trim()) {
                case "NOT 99-20":
                    d.setCategory9920(Category9920.NOT_9920);
                    break;
                case "EXISTING 99-20":
                    d.setCategory9920(Category9920.EXISTING_9920);
                    break;
                case "NEW 99-20":
                    d.setCategory9920(Category9920.NEW_9920);
                    break;
                default:
                    throw new IllegalArgumentException("unknown category 9920:" + fields[i]);
            }
            d.setReason9920(fields[++i].trim());
            d.setSecType(fields[++i].trim());
            d.setIoPoInd(fields[++i]);
            d.setTaxLotId(fields[++i]);
            d.setCouponRate(new Double(fields[++i]));
            d.setTradeDate(dateFormat1.parse(fields[++i]));
            d.setSettleDate(dateFormat1.parse(fields[++i]));
            d.setQuantityPar(new Double(fields[++i]));
            d.setAmortizedCostPrice(new Double(fields[++i]));
            d.setCurrentBookCost(new Double(fields[++i]));
            d.setBaseOrigCost(new Double(fields[++i]));
            d.setMarketValuePrice(new Double(fields[++i]));
            d.setMarketValue(new Double(fields[++i]));
            d.setUnitOfCalc(fields[++i]);
            d.setCurrencyCode(fields[++i]);
            d.setExchangeRate(new Double(fields[++i]));
            d.setExchangeRateInd(fields[++i]);
            d.setYieldToMaturity(new Double(fields[++i]));
            d.setYieldToWorst(new Double(fields[++i]));
            d.setPurchasePrice(new Double(fields[++i]));
            d.setMaturityDate(dateFormat1.parse(fields[++i]));
            d.setRedemptionValue(fields[++i]);
            d.setTaxLotLsInd(fields[++i]);
            d.setSecDesc(fields[++i]);
            d.setSandpRating(fields[++i]);
            d.setFitchRating(fields[++i]);
            d.setMoodyRating(fields[++i]);
            d.setUserRating(fields[++i]);
            d.setIssueLength(fields[++i]);
            d.setEffectMaturityDate(dateFormat1.parse(fields[++i]));
            d.setProspectMethod(fields[++i]);
            i = 58;
            d.setFyrBeginDate(dateFormat2.parse(fields[i]));
            d.setFyrEndDate(dateFormat2.parse(fields[++i].substring(0, 10)));
            
        } catch (ParseException e) {
            Helper.logException(LOGGER, signature,
                    new ServiceException(String.format("Failed to process line %d in %s,"
                            + " value at column %d is not a valid date", lineCount, filePath, i), e));
            d = null;
        } catch (NumberFormatException e) {
            Helper.logException(LOGGER, signature,
                    new ServiceException(String.format("Failed to process line %d in %s,"
                            + " value at column %d is not a valid number", lineCount, filePath, i), e));
            d = null;
        } catch (IllegalArgumentException e) {
            Helper.logException(LOGGER, signature,
                    new ServiceException(String.format("Failed to process line %d in %s,"
                            + " value at column %d is not a valid category", lineCount, filePath, i), e));
            d = null;
        }
        return d;
    }

    /**
     * This is the DTO class to hold data during file import.
     *
     * @author bannie2492
     * @since 1.2
     */
    class DataImportInfoDTO {

        /**
         * Total record count.
         */
        private int totalRecordCount;

        /**
         * Success record count.
         */
        private int successRecordCount;

        /**
         * The security details.
         */
        private List<SecurityDetail> details;

        /**
         * Get the total record count.
         * @return the total record count
         */
        public int getTotalRecordCount() {
            return totalRecordCount;
        }

        /**
         * Set the total record count.
         * @param totalRecordCount the total record count to be set
         */
        public void setTotalRecordCount(int totalRecordCount) {
            this.totalRecordCount = totalRecordCount;
        }

        /**
         * Get the success record count.
         * @return the success record count
         */
        public int getSuccessRecordCount() {
            return successRecordCount;
        }

        /**
         * Set the success record count.
         * @param successRecordCount the success record count to be set
         */
        public void setSuccessRecordCount(int successRecordCount) {
            this.successRecordCount = successRecordCount;
        }

        /**
         * Get the security details.
         * @return the security details
         */
        public List<SecurityDetail> getDetails() {
            return details;
        }

        /**
         * Set the security details.
         * @param details the security details to be set
         */
        public void setDetails(List<SecurityDetail> details) {
            this.details = details;
        }
    }
}

