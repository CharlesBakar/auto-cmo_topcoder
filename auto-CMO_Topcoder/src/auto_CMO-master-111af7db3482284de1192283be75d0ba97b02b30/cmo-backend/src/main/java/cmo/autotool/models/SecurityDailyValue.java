/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.models;

import java.util.Date;

/**
 * <p>
 * The security daily value.
 * </p>
 *
 * <p>
 * <strong>Thread Safety: </strong> This class is not thread safe because it is mutable.
 * </p>
 *
 * @author LOY, bannie2942
 * @version 1.0
 */
public class SecurityDailyValue extends AuditableEntity {

    /**
     * The security ID.
     */
    private long securityId;

    /**
     * The business date.
     */
    private Date businessDate;

    /**
     * The market value price.
     */
    private double marketValuePrice;

    /**
     * The days stale.
     */
    private int daysStale;

    /**
     * The security status.
     */
    private SecurityStatus securityStatus;

    /**
     * The comment.
     */
    private String comment;

    /**
     * Default constructor.
     */
    public SecurityDailyValue() {
    }

    /**
     * Set securityId.
     *
     * @param securityId the securityId to be set
     */
    public void setSecurityId(long securityId) {
        this.securityId = securityId;
    }

    /**
     * Get securityId.
     *
     * @return the securityId
     */
    public long getSecurityId() {
        return securityId;
    }

    /**
     * Set businessDate.
     *
     * @param businessDate the businessDate to be set
     */
    public void setBusinessDate(Date businessDate) {
        this.businessDate = businessDate;
    }

    /**
     * Get businessDate.
     *
     * @return the businessDate
     */
    public Date getBusinessDate() {
        return businessDate;
    }

    /**
     * Set marketValuePrice.
     *
     * @param marketValuePrice the marketValuePrice to be set
     */
    public void setMarketValuePrice(double marketValuePrice) {
        this.marketValuePrice = marketValuePrice;
    }

    /**
     * Get marketValuePrice.
     *
     * @return the marketValuePrice
     */
    public double getMarketValuePrice() {
        return marketValuePrice;
    }

    /**
     * Set daysStale.
     *
     * @param daysStale the daysStale to be set
     */
    public void setDaysStale(int daysStale) {
        this.daysStale = daysStale;
    }

    /**
     * Get daysStale.
     *
     * @return the daysStale
     */
    public int getDaysStale() {
        return daysStale;
    }

    /**
     * Set securityStatus.
     *
     * @param securityStatus the securityStatus to be set
     */
    public void setSecurityStatus(SecurityStatus securityStatus) {
        this.securityStatus = securityStatus;
    }

    /**
     * Get securityStatus.
     *
     * @return the securityStatus
     */
    public SecurityStatus getSecurityStatus() {
        return securityStatus;
    }

    /**
     * Set comment.
     *
     * @param comment the comment to be set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * Get comment.
     *
     * @return the comment
     */
    public String getComment() {
        return comment;
    }
}
