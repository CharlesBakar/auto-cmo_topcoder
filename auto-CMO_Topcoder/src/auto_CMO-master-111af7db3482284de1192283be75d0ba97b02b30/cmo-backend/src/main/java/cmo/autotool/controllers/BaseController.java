/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.controllers;

import cmo.autotool.models.dto.User;
import javax.servlet.http.HttpServletRequest;

/**
 * Base class for Spring controller implementations.
 *
 * <p>
 * <strong>Thread Safety: </strong>This class effectively thread safe when the following condition is met: this class is
 * initialized by Spring right after construction and its parameters are never changed after that. All entities passed
 * to this class are used by the caller in thread safe manner (accessed from a single thread only).
 * </p>
 *
 * <p>Change log 1.1:
 * <ul>
 *   <li>Remove logger.</li>
 * </ul>
 * </p>
 * @author LOY, bannie2492
 * @version 1.1
 */
public abstract class BaseController {

    /**
     * Default constructor.
     */
    protected BaseController() {
    }

    /**
     * Get current user from session.
     *
     * @param request the HTTP servlet request
     * @return the current user object
     */
    protected User getCurrentUser(HttpServletRequest request) {
        return (User) request.getSession().getAttribute("currentUser");
    }
}
