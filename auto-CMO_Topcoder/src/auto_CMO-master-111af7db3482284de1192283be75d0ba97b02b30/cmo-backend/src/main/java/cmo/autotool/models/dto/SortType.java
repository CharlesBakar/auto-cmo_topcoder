/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.models.dto;

/**
 * <p>
 * The sort type.
 * </p>
 *
 * <p>
 * <strong>Thread Safety: </strong> This enumeration is immutable and thread safe.
 * </p>
 *
 * @author LOY, bannie2942
 * @version 1.0
 */
public enum SortType {

    /**
     * The sort type is "Ascending".
     */
    ASCENDING,
    /**
     * The sort type is "Descending".
     */
    DESCENDING
}
