/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.models.dto;

import cmo.autotool.models.Category9920;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * The security search criteria.
 * </p>
 *
 * <p>
 * <strong>Thread Safety: </strong> This class is not thread safe because it is mutable.
 * </p>
 *
 * <p>Change log 1.1:
 * <ul>
 *   <li>Add fields marketValueFrom and marketValueTo.</li>
 * </ul>
 * </p>
 * @author LOY, bannie2492
 * @version 1.1
 */
public class SecuritySearchCriteria extends BaseSearchCriteria {

    /**
     * The keyword.
     */
    private String keyword;

    /**
     * The CUSIP.
     */
    private String cusip;

    /**
     * The FYR begin date.
     */
    private Date fyrBeginDate;

    /**
     * The FYR end date.
     */
    private Date fyrEndDate;

    /**
     * The days stale.
     */
    private Integer daysStale;

    /**
     * The 99-20 categories.
     */
    private List<Category9920> categories9920;

    /**
     * The business date.
     */
    private Date businessDate;

    /**
     * The flag if it's eligible for monitoring.
     */
    private Boolean eligibleForMonitoring;

    /**
     * The criteria of the start range of the market value price.
     * @since 1.1
     */
    private Double marketValueFrom;

    /**
     * The criteria of the end range of the market value price.
     * @since 1.1
     */
    private Double marketValueTo;

    /**
     * Default constructor.
     */
    public SecuritySearchCriteria() {
    }

    /**
     * Set keyword.
     *
     * @param keyword the keyword to be set
     */
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    /**
     * Get keyword.
     *
     * @return the keyword
     */
    public String getKeyword() {
        return keyword;
    }

    /**
     * Set cusip.
     *
     * @param cusip the cusip to be set
     */
    public void setCusip(String cusip) {
        this.cusip = cusip;
    }

    /**
     * Get cusip.
     *
     * @return the cusip
     */
    public String getCusip() {
        return cusip;
    }

    /**
     * Set fyrBeginDate.
     *
     * @param fyrBeginDate the fyrBeginDate to be set
     */
    public void setFyrBeginDate(Date fyrBeginDate) {
        this.fyrBeginDate = fyrBeginDate;
    }

    /**
     * Get fyrBeginDate.
     *
     * @return the fyrBeginDate
     */
    public Date getFyrBeginDate() {
        return fyrBeginDate;
    }

    /**
     * Set fyrEndDate.
     *
     * @param fyrEndDate the fyrEndDate to be set
     */
    public void setFyrEndDate(Date fyrEndDate) {
        this.fyrEndDate = fyrEndDate;
    }

    /**
     * Get fyrEndDate.
     *
     * @return the fyrEndDate
     */
    public Date getFyrEndDate() {
        return fyrEndDate;
    }

    /**
     * Set daysStale.
     *
     * @param daysStale the daysStale to be set
     */
    public void setDaysStale(Integer daysStale) {
        this.daysStale = daysStale;
    }

    /**
     * Get daysStale.
     *
     * @return the daysStale
     */
    public Integer getDaysStale() {
        return daysStale;
    }

    /**
     * Set categories9920.
     *
     * @param categories9920 the categories9920 to be set
     */
    public void setCategories9920(List<Category9920> categories9920) {
        this.categories9920 = categories9920;
    }

    /**
     * Get categories9920.
     *
     * @return the categories9920
     */
    public List<Category9920> getCategories9920() {
        return categories9920;
    }

    /**
     * Set businessDate.
     *
     * @param businessDate the businessDate to be set
     */
    public void setBusinessDate(Date businessDate) {
        this.businessDate = businessDate;
    }

    /**
     * Get businessDate.
     *
     * @return the businessDate
     */
    public Date getBusinessDate() {
        return businessDate;
    }

    /**
     * Set eligibleForMonitoring.
     *
     * @param eligibleForMonitoring the eligibleForMonitoring to be set
     */
    public void setEligibleForMonitoring(Boolean eligibleForMonitoring) {
        this.eligibleForMonitoring = eligibleForMonitoring;
    }

    /**
     * Get eligibleForMonitoring.
     *
     * @return the eligibleForMonitoring
     */
    public Boolean getEligibleForMonitoring() {
        return eligibleForMonitoring;
    }

    /**
     * Get marketValueFrom.
     *
     * @return the marketValueFrom
     * @since 1.1
     */
    public Double getMarketValueFrom() {
        return marketValueFrom;
    }

    /**
     * Set marketValueFrom.
     *
     * @param marketValueFrom the marketValueFrom to be set
     * @since 1.1
     */
    public void setMarketValueFrom(Double marketValueFrom) {
        this.marketValueFrom = marketValueFrom;
    }

    /**
     * Get marketValueTo.
     *
     * @return the marketValueTo
     * @since 1.1
     */
    public Double getMarketValueTo() {
        return marketValueTo;
    }

    /**
     * Set marketValueTo.
     *
     * @param marketValueTo the marketValueTo to be set
     * @since 1.1
     */
    public void setMarketValueTo(Double marketValueTo) {
        this.marketValueTo = marketValueTo;
    }
}
