/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.controllers;

import cmo.autotool.Helper;
import cmo.autotool.exceptions.ConfigurationException;
import cmo.autotool.exceptions.ServiceException;
import cmo.autotool.models.Security;
import cmo.autotool.models.SecurityStatus;
import cmo.autotool.models.dto.Comment;
import cmo.autotool.models.dto.SearchResult;
import cmo.autotool.models.dto.SecuritySearchCriteria;
import cmo.autotool.services.SecurityService;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



/**
 * The controller to expose security related APIs.
 *
 * <p>
 * <strong>Thread Safety: </strong>This class effectively thread safe when the following condition is met: this class is
 * initialized by Spring right after construction and its parameters are never changed after that. All entities passed
 * to this class are used by the caller in thread safe manner (accessed from a single thread only).
 * </p>
 *
 * <p>Change log 1.1:
 * <ul>
 *   <li>Add end point getSecurityComments.</li>
 *   <li>Add end point checkSecurityStatusUsed.</li>
 * </ul>
 * </p>
 * <p>Change log 1.2:
 * <ul>
 *   <li>Add static logger.</li>
 * </ul>
 * </p>
 * @author LOY, bannie2492
 * @version 1.2
 */
@RestController
public class SecurityController extends BaseController {

    /**
     * The LOGGER.
     */
    private static final Logger LOGGER = Logger.getLogger(SecurityController.class.getPackage().getName());

    /**
     * The class name.
     */
    private static final String CLASS_NAME = SecurityController.class.getName();

    /**
     * The SecurityService instance.
     *
     * It is initialized with Spring setter dependency injection.
     *
     * It should not be null after initialization.
     */
    @Autowired
    private SecurityService securityService;

    /**
     * Default constructor.
     */
    public SecurityController() {
    }

    /**
     * Check the configuration.
     *
     * @throws ConfigurationException if there's any error in configuration.
     */
    @PostConstruct
    public void checkConfiguration() {
        Helper.checkConfigNotNull(securityService, "securityService");
    }

    /**
     * Search securities.
     *
     * @param criteria the search criteria
     * @return The search result.
     * @throws ServiceException populate the exception from backend
     */
    @RequestMapping(value = "securities",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public SearchResult<Security> search(@ModelAttribute("criteria") SecuritySearchCriteria criteria)
            throws ServiceException {
        final String signature = CLASS_NAME + "#search(SecuritySearchCriteria)";

        // Log entrance
        Helper.logEntrance(LOGGER, signature, new String[] {"criteria"}, new Object[] {criteria});

        SearchResult<Security> result = securityService.search(criteria);
        Helper.logExit(LOGGER, signature, result);
        return result;
    }

    /**
     * Set comment.
     *
     * @param securityDailyValueId the security daily value ID
     * @param comment the comment
     * @throws ServiceException populate the exception from backend
     */
        @RequestMapping(value = "SecurityDailyValues/{securityDailyValueId}/comments",
            method = RequestMethod.POST)
    public void setComment(@PathVariable long securityDailyValueId, @RequestParam String comment)
            throws ServiceException {
        final String signature = CLASS_NAME + "#setComment(long)";

        // Log entrance
        Helper.logEntrance(LOGGER, signature, new String[] {"securityDailyValueId"},
                new Object[] {securityDailyValueId});
        securityService.setComment(securityDailyValueId, comment);
        Helper.logExit(LOGGER, signature, null);
    }

    /**
     * Update security status.
     *
     * @param securityStatus the security status
     * @param securityDailyValueId the security ID.
     * @throws ServiceException populate the exception from backend
     */
    @RequestMapping(value = "SecurityDailyValues/{securityDailyValueId}/securityStatus",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateSecurityStatus(@PathVariable long securityDailyValueId,
            @RequestBody SecurityStatus securityStatus) throws ServiceException {
        final String signature = CLASS_NAME + "#updateSecurityStatus(long, SecurityStatus)";

        // Log entrance
        Helper.logEntrance(LOGGER, signature, new String[] {"securityDailyValueId", "securityStatus"},
                new Object[] {securityDailyValueId, securityStatus});

        securityService.updateSecurityStatus(securityDailyValueId, securityStatus);
        Helper.logExit(LOGGER, signature, null);
    }

    /**
     * Get security comments.
     *
     * @param securityId the security ID
     * @param businessDate the business date
     * @return the comments
     * @throws ServiceException populate the exception from backend
     * @since 1.1
     */
    @RequestMapping(value = "securities/{securityId}/commentHistory",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Comment> getSecurityComments(@PathVariable long securityId, @RequestParam Date businessDate)
            throws ServiceException {
        final String signature = CLASS_NAME + "#getSecurityComments(long)";

        // Log entrance
        Helper.logEntrance(LOGGER, signature, new String[] {"securityId", "businessDate"},
                new Object[] {securityId, businessDate});

        List<Comment> result = securityService.getSecurityComments(securityId, businessDate);
        Helper.logExit(LOGGER, signature, result);
        return result;
    }

    /**
     * Check if a security status is in used.
     * @param securityStatusId the security status id
     * @return true if the security status is in used
     * @throws ServiceException populate the exception from backend
     * @since 1.1
     */
    @RequestMapping(value = "SecurityDailyValues/{securityStatusId}/used",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean checkSecurityStatusUsed(@PathVariable long securityStatusId) throws ServiceException {
        final String signature = CLASS_NAME + "#checkSecurityStatusUsed(long)";

        // Log entrance
        Helper.logEntrance(LOGGER, signature, new String[] {"securityStatusId"},
                new Object[] {securityStatusId});
        boolean result = securityService.isSecurityStatusInUse(securityStatusId);
        Helper.logExit(LOGGER, signature, result);
        return result;
    }

    /**
     * Prepares the search criteria.
     *
     * @return the entity
     */
    @ModelAttribute("criteria")
    public SecuritySearchCriteria getCriteria() {
        return new SecuritySearchCriteria();
    }
}

