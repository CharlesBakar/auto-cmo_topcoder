/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.services.impl;

import cmo.autotool.Helper;
import cmo.autotool.exceptions.EntityNotFoundException;
import cmo.autotool.exceptions.PersistenceException;
import cmo.autotool.exceptions.ServiceException;
import cmo.autotool.models.SecurityStatus;
import cmo.autotool.models.UserRole;
import cmo.autotool.services.LookupService;
import java.util.List;
import javax.transaction.Transactional;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

/**
 * This is the implementation of LookupService.
 *
 * <p>
 * <strong>Thread Safety: </strong>This class effectively thread safe when the following condition is met: this class is
 * initialized by Spring right after construction and its parameters are never changed after that. All entities passed
 * to this class are used by the caller in thread safe manner (accessed from a single thread only).
 * </p>
 *
 * <p>Change log 1.1:
 * <ul>
 *   <li>Update method updateSecurityStatuses for checking update of defaultStatus.</li>
 * </ul>
 * </p>
 * <p>Change log 1.2:
 * <ul>
 *   <li>Refactor the code to lower complexity.</li>
 *   <li>Add static final logger.</li>
 * </ul>
 * </p>
 * @author LOY, bannie2492
 * @version 1.2
 */
@Service
public class LookupServiceImpl extends BasePersistenceService implements LookupService {

    /**
     * The LOGGER.
     */
    private static final Logger LOGGER = Logger.getLogger(LookupServiceImpl.class.getPackage().getName());

    /**
     * The class name.
     */
    private static final String CLASS_NAME = LookupServiceImpl.class.getName();


    /**
     * Empty constructor.
     */
    public LookupServiceImpl() {
    }

    /**
     * Get all security statuses.
     *
     * @throws PersistenceException if some error occurred while accessing the persistence.
     * @throws ServiceException if any other error occurs.
     * @return All security statuses
     */
    public List<SecurityStatus> getAllSecurityStatuses() throws ServiceException {
        final String signature = CLASS_NAME + "#getAllSecurityStatuses()";
        // Log entrance
        Helper.logEntrance(LOGGER, signature, new String[] {}, new Object[] {});

        CMOMapper cmoMapper = getCMOMapper();
        try {
            List<SecurityStatus> result = cmoMapper.getAllSecurityStatuses();
            Helper.logExit(LOGGER, signature, result);
            return result;
        } catch (org.apache.ibatis.exceptions.PersistenceException e) {
            throw Helper.logException(LOGGER, signature,
                    new PersistenceException("BE_DB_ERROR", e));
        }

    }

    /**
     * Get all user roles.
     *
     * @throws PersistenceException if some error occurred while accessing the persistence.
     * @throws ServiceException if any other error occurs.
     * @return All user roles
     */
    public List<UserRole> getAllUserRoles() throws ServiceException {
        final String signature = CLASS_NAME + "#getAllUserRoles()";
        // Log entrance
        Helper.logEntrance(LOGGER, signature, new String[] {}, new Object[] {});

        CMOMapper cmoMapper = getCMOMapper();
        try {
            List<UserRole> result = cmoMapper.getAllUserRoles();
            Helper.logExit(LOGGER, signature, result);
            return result;
        } catch (org.apache.ibatis.exceptions.PersistenceException e) {
            throw Helper.logException(LOGGER, signature,
                    new PersistenceException("BE_DB_ERROR", e));
        }
    }

    /**
     * Update security statuses.
     *
     * @param securityStatuses the security statuses
     * @throws IllegalArgumentException if securityStatuses is null/empty, or contains null.
     * @throws EntityNotFoundException if the entity is not found in persistence.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     * @throws ServiceException if any other error occurs.
     */
    @Transactional
    public void updateSecurityStatuses(List<SecurityStatus> securityStatuses) throws ServiceException {
        final String signature = CLASS_NAME + "#updateSecurityStatuses(List<SecurityStatus>)";

        // Log entrance
        Helper.logEntrance(LOGGER, signature, new String[] {"securityStatuses"}, new Object[] {securityStatuses});
        Helper.checkNullValues(securityStatuses, "securityStatuses");
        CMOMapper cmoMapper = getCMOMapper();
        try {
            List<SecurityStatus> existingSecurityStatuses = getAllSecurityStatuses();
            // check status ID against existing statuses
            checkStatusId(securityStatuses, cmoMapper, existingSecurityStatuses);
            // update the statuses
            updateStatuses(existingSecurityStatuses, securityStatuses, cmoMapper);
            Helper.logExit(LOGGER, signature, null);
        } catch (org.apache.ibatis.exceptions.PersistenceException e) {
            throw Helper.logException(LOGGER, signature,
                    new PersistenceException("BE_DB_ERROR", e));
        }
    }

    /**
     * Update the statuses.
     * @param existingSecurityStatuses statuses existing in DB
     * @param securityStatuses statuses to be updated
     * @param cmoMapper the mybatis mapper
     * @since 1.2
     */
    private void updateStatuses(List<SecurityStatus> existingSecurityStatuses, List<SecurityStatus> securityStatuses,
            CMOMapper cmoMapper) {
        for (SecurityStatus existingSecurityStatus : existingSecurityStatuses) {
            boolean found = false;
            for (SecurityStatus securityStatus : securityStatuses) {
                if (securityStatus.getId() == existingSecurityStatus.getId()) {
                    if (!securityStatus.getName().equals(existingSecurityStatus.getName())
                            || securityStatus.isDefaultStatus() ^ existingSecurityStatus.isDefaultStatus()) {
                        cmoMapper.updateSecurityStatus(securityStatus);
                    }
                    found = true;
                    break;
                }
            }
            if (!found) {
                cmoMapper.deleteSecurityStatus(existingSecurityStatus.getId());
            }
        }
    }

    /**
     * Check ID the status.
     * @param securityStatuses the status
     * @param cmoMapper the mybatis mapper
     * @param existingSecurityStatuses current existing statuses
     * @throws EntityNotFoundException if status can not be found
     * @since 1.2
     */
    private void checkStatusId(List<SecurityStatus> securityStatuses, CMOMapper cmoMapper,
            List<SecurityStatus> existingSecurityStatuses) throws EntityNotFoundException {
        for (SecurityStatus status : securityStatuses) {
            if (status.getId() == 0) {
                cmoMapper.insertSecurityStatus(status);
            } else {
                boolean found = false;
                for (SecurityStatus existingStatus : existingSecurityStatuses) {
                    if (existingStatus.getId() == status.getId()) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    throw new EntityNotFoundException("BE_DATA_LOSS",
                    Helper.logException(LOGGER, "checkStatusId",
                            new EntityNotFoundException("Security status with ID ["
                            + status.getId() + "] can not be found")));
                }
            }
        }
    }
}
