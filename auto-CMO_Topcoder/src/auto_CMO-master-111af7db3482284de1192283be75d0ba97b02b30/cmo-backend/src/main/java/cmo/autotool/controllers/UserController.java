/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.controllers;

import cmo.autotool.Helper;
import cmo.autotool.models.dto.User;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * The controller to expose user related APIs.
 *
 * <p>
 * <strong>Thread Safety: </strong>This class effectively thread safe when the following condition is met: this class is
 * initialized by Spring right after construction and its parameters are never changed after that. All entities passed
 * to this class are used by the caller in thread safe manner (accessed from a single thread only).
 * </p>
 *
 * <p>Change log 1.1:
 * <ul>
 *   <li>Add static logger.</li>
 * </ul>
 * </p>
 * @author LOY, bannie2492
 * @version 1.1
 */
@RestController
public class UserController extends BaseController {

    /**
     * The LOGGER.
     */
    private static final Logger LOGGER = Logger.getLogger(UserController.class.getPackage().getName());

    /**
     * The class name.
     */
    private static final String CLASS_NAME = UserController.class.getName();

    /**
     * Default constructor.
     */
    public UserController() {
    }

    /**
     * Get current user.
     *
     * @param request the http request
     * @return the current user
     */
    @RequestMapping(value = "users/current",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public User getCurrentUser(HttpServletRequest request) {
        final String signature = CLASS_NAME + "#getCurrentUser(HttpServletRequest)";

        // Log entrance
        Helper.logEntrance(LOGGER, signature, new String[] {"request"}, new Object[] {request});

        User result = super.getCurrentUser(request);
        Helper.logExit(LOGGER, signature, result);
        return result;
    }
}

