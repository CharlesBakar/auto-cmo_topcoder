/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.services;

import cmo.autotool.exceptions.EntityNotFoundException;
import cmo.autotool.exceptions.PersistenceException;
import cmo.autotool.exceptions.ServiceException;
import cmo.autotool.models.Security;
import cmo.autotool.models.SecurityStatus;
import cmo.autotool.models.dto.Comment;
import cmo.autotool.models.dto.SearchResult;
import cmo.autotool.models.dto.SecuritySearchCriteria;
import java.util.Date;
import java.util.List;

/**
 * This service provides the operations to manage security.
 *
 * <p>
 * <strong>Thread Safety: </strong>Implementations of this interface should be effectively thread safe when entities
 * passed to them are used by the caller in thread safe manner.
 * </p>
 *
 * <p>Change log 1.1:
 * <ul>
 *   <li>Add methods getSecurityComments and isSecurityStatusInUse.</li>
 * </ul>
 * </p>
 * @author LOY, bannie2492
 * @version 1.1
 */
public interface SecurityService {

    /**
     * Search the securities. Note: if criteria.businessDate is not set, it will be set to last business date by
     * default.
     *
     * @param criteria the search criteria
     * @return the search result.
     * @throws IllegalArgumentException if criteria is null, or criteria.pageNumber &lt; 0, or (criteria.pageSize &ge; 1
     * and criteria.pageNumber &le; 0)
     * @throws PersistenceException if some error occurred while accessing the persistence.
     * @throws ServiceException if any other error occurs.
     */
    public SearchResult<Security> search(SecuritySearchCriteria criteria) throws ServiceException;

    /**
     * Set comment to a security.
     *
     * @param securityDailyValueId the security daily value ID
     * @param comment the comment
     * @throws IllegalArgumentException if securityDailyValueId is not positive.
     * @throws EntityNotFoundException if the entity is not found in persistence.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     * @throws ServiceException if any other error occurs.
     */
    public void setComment(long securityDailyValueId, String comment) throws ServiceException;

    /**
     * Update security status.
     *
     * @param securityStatus the daily value ID
     * @param securityDailyValueId the security status
     * @throws IllegalArgumentException if securityDailyValueId is not positive.
     * @throws EntityNotFoundException if the entity is not found in persistence.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     * @throws ServiceException if any other error occurs.
     */
    public void updateSecurityStatus(long securityDailyValueId, SecurityStatus securityStatus) throws ServiceException;

    /**
     * Get security comments.
     * @param securityId the security id
     * @param businessDate the business date
     * @return the security comments
     * @throws ServiceException if any other error occurs.
     * @throws IllegalArgumentException if securityId is not positive, businessDate is null
     * @since 1.1
     */
    public List<Comment> getSecurityComments(long securityId, Date businessDate) throws ServiceException;

    /**
     * Check if the security status is in used.
     * @param securityStatusId the security status id
     * @return true if the security status is in used
     * @throws IllegalArgumentException if securityStatusId is not positive
     * @throws ServiceException if any other error occurs
     * @since 1.1
     */
    public boolean isSecurityStatusInUse(long securityStatusId) throws ServiceException;
}
