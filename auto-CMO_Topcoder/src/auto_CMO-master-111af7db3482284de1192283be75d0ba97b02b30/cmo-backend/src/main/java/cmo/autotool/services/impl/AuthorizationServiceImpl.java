/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.services.impl;

import cmo.autotool.Helper;
import cmo.autotool.exceptions.PersistenceException;
import cmo.autotool.exceptions.ServiceException;
import cmo.autotool.services.AuthorizationService;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;


/**
 * This is the implementation of AuthorizationService.
 *
 * <p>
 * <strong>Thread Safety: </strong>This class effectively thread safe when the following condition is met: this class is
 * initialized by Spring right after construction and its parameters are never changed after that. All entities passed
 * to this class are used by the caller in thread safe manner (accessed from a single thread only).
 * </p>
 *
 * <p>Change log 1.1:
 * <ul>
 *   <li>Add static logger.</li>
 * </ul>
 * </p>
 * @author LOY, bannie2942
 * @version 1.1
 */
@Service
public class AuthorizationServiceImpl extends BasePersistenceService implements AuthorizationService {

    /**
     * The logger.
     */
    private static final Logger LOGGER = Logger.getLogger(AuthorizationServiceImpl.class.getPackage().getName());

    /**
     * The class name.
     */
    private static final String CLASS_NAME = AuthorizationServiceImpl.class.getName();

    /**
     * Empty constructor.
     */
    public AuthorizationServiceImpl() {
    }

    /**
     * Check authorization.
     *
     * @param action the action
     * @param roleId the role ID
     * @return True if authorization is successful. Otherwise, false.
     * @throws IllegalArgumentException if action is null/empty, roleId is not positive
     * @throws PersistenceException if some error occurred while accessing the persistence.
     * @throws ServiceException if any other error occurs.
     */
    public boolean isAuthorized(String action, long roleId) throws ServiceException {
        final String signature = CLASS_NAME + "#isAuthorized(String, long)";
        // Log entrance
        Helper.logEntrance(LOGGER, signature, new String[] {"action", "roleId"}, new Object[] {action, roleId});

        Helper.checkNullOrEmpty(action, "action");
        Helper.checkPositive(roleId, "roleId");

        CMOMapper cmoMapper = getCMOMapper();
        try {
            boolean result = cmoMapper.countRolePrivilege(action, roleId) > 0;
            Helper.logExit(LOGGER, signature, result);
            return result;
        } catch (org.apache.ibatis.exceptions.PersistenceException e) {
            throw Helper.logException(LOGGER, signature,
                    new PersistenceException("Failed to authorize actions.", e));
        }
    }
}

