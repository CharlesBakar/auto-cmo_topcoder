/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.models;

/**
 * <p>
 * The 99-20 category.
 * </p>
 *
 * <p>
 * <strong>Thread Safety: </strong> This enumeration is immutable and thread safe.
 * </p>
 *
 * @author LOY, bannie2492
 * @version 1.0
 */
public enum Category9920 {
    /**
     * The 99-20 category is "New 99-20".
     */
    NEW_9920,

    /**
     * The 99-20 category is "NOT 99-20".
     */
    NOT_9920,

    /**
     * The 99-20 category is "EXISTING 99-20".
     */
    EXISTING_9920,
}

