/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.services;

import cmo.autotool.exceptions.EntityNotFoundException;
import cmo.autotool.exceptions.PersistenceException;
import cmo.autotool.exceptions.ServiceException;
import cmo.autotool.models.FileDetail;
import java.util.List;

/**
 * This service provides the operations to manage file details.
 *
 * <p>
 * <strong>Thread Safety: </strong>Implementations of this interface should be effectively thread safe when entities
 * passed to them are used by the caller in thread safe manner.
 * </p>
 *
 * @author LOY, bannie2942
 * @version 1.0
 */
public interface FileDetailService {

    /**
     * Create file detail.
     *
     * @param fileDetail the file detail.
     * @return created file detail.
     * @throws IllegalArgumentException if fileDetail is null.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     * @throws ServiceException if any other error occurs.
     */
    public FileDetail create(FileDetail fileDetail) throws ServiceException;

    /**
     * Update file detail.
     *
     * @param fileDetail the file detail.
     * @throws IllegalArgumentException if fileDetail is null.
     * @throws EntityNotFoundException if the entity is not found.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     * @throws ServiceException if any other error occurs.
     */
    public void update(FileDetail fileDetail) throws ServiceException;

    /**
     * Get most recent file details.
     *
     * @return The most recent file details.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     * @throws ServiceException if any other error occurs.
     */
    public List<FileDetail> getFileDetails() throws ServiceException;
}
