/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.services.impl;

import cmo.autotool.Helper;
import cmo.autotool.exceptions.ConfigurationException;
import cmo.autotool.exceptions.EntityNotFoundException;
import cmo.autotool.exceptions.PersistenceException;
import cmo.autotool.exceptions.ServiceException;
import cmo.autotool.models.FileDetail;
import cmo.autotool.models.dto.ServiceContext;
import cmo.autotool.services.FileDetailService;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


/**
 * This is the implementation of FileDetailService.
 *
 * <p>
 * <strong>Thread Safety: </strong>This class effectively thread safe when the following condition is met: this class is
 * initialized by Spring right after construction and its parameters are never changed after that. All entities passed
 * to this class are used by the caller in thread safe manner (accessed from a single thread only).
 * </p>
 *
 * <p>Change log 1.1:
 * <ul>
 *   <li>Add static logger.</li>
 * </ul>
 * </p>
 * @author LOY, bannie2942
 * @version 1.1
 */
@Service
public class FileDetailServiceImpl extends BasePersistenceService implements FileDetailService {

    /**
     * The LOGGER.
     */
    private static final Logger LOGGER = Logger.getLogger(FileDetailServiceImpl.class.getPackage().getName());

    /**
     * The class name.
     */
    private static final String CLASS_NAME = FileDetailServiceImpl.class.getName();


    /**
     * The max count of file details that should be returned.
     */
    @Value("#{maxCount}")
    private int maxCount;

    /**
     * Empty constructor.
     */
    public FileDetailServiceImpl() {
    }

    /**
     * Check the configuration.
     *
     * @throws ConfigurationException if there's any error in configuration.
     */
    @PostConstruct
    public void checkConfiguration() {
        super.checkConfiguration();
        Helper.checkConfigPositive(maxCount, "maxCount");
    }

    /**
     * Create file detail.
     *
     * @param fileDetail the file detail.
     * @return created file detail.
     * @throws IllegalArgumentException if fileDetail is null.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     * @throws ServiceException if any other error occurs.
     */
    public FileDetail create(FileDetail fileDetail) throws ServiceException {
        final String signature = CLASS_NAME + "#create(FileDetail)";

        // Log entrance
        Helper.logEntrance(LOGGER, signature, new String[] {"fileDetail"}, new Object[] {fileDetail});

        Helper.checkNull(fileDetail, "fileDetail");

        CMOMapper cmoMapper = getCMOMapper();
        final Date today = new Date();
        final String user = ServiceContext.getUser().getCorpId();
        fileDetail.setCreatedTS(today);
        fileDetail.setCreatedByNM(user);
        fileDetail.setUpdatedTS(today);
        fileDetail.setUpdatedByNM(user);
        try {
            cmoMapper.insertFileDetail(fileDetail);
            Helper.logExit(LOGGER, signature, fileDetail);
            return fileDetail;
        } catch (org.apache.ibatis.exceptions.PersistenceException e) {
            throw Helper.logException(LOGGER, signature,
                    new PersistenceException("BE_DB_ERROR", e));
        }
    }

    /**
     * Update file detail.
     *
     * @param fileDetail the file detail.
     * @throws IllegalArgumentException if fileDetail is null.
     * @throws EntityNotFoundException if the entity is not found.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     * @throws ServiceException if any other error occurs.
     */
    @Transactional
    public void update(FileDetail fileDetail) throws ServiceException {
        final String signature = CLASS_NAME + "#update(FileDetail)";

        // Log entrance
        Helper.logEntrance(LOGGER, signature, new String[] {"fileDetail"}, new Object[] {fileDetail});

        Helper.checkNull(fileDetail, "fileDetail");

        CMOMapper cmoMapper = getCMOMapper();
        try {
            fileDetail.setUpdatedTS(new Date());
            fileDetail.setUpdatedByNM(ServiceContext.getUser().getCorpId());
            int result = cmoMapper.updateFileDetail(fileDetail);
            if (result == 0) {
                throw new EntityNotFoundException("FileDetail with id [" + fileDetail.getId() + "] can not be found");
            }
            Helper.logExit(LOGGER, signature, null);
        } catch (org.apache.ibatis.exceptions.PersistenceException e) {
            throw Helper.logException(LOGGER, signature,
                    new PersistenceException("BE_DB_ERROR", e));
        }
    }

    /**
     * Get most recent file details.
     *
     * @return The most recent file details.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     * @throws ServiceException if any other error occurs.
     */
    public List<FileDetail> getFileDetails() throws ServiceException {
        final String signature = CLASS_NAME + "#getFileDetails()";

        // Log entrance
        Helper.logEntrance(LOGGER, signature, new String[] {}, new Object[] {});

        CMOMapper cmoMapper = getCMOMapper();
        try {
            List<FileDetail> result = cmoMapper.getFileDetails(maxCount);
            Helper.logExit(LOGGER, signature, result);
            return result;
        } catch (org.apache.ibatis.exceptions.PersistenceException e) {
            throw Helper.logException(LOGGER, signature,
                    new PersistenceException("BE_DB_ERROR", e));
        }
    }
}

