/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.exceptions;

/**
 * <p>
 * This exception is the base exception for other checked exceptions.
 * </p>
 *
 * <p>
 * <strong>Thread Safety: </strong> Exception is not thread safe because its base class is not thread safe.
 * </p>
 *
 * @author LOY, bannie2492
 * @version 1.0
 */
public class ServiceException extends Exception {

    /**
     * <p>
     * Constructs a new <code>ServiceException</code> instance with error message.
     * </p>
     *
     * @param message the error message.
     */
    public ServiceException(String message) {
        super(message);
    }

    /**
     * <p>
     * Constructs a new <code>ServiceException</code> instance with error message and inner cause.
     * </p>
     *
     * @param message the error message.
     * @param cause the inner cause.
     */
    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
