/*
 * Copyright (C) 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.services.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

/**
 * This is a mock implementation of user service. Further update should be made when the SSO feature is available.
 *
 * <p>Change log 1.1:
 * <ul>
 *   <li>Remove debug code.</li>
 * </ul>
 * </p>
 * @author bannie2492
 * @version 1.1
 */
public class MockSiteMinderUserService implements
        AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken> {

    /**
     * Load the user detail and create mock user data.
     *
     * @param token the authentication token
     * @return the user data
     * @throws UsernameNotFoundException not going to be thrown in this mock implementation
     */
    @Override
    public UserDetails loadUserDetails(PreAuthenticatedAuthenticationToken token)
            throws UsernameNotFoundException {
        String corpid = token.getName();
        String role;
        switch (corpid) {
            case "001":
                role = "Admin";
                break;
            case "002":
                role = "Analyst";
                break;
            default:
                role = "Guest";
                break;
        }
        List<GrantedAuthority> list = new ArrayList<>();
        list.add(new GrantedAuthorityImpl(role));
        UserDetails user = new User(corpid, "", list);
        return user;
    }
}
