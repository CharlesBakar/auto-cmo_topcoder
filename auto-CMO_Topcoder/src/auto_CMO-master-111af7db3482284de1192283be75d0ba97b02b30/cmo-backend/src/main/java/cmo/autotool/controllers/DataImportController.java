/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.controllers;

import cmo.autotool.Helper;
import cmo.autotool.exceptions.ConfigurationException;
import cmo.autotool.exceptions.ServiceException;
import cmo.autotool.services.DataImportService;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;
import javax.annotation.PostConstruct;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * The controller to expose data import related APIs.
 *
 * <p>
 * <strong>Thread Safety: </strong>This class effectively thread safe when the following condition is met: this class is
 * initialized by Spring right after construction and its parameters are never changed after that. All entities passed
 * to this class are used by the caller in thread safe manner (accessed from a single thread only).
 * </p>
 *
 * <p>Change log 1.1:
 * <ul>
 *   <li>Add static logger.</li>
 * </ul>
 * </p>
 * @author LOY, bannie2492
 * @version 1.1
 */
@RestController
public class DataImportController extends BaseController {

    /**
     * The LOGGER.
     */
    private static final Logger LOGGER = Logger.getLogger(DataImportController.class.getPackage().getName());

    /**
     * The class name.
     */
    private static final String CLASS_NAME = DataImportController.class.getName();

    /**
     * The DataImportService instance.
     *
     * It is initialized with Spring setter dependency injection.
     *
     * It should not be null after initialization.
     */
    @Autowired
    private DataImportService dataImportService;

    /**
     * The input file folder.
     *
     * It is initialized with Spring setter dependency injection.
     *
     * Valid value: Not null/empty.
     */
    @Value("#{inputFileFolder}")
    private String inputFileFolder;

    /**
     * Default constructor.
     */
    public DataImportController() {
    }

    /**
     * Check the configuration.
     *
     * @throws ConfigurationException if there's any error in configuration.
     */
    @PostConstruct
    public void checkConfiguration() {
        Helper.checkConfigNotNull(dataImportService, "dataImportService");
        Helper.checkConfigNotEmpty(inputFileFolder, "inputFileFolder");
        Helper.checkFolder(inputFileFolder, "inputFileFolder");
    }

    /**
     * Import data.
     *
     * @param multipartFile the multipart file to import
     * @param businessDate the business date
     * @throws ServiceException populate the exception from backend or if failed to transfer file
     */
    @RequestMapping(value = "dataImport/import",
            method = RequestMethod.POST)
    public void importData(@RequestParam MultipartFile multipartFile, @RequestParam Date businessDate)
            throws ServiceException {
        final String signature = CLASS_NAME + "#importData(MultipartFile)";

        // Log entrance
        Helper.logEntrance(LOGGER, signature, new String[]{"multipartFile"}, new Object[]{multipartFile});

        // save file
        File file = new File(inputFileFolder, UUID.randomUUID().toString() + ".csv");
        try {
            multipartFile.transferTo(file);
        } catch (IOException e) {
            throw Helper.logException(LOGGER, signature, new ServiceException("BE_IMPORT_FILE_FAILED", e));
        }

        // import
        dataImportService.importData(file.getAbsolutePath(), businessDate);
        Helper.logExit(LOGGER, signature, null);
    }
}
