/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.jobs;

import org.quartz.spi.TriggerFiredBundle;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;

/**
 * <p>
 * This is the job factory that auto wires beans to the jobs.
 * </p>
 *
 * <p>
 * <strong>Thread Safety: </strong> This class is effectively thread safe (injected configurations are not considered as
 * thread safety factor). Refer to ADS 1.3.4 for general assumptions and notes on thread safety.
 * </p>
 *
 * @author bannie2492
 * @version 1.0
 */
public final class AutowiringSpringBeanJobFactory extends SpringBeanJobFactory implements
    ApplicationContextAware {

    /**
     * The bean factory.
     */
    private transient AutowireCapableBeanFactory beanFactory;

    /**
     * Set the application context.
     * @param context the application context
     */
    @Override
    public void setApplicationContext(final ApplicationContext context) {
        beanFactory = context.getAutowireCapableBeanFactory();
    }

    /**
     * Create the job instance.
     * @param bundle the trigger fired bundle
     * @return the job instance
     * @throws Exception if there is any error
     */
    @Override
    protected Object createJobInstance(final TriggerFiredBundle bundle) throws Exception {
        final Object job = super.createJobInstance(bundle);
        beanFactory.autowireBean(job);
        return job;
    }
}
