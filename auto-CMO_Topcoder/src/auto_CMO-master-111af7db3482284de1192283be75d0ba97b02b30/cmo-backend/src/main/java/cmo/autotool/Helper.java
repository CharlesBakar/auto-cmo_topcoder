/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool;

import cmo.autotool.exceptions.ConfigurationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.StringWriter;
import java.util.Collection;
import org.apache.log4j.Logger;

/**
 * Class Helper.
 * It's a helper class that provides basic validation/logging methods.
 *
 * <p>Change log 1.1:
 * <ul>
 *   <li>Add exception detail when mapper failed to parse an object.</li>
 * </ul>
 * </p>
 * @author bannie2492
 * @version 1.1
 */
public class Helper {

    /**
     * <p>
     * Represents the entrance message.
     * </p>
     */
    private static final String MESSAGE_ENTRANCE = "Entering method %1$s.";

    /**
     * <p>
     * Represents the exit message.
     * </p>
     */
    private static final String MESSAGE_EXIT = "Exiting method %1$s.";

    /**
     * <p>
     * Represents the error message.
     * </p>
     */
    private static final String MESSAGE_ERROR = "Error in method %1$s. Details:";

    /**
     * The object mapper.
     */
    private static final ObjectMapper MAPPER = new ObjectMapper();

    /**
     * The string meaning not applicable.
     */
    public static final String NA = "N/A";

    /**
     * Prevent from initialization.
     */
    private Helper() {

    }

    /**
     * Check if a file path represents a folder.
     * @param path the file path
     * @param name the variable name
     * @throws IllegalArgumentException if the file path is not a folder
     */
    public static void checkFolder(String path, String name) throws IllegalArgumentException {
        File folder = new File(path);
        Helper.checkConfigState(folder.exists(), name + " does not exist");
        Helper.checkConfigState(folder.isDirectory(), name + " should be a directory");
    }

    /**
     * It checks whether a given object is null.
     *
     * @param object the object to be checked
     * @param name the name of the object, used in the exception message
     * @throws IllegalArgumentException the exception thrown when the object is null
     */
    public static void checkNull(Object object, String name) throws IllegalArgumentException {
        if (object == null) {
            throw new IllegalArgumentException("" + name + " is not provided");
        }
    }

    /**
     * It checks whether a given string is null or empty.
     *
     * @param str the string to be checked
     * @param name the name of the object, used in the exception message
     * @throws IllegalArgumentException the exception thrown when the object is null
     */
    public static void checkNullOrEmpty(String str, String name) throws IllegalArgumentException {
        if (str == null || str.trim().isEmpty()) {
            throw new IllegalArgumentException("" + name + " is not provided");
        }
    }

    /**
     * It checks whether a given collection is null or contains null elements.
     *
     * @param collection the collection to be checked
     * @param name the name of the collection
     * @param <T> the type of the elements in the collection
     * @throws IllegalArgumentException the exception thrown when the collection is null or contains null elements
     */
    public static <T> void checkNullValues(Collection<T> collection, String name) throws IllegalArgumentException {
        checkNull(collection, "the collection " + name);
        if (collection.isEmpty()) {
            throw new IllegalArgumentException(name + " should not be an empty list.");
        }
        for (T element : collection) {
            checkNull(element, "the element of the collection " + name);
        }
    }

    /**
     * Check if the value is positive.
     * @param value the value
     * @param name the name
     * @throws IllegalArgumentException if the value is not positive
     */
    public static void checkPositive(long value, String name) {
        if (value <= 0) {
            throw new IllegalArgumentException(name + " should be a positive value.");
        }
    }

    /**
     * Check if the state is true.
     * @param state the state
     * @param message the error message if the state is not true
     * @throws IllegalArgumentException if the state is not true
     */
    public static void checkState(boolean state, String message) {
        if (!state) {
            throw new IllegalArgumentException(message);
        }
    }

    /**
     * Check if the configuration state is true.
     * @param state the state
     * @param message the error message if the state is not true
     * @throws ConfigurationException if the state is not true
     */
    public static void checkConfigState(boolean state, String message) {
        if (!state) {
            throw new ConfigurationException(message);
        }
    }

    /**
     * Check if the configuration is null or not.
     * @param object the object
     * @param name the name
     * @throws ConfigurationException if the state is not true
     */
    public static void checkConfigNotNull(Object object, String name) {
        if (object == null) {
            throw new ConfigurationException(String.format("%s should be provided", name));
        }
    }

    /**
     * Check if the configuration is positive.
     * @param object the object
     * @param name the name
     * @throws ConfigurationException if the state is not true
     */
    public static void checkConfigPositive(long object, String name) {
        if (object <= 0) {
            throw new ConfigurationException(String.format("%s should not be positive", name));
        }
    }

    /**
     * Check if the configuration is positive.
     * @param object the object
     * @param name the name
     * @throws ConfigurationException if the state is not true
     */
    public static void checkConfigPositive(double object, String name) {
        if (object <= 0) {
            throw new ConfigurationException(String.format("%s should not be positive", name));
        }
    }

    /**
     * Check if the configuration string is not empty.
     * @param value the string value
     * @param name the name
     * @throws ConfigurationException if the state is not true
     */
    public static void checkConfigNotEmpty(String value, String name) {
        checkConfigNotNull(value, name);
        if (value.trim().isEmpty()) {
            throw new ConfigurationException(String.format("%s should not be empty", name));
        }
    }

    /**
     * <p>
     * Logs for entrance into public methods at <code>DEBUG</code> level.
     * </p>
     *
     * @param logger the log service.
     * @param signature the signature.
     * @param paramNames the names of parameters to log (not Null).
     * @param params the values of parameters to log (not Null).
     */
    public static void logEntrance(Logger logger, String signature, String[] paramNames, Object[] params) {
        if (logger == null) {
            // No logging
            return;
        }

        String msg = String.format(MESSAGE_ENTRANCE, signature) + toString(paramNames, params);
        if (logger.isDebugEnabled()) {
            logger.debug(msg);
        }
    }

    /**
     * <p>
     * Logs for exit from public methods at <code>DEBUG</code> level.
     * </p>
     *
     * @param logger the log service.
     * @param signature the signature of the method to be logged.
     * @param value the return value to log.
     */
    public static void logExit(Logger logger, String signature, Object value) {
        if (logger == null) {
            // No logging
            return;
        }

        String msg = String.format(MESSAGE_EXIT, signature);
        if (value != null) {
            msg += " Output parameter : " + toString(value);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(msg);
        }
    }

    /**
     * <p>
     * Logs the given exception and message at <code>ERROR</code> level.
     * </p>
     *
     * @param <T> the exception type.
     * @param logger the log service.
     * @param signature the signature of the method to log.
     * @param e the exception to log.
     * @return the passed in exception.
     */
    public static <T extends Throwable> T logException(Logger logger, String signature, T e) {
        if (logger != null) {
            StringWriter sw = new StringWriter();
            if (logger.isDebugEnabled()) {
                logger.debug(String.format(MESSAGE_ERROR, signature + " " + sw.toString()));
            }
        }
        return e;
    }

    /**
     * <p>
     * Converts the parameters to string.
     * </p>
     *
     * @param paramNames the names of parameters.
     * @param params the values of parameters.
     * @return the string
     */
    private static String toString(String[] paramNames, Object[] params) {
        StringBuilder sb = new StringBuilder(" Input parameters: {");
        if (params != null) {
            for (int i = 0; i < params.length; i++) {
                if (i > 0) {
                    sb.append(", ");
                }
                sb.append(paramNames[i]).append(":").append(toString(params[i]));
            }
        }
        sb.append("}.");
        return sb.toString();
    }

    /**
     * <p>
     * Converts the object to string.
     * </p>
     *
     * @param obj the object
     * @return the string representation of the object.
     */
    private static String toString(Object obj) {
        String result;
        try {
            result = MAPPER.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            result = "The object can not be serialized by Jackson JSON mapper, error: " + e.getMessage();
        }
        return result;
    }
}
