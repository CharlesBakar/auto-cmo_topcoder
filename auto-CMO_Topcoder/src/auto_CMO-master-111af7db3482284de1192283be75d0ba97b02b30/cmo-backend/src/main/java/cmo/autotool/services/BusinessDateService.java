/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.services;

import cmo.autotool.exceptions.PersistenceException;
import cmo.autotool.exceptions.ServiceException;
import java.util.Date;
import java.util.List;


/**
 * This service provides the operations to manage business dates.
 *
 * <p>
 * <strong>Thread Safety: </strong>Implementations of this interface should be effectively thread safe when entities
 * passed to them are used by the caller in thread safe manner.
 * </p>
 *
 * @author LOY, bannie2942
 * @version 1.0
 */
public interface BusinessDateService {

    /**
     * Get last business date.
     *
     * @return The last business date.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     * @throws ServiceException if any other error occurs.
     */
    public Date getLastBusinessDate() throws ServiceException;

    /**
     * Get the business dates.
     *
     * @param startDate the start date
     * @param endDate the end date
     * @return The business dates between start date and end date.
     * @throws IllegalArgumentException if startDate is null, or endDate is null.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     * @throws ServiceException if any other error occurs.
     */
    public List<Date> getBusinessDates(Date startDate, Date endDate) throws ServiceException;
}

