/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.services.impl;

import cmo.autotool.Helper;
import cmo.autotool.exceptions.PersistenceException;
import cmo.autotool.exceptions.ServiceException;
import cmo.autotool.services.BusinessDateService;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

/**
 * This is the implementation of BusinessDateService.
 *
 * <p>
 * <strong>Thread Safety: </strong>This class effectively thread safe when the following condition is met: this class is
 * initialized by Spring right after construction and its parameters are never changed after that. All entities passed
 * to this class are used by the caller in thread safe manner (accessed from a single thread only).
 * </p>
 *
 * <p>Change log 1.1:
 * <ul>
 *   <li>Add static logger.</li>
 * </ul>
 * </p>
 * @author LOY, bannie2942
 * @version 1.1
 */
@Service
public class BusinessDateServiceImpl extends BasePersistenceService implements BusinessDateService {

    /**
     * The logger.
     */
    private static final Logger LOGGER = Logger.getLogger(BusinessDateServiceImpl.class.getPackage().getName());

    /**
     * The class name.
     */
    private static final String CLASS_NAME = BusinessDateServiceImpl.class.getName();

    /**
     * Empty constructor.
     */
    public BusinessDateServiceImpl() {
    }

    /**
     * Get last business date.
     *
     * @return The last business date.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     * @throws ServiceException if any other error occurs.
     */
    public Date getLastBusinessDate() throws ServiceException {
        final String signature = CLASS_NAME + "#getLastBusinessDate()";
        // Log entrance
        Helper.logEntrance(LOGGER, signature, new String[] {}, new Object[] {});
        CMOMapper cmoMapper = getCMOMapper();
        Date currentDate = new Date();
        try {
            Date result = cmoMapper.getLastBusinessDate(currentDate);
            Helper.logExit(LOGGER, signature, result);
            return result;
        } catch (org.apache.ibatis.exceptions.PersistenceException e) {
            throw Helper.logException(LOGGER, signature,
                    new PersistenceException("BE_DB_ERROR", e));
        }
    }

    /**
     * Get the business dates.
     *
     * @param startDate the start date
     * @param endDate the end date
     * @return The business dates between start date and end date.
     * @throws IllegalArgumentException if startDate is null, or endDate is null. or start date is later than end date.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     * @throws ServiceException if any other error occurs.
     */
    public List<Date> getBusinessDates(Date startDate, Date endDate) throws ServiceException {
        final String signature = CLASS_NAME + "#getBusinessDates(Date, Date)";
        // Log entrance
        Helper.logEntrance(LOGGER, signature, new String[] {"startDate", "endDate"}, new Object[] {startDate, endDate});

        Helper.checkNull(startDate, "startDate");
        Helper.checkNull(endDate, "endDate");
        Helper.checkState(!startDate.after(endDate), "BE_BUSINESS_DATE_RANGE_ERROR");

        CMOMapper cmoMapper = getCMOMapper();
        try {
            List<Date> result = cmoMapper.getBusinessDates(startDate, endDate);
            Helper.logExit(LOGGER, signature, result);
            return result;
        } catch (org.apache.ibatis.exceptions.PersistenceException e) {
            throw Helper.logException(LOGGER, signature,
                    new PersistenceException("BE_DB_ERROR", e));
        }
    }
}
