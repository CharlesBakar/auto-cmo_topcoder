/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.models.dto;

import cmo.autotool.models.UserRole;

/**
 * <p>
 * The user.
 * </p>
 *
 * <p>
 * <strong>Thread Safety: </strong> This class is not thread safe because it is mutable.
 * </p>
 *
 * @author LOY, bannie2942
 * @version 1.0
 */
public class User {

    /**
     * The corp ID.
     */
    private String corpId;

    /**
     * The role.
     */
    private UserRole role;

    /**
     * Default constructor.
     */
    public User() {
    }

    /**
     * Set corpId.
     *
     * @param corpId the corpId to be set
     */
    public void setCorpId(String corpId) {
        this.corpId = corpId;
    }

    /**
     * Get corpId.
     *
     * @return the corpId
     */
    public String getCorpId() {
        return corpId;
    }

    /**
     * Set role.
     *
     * @param role the role to be set
     */
    public void setRole(UserRole role) {
        this.role = role;
    }

    /**
     * Get role.
     *
     * @return the role
     */
    public UserRole getRole() {
        return role;
    }
}
