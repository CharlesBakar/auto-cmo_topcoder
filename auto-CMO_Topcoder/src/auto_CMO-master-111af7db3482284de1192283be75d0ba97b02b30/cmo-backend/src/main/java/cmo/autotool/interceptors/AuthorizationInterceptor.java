/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.interceptors;

import cmo.autotool.Helper;
import cmo.autotool.exceptions.ConfigurationException;
import cmo.autotool.exceptions.ServiceException;
import cmo.autotool.models.UserRole;
import cmo.autotool.models.dto.ServiceContext;
import cmo.autotool.models.dto.User;
import cmo.autotool.services.AuthorizationService;
import cmo.autotool.services.LookupService;
import java.io.IOException;
import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * The interceptor to check authorization.
 *
 * <p>
 * <strong>Thread Safety: </strong>This class effectively thread safe when the following condition is met: this class is
 * initialized by Spring right after construction and its parameters are never changed after that. All entities passed
 * to this class are used by the caller in thread safe manner (accessed from a single thread only).
 * </p>
 *
 * <p>Change log 1.1:
 * <ul>
 *   <li>Remove mock implementation.</li>
 *   <li>Add static logger.</li>
 * </ul>
 * </p>
 * @author LOY, bannie2492
 * @version 1.1
 */
public class AuthorizationInterceptor extends HandlerInterceptorAdapter {

    /**
     * The LOGGER.
     */
    private static final Logger LOGGER = Logger.getLogger(AuthorizationInterceptor.class.getPackage().getName());

    /**
     * The class name.
     */
    private static final String CLASS_NAME = AuthorizationInterceptor.class.getName();

    /**
     * The LookupService instance.
     *
     * It is initialized with Spring setter dependency injection.
     *
     * It should not be null after initialization.
     */
    @Autowired
    private LookupService lookupService;

    /**
     * The ActionPermissionService instance.
     *
     * It is initialized with Spring setter dependency injection.
     *
     * It should not be null after initialization.
     */
    @Autowired
    private AuthorizationService authorizationService;

    /**
     * Default constructor.
     */
    public AuthorizationInterceptor() {
    }

    /**
     * Check the configuration.
     *
     * @throws ConfigurationException if there's any error in configuration.
     */
    @PostConstruct
    public void checkConfiguration() {
        Helper.checkConfigNotNull(lookupService, "lookupService");
        Helper.checkConfigNotNull(authorizationService, "authorizationService");
    }

    /**
     * Check authorization.
     *
     * @param response the response
     * @param request the request
     * @param handler the handler
     * @return return true if the user is authenticated/authorized and false otherwise
     */
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        final String signature = CLASS_NAME + "#preHandle(request, response, handler)";
        // Log entrance
        Helper.logEntrance(LOGGER, signature, new String[] {"request", "response", "handler"},
                new Object[] {Helper.NA, Helper.NA, Helper.NA});

        boolean result = true;
        // Get current corp ID
        String currentCorpId = request.getHeader("CORPID");
        UserRole currentUserRole = null;
        try {
            // check authentication
            if (currentCorpId == null) {
                result = false;
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                response.getWriter().write("BE_UNAUTHENTICATED");
            } else {
                // get current user role
                for (UserRole userRole : lookupService.getAllUserRoles()) {
                    if (request.isUserInRole(userRole.getName())) {
                        currentUserRole = userRole;
                        break;
                    }
                }
                if (currentUserRole == null) {
                    result = false;
                    response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                    response.getWriter().write("BE_NOT_VALID_ROLE");
                } else {
                    // check authorization
                    String action = request.getRequestURI().replace(request.getContextPath(), "");
                    if (!authorizationService.isAuthorized(action, currentUserRole.getId())) {
                        result = false;
                        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                        response.getWriter().write("BE_ACTION_NOT_ALLOWED");
                    }
                }
            }
            if (result) {
                // save current user in session
                User currentUser = new User();
                currentUser.setCorpId(currentCorpId);
                currentUser.setRole(currentUserRole);
                request.getSession().setAttribute("currentUser", currentUser);

                // save current user in ServiceContext
                ServiceContext.setUser(currentUser);
            }
        } catch (IOException | ServiceException e) {
            Helper.logException(LOGGER, signature, e);
            result = false;
        }
        // Log exit
        Helper.logExit(LOGGER, signature, result);
        return result;
    }
}

