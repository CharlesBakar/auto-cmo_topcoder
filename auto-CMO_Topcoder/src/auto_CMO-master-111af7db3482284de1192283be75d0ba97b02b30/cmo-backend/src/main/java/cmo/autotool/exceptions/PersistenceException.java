/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.exceptions;

/**
 * <p>
 * This exception is thrown if error occurs when persisting data.
 * </p>
 *
 * <p>
 * <strong>Thread Safety: </strong> Exception is not thread safe because its base class is not thread safe.
 * </p>
 *
 * @author LOY, bannie2492
 * @version 1.0
 */
public class PersistenceException extends ServiceException {

    /**
     * <p>
     * Constructs a new <code>PersistenceException</code> instance with error message.
     * </p>
     *
     * @param message the error message.
     */
    public PersistenceException(String message) {
        super(message);
    }

    /**
     * <p>
     * Constructs a new <code>PersistenceException</code> instance with error message and inner cause.
     * </p>
     *
     * @param message the error message.
     * @param cause the inner cause.
     */
    public PersistenceException(String message, Throwable cause) {
        super(message, cause);
    }
}
