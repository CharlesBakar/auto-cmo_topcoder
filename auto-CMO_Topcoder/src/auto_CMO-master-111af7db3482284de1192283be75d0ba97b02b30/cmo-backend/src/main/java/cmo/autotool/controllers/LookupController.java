/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.controllers;

import cmo.autotool.Helper;
import cmo.autotool.exceptions.ConfigurationException;
import cmo.autotool.exceptions.ServiceException;
import cmo.autotool.models.SecurityStatus;
import cmo.autotool.services.LookupService;
import java.util.List;
import javax.annotation.PostConstruct;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * The controller to expose lookup related APIs.
 *
 * <p>
 * <strong>Thread Safety: </strong>This class effectively thread safe when the following condition is met: this class is
 * initialized by Spring right after construction and its parameters are never changed after that. All entities passed
 * to this class are used by the caller in thread safe manner (accessed from a single thread only).
 * </p>
 *
 * <p>Change log 1.1:
 * <ul>
 *   <li>Add static logger.</li>
 * </ul>
 * </p>
 * @author LOY, bannie2492
 * @version 1.1
 */
@RestController
public class LookupController extends BaseController {

    /**
     * The LOGGER.
     */
    private static final Logger LOGGER = Logger.getLogger(LookupController.class.getPackage().getName());

    /**
     * The class name.
     */
    private static final String CLASS_NAME = LookupController.class.getName();

    /**
     * The LookupService instance.
     *
     * It is initialized with Spring setter dependency injection.
     *
     * It should not be null after initialization.
     */
    @Autowired
    private LookupService lookupService;

    /**
     * Default constructor.
     *
     * Impl Notes:
     * do nothing
     */
    public LookupController() {
    }

    /**
     * Check the configuration.
     *
     * @throws ConfigurationException if there's any error in configuration.
     */
    @PostConstruct
    public void checkConfiguration() {
        Helper.checkConfigNotNull(lookupService, "lookupService");
    }

    /**
     * Get all security statuses.
     *
     * @return All security statuses
     * @throws ServiceException populate the exception from backend
     */
    @RequestMapping(value = "lookup/securityStatuses",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<SecurityStatus> getAllSecurityStatuses() throws ServiceException {
        final String signature = CLASS_NAME + "#getAllSecurityStatuses()";

        // Log entrance
        Helper.logEntrance(LOGGER, signature, new String[] {}, new Object[] {});

        List<SecurityStatus> result = lookupService.getAllSecurityStatuses();
        Helper.logExit(LOGGER, signature, result);
        return result;
    }

    /**
     * Update security statuses.
     *
     * @param securityStatuses the security statuses
     * @throws ServiceException populate the exception from backend
     */
    @RequestMapping(value = "lookup/securityStatuses",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateSecurityStatuses(@RequestBody List<SecurityStatus> securityStatuses) throws ServiceException {
        final String signature = CLASS_NAME + "#updateSecurityStatuses(List<SecurityStatus>)";

        // Log entrance
        Helper.logEntrance(LOGGER, signature, new String[] {"securityStatuses"}, new Object[] {securityStatuses});

        lookupService.updateSecurityStatuses(securityStatuses);
        Helper.logExit(LOGGER, signature, null);
    }
}

