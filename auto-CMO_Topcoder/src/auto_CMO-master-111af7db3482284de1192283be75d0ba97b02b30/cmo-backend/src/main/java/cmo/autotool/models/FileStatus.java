/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.models;

/**
 * <p>
 * The file status.
 * </p>
 *
 * <p>
 * <strong>Thread Safety: </strong> This enumeration is immutable and thread safe.
 * </p>
 *
 * @author LOY, bannie2492
 * @version 1.0
 */
public enum FileStatus {
    /**
     * The file status is "Success".
     */
    SUCCESS,

    /**
     * The file status is "Failed".
     */
    FAILED,

    /**
     * The file status is "Processing".
     */
    PROCESSING
}

