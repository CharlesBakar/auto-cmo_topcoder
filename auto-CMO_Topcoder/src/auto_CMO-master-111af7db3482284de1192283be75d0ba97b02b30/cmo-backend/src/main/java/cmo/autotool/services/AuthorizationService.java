/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.services;

import cmo.autotool.exceptions.PersistenceException;
import cmo.autotool.exceptions.ServiceException;

/**
 * This service provides the operations to check authorization.
 *
 * <p>
 * <strong>Thread Safety: </strong>Implementations of this interface should be effectively thread safe when entities
 * passed to them are used by the caller in thread safe manner.
 * </p>
 *
 * @author LOY, bannie2942
 * @version 1.0
 */
public interface AuthorizationService {

    /**
     * Check authorization.
     *
     * @param action the action
     * @param roleId the role ID
     * @return True if authorization is successful. Otherwise, false.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     * @throws ServiceException if any other error occurs.
     */
    public boolean isAuthorized(String action, long roleId) throws ServiceException;
}
