/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.services.impl;

import cmo.autotool.models.CMOSetting;
import cmo.autotool.models.FileDetail;
import cmo.autotool.models.Security;
import cmo.autotool.models.SecurityDailyValue;
import cmo.autotool.models.SecurityDetail;
import cmo.autotool.models.SecurityStatus;
import cmo.autotool.models.UserRole;
import cmo.autotool.models.dto.Comment;
import cmo.autotool.models.dto.SecuritySearchCriteria;
import java.util.Date;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.exceptions.PersistenceException;

/**
 * This interface represents a MyBatis mapper that defines operations. Each method corresponds to a separate SQL query
 * (or a set of SELECT queries) or DML statement to be executed.
 *
 * Note that, this architecture doesn't define any implementations of this interface, instead its implementation is
 * defined dynamically by MyBatis during the runtime.
 *
 * <p>Change log 1.1:
 * <ul>
 *   <li>Add methods getSecurityComments and getSecurityStatusUsedCount.</li>
 * </ul>
 * </p>
 * @author LOY, bannie2492
 * @version 1.1
 */
public interface CMOMapper {

    /**
     * Insert a security.
     *
     * @param security the security
     * @throws PersistenceException if some error occurred while accessing the persistence.
     */
    public void insertSecurity(Security security);

    /**
     * Get market value prices.
     *
     * @param businessDate the businessDate
     * @param securityId the security ID
     * @return The market value prices.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     */
    public List<Double> getMarketValuePrices(@Param("securityId") long securityId,
            @Param("businessDate") Date businessDate);

    /**
     * Delete security details for the given business date.
     *
     * @param businessDate the businessDate
     * @throws PersistenceException if some error occurred while accessing the persistence.
     */
    public void deleteSecurityDetails(Date businessDate);

    /**
     * Delete security daily values for the given business date.
     *
     * @param businessDate the businessDate
     * @throws PersistenceException if some error occurred while accessing the persistence.
     */
    public void deleteSecurityDailyValues(Date businessDate);

    /**
     * Insert security details.
     *
     * @param securityDetails the security details
     * @throws PersistenceException if some error occurred while accessing the persistence.
     */
    public void insertSecurityDetails(@Param("details") List<SecurityDetail> securityDetails);

    /**
     * Insert security daily values.
     *
     * @param securityDailyValues the security daily values.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     */
    public void insertSecurityDailyValues(@Param("values") List<SecurityDailyValue> securityDailyValues);

    /**
     * Get all securities.
     *
     * @return All securities.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     */
    public List<Security> getAllSecurities();

    /**
     * Search securities.
     *
     * @param criteria the search criteria.
     * @return The matched items.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     */
    public List<Security> searchSecurities(SecuritySearchCriteria criteria);

    /**
     * Get security details by given security ID and business date.
     *
     * @param businessDate the business date.
     * @param securityId the security ID.
     * @return The security details.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     */
    public List<SecurityDetail> getSecurityDetails(@Param("securityId") long securityId,
            @Param("businessDate") Date businessDate);

    /**
     * Count the matched securities. The pagination parameters will be ignored.
     *
     * @param criteria the search criteria.
     * @return The count of matched items.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     */
    public int countSecurities(SecuritySearchCriteria criteria);

    /**
     * Update comment.
     *
     * @param securityDailyValueId the security daily value ID
     * @param comment the comment
     * @param date the update date
     * @param user the update user
     * @return the effected row count.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     */
    public int updateComment(@Param("securityDailyValueId") long securityDailyValueId,
            @Param("comment") String comment, @Param("date") Date date, @Param("user") String user);

    /**
     * Update security status.
     *
     * @param securityStatus the security status
     * @param securityDailyValueId the security daily value ID
     * @param date the update date
     * @param user the update user
     * @return the effected row count.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     */
    public int updateSecurityDailyValueSecurityStatus(@Param("securityDailyValueId") long securityDailyValueId,
            @Param("securityStatus") SecurityStatus securityStatus,
            @Param("date") Date date, @Param("user") String user);

    /**
     * Get the last business date that is previous to the given date.
     *
     * @param date the date
     * @return The last business date that is previous to the given date.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     */
    public Date getLastBusinessDate(Date date);

    /**
     * Get the business dates.
     *
     * @param startDate the start date
     * @param endDate the end date
     * @return The business dates between start date and end date.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     */
    public List<Date> getBusinessDates(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

    /**
     * Get the current CMO setting.
     *
     * @return The current CMO setting.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     */
    public CMOSetting getCurrentCMOSetting();

    /**
     * Insert a CMO setting.
     *
     * @param cmoSetting the CMO setting.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     */
    public void insertCMOSetting(CMOSetting cmoSetting);

    /**
     * Insert a file detail.
     *
     * @param fileDetail the file detail.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     */
    public void insertFileDetail(FileDetail fileDetail);

    /**
     * Update a file detail.
     *
     * @param fileDetail the file detail.
     * @return the effected row count.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     */
    public int updateFileDetail(FileDetail fileDetail);

    /**
     * Get file details.
     *
     * @param maxCount the max count of file details that should be returned.
     * @return The file details.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     */
    public List<FileDetail> getFileDetails(int maxCount);

    /**
     * Count the role privilege with given action and role ID.
     *
     * @param action the action
     * @param roleId the role ID
     * @return The file details.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     */
    public int countRolePrivilege(@Param("action") String action, @Param("roleId") long roleId);

    /**
     * Get all security statuses.
     *
     * @return All security statuses.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     */
    public List<SecurityStatus> getAllSecurityStatuses();

    /**
     * Get all user roles.
     *
     * @return All user roles
     * @throws PersistenceException if some error occurred while accessing the persistence.
     */
    public List<UserRole> getAllUserRoles();

    /**
     * Insert a security status.
     *
     * @param securityStatus the security status.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     */
    public void insertSecurityStatus(SecurityStatus securityStatus);

    /**
     * Update a security status.
     *
     * @param securityStatus the security status.
     * @return the effected row count
     * @throws PersistenceException if some error occurred while accessing the persistence.
     */
    public int updateSecurityStatus(SecurityStatus securityStatus);

    /**
     * Delete a security status.
     *
     * @param securityStatusId the security status ID.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     */
    public void deleteSecurityStatus(long securityStatusId);

    /**
     * Get security status by ID.
     *
     * @param id the id
     * @return the security status
     * @throws PersistenceException if some error occurred while accessing the persistence.
     */
    public SecurityStatus getSecurityStatus(@Param("id") long id);

    /**
     * Get security comments.
     *
     * @param id the security id
     * @param date the business date
     * @param count the max count of the comments
     * @return the list of security comments
     * @throws PersistenceException if some error occurred while accessing the persistence.
     * @since 1.1
     */
    public List<Comment> getSecurityComments(@Param("id") long id, @Param("date") Date date,
            @Param("maxCount") int count);
    
    /**
     * Get how many security daily values are using the specified security status.
     * @param id the security status id
     * @return the count of security daily value
     * @throws PersistenceException if some error occurred while accessing the persistence.
     * @since 1.1
     */
    public int getSecurityStatusUsedCount(@Param("id") long id);
}
