/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.services;

import cmo.autotool.exceptions.PersistenceException;
import cmo.autotool.exceptions.ServiceException;
import java.util.Date;

/**
 * This service provides the operations to import data.
 *
 * <p>
 * <strong>Thread Safety: </strong>Implementations of this interface should be effectively thread safe when entities
 * passed to them are used by the caller in thread safe manner.
 * </p>
 *
 * @author LOY, bannie2942
 * @version 1.0
 */
public interface DataImportService {

    /**
     * Import the data from the input file.
     *
     * @param filePath the path of input file
     * @param businessDate the business date
     * @throws IllegalArgumentException if filePath is null/empty, or businessDate is null.
     * @throws PersistenceException if error occurred during access the database persistence
     * @throws ServiceException if any other error occurred during the operation
     */
    public void importData(String filePath, Date businessDate) throws ServiceException;
}
