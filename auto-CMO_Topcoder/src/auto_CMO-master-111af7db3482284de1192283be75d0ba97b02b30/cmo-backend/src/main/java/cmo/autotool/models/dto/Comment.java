/*
 * Copyright (C) 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.models.dto;

import java.util.Date;

/**
 * <p>
 * The comment of a security daily value.
 * </p>
 *
 * <p>
 * <strong>Thread Safety: </strong> This class is not thread safe because it is mutable.
 * </p>
 *
 * @author LOY, bannie2492
 * @version 1.0
 */
public class Comment {

    /**
     * The date of the security daily value.
     */
    private Date businessDate;

    /**
     * The comment text.
     */
    private String commentText;

    /**
     * Default constructor.
     */
    public Comment() {
    }

    /**
     * Get businessDate.
     *
     * @return the businessDate
     */
    public Date getBusinessDate() {
        return businessDate;
    }

    /**
     * Set businessDate.
     *
     * @param businessDate the businessDate to be set
     */
    public void setBusinessDate(Date businessDate) {
        this.businessDate = businessDate;
    }

    /**
     * Get commentText.
     *
     * @return the commentText
     */
    public String getCommentText() {
        return commentText;
    }

    /**
     * Set commentText.
     *
     * @param commentText the commentText to be set
     */
    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

}
