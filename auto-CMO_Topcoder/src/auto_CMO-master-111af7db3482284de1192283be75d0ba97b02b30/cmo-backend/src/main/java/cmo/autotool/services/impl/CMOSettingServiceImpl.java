/*
 * Copyright (C) 2015 - 2016 TopCoder Inc., All Rights Reserved.
 */
package cmo.autotool.services.impl;

import cmo.autotool.Helper;
import cmo.autotool.exceptions.ConfigurationException;
import cmo.autotool.exceptions.PersistenceException;
import cmo.autotool.exceptions.ServiceException;
import cmo.autotool.models.CMOSetting;
import cmo.autotool.services.CMOSettingService;
import java.util.Date;
import javax.annotation.PostConstruct;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * This is the implementation of CMOSettingService.
 *
 * <p>
 * <strong>Thread Safety: </strong>This class effectively thread safe when the following condition is met: this class is
 * initialized by Spring right after construction and its parameters are never changed after that. All entities passed
 * to this class are used by the caller in thread safe manner (accessed from a single thread only).
 * </p>
 *
 * <p>Change log 1.1:
 * <ul>
 *   <li>Add static logger.</li>
 * </ul>
 * </p>
 * @author LOY, bannie2942
 * @version 1.1
 */
@Service
public class CMOSettingServiceImpl extends BasePersistenceService implements CMOSettingService {

    /**
     * The logger.
     */
    private static final Logger LOGGER = Logger.getLogger(CMOSettingServiceImpl.class.getPackage().getName());

    /**
     * The class name.
     */
    private static final String CLASS_NAME = CMOSettingServiceImpl.class.getName();

    /**
     * The default limit value.
     *
     * It is initialized with Spring setter dependency injection.
     *
     * It should be positive
     */
    @Value("#{defaultMarketValueThreshold}")
    private double defaultMarketValueThreshold;

    /**
     * Empty constructor.
     */
    public CMOSettingServiceImpl() {
    }

    /**
     * Check the configuration.
     *
     * @throws ConfigurationException if there's any error in configuration.
     */
    @PostConstruct
    public void checkConfiguration() {
        super.checkConfiguration();
        Helper.checkConfigPositive(defaultMarketValueThreshold, "defaultMarketValueThreshold");
    }

    /**
     * Get current CMO setting.
     *
     * @return The current CMO setting.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     * @throws ServiceException if any other error occurs.
     */
    public CMOSetting getCurrentCMOSetting() throws ServiceException {
        final String signature = CLASS_NAME + "#getCurrentCMOSetting()";
        // Log entrance
        Helper.logEntrance(LOGGER, signature, new String[] {}, new Object[] {});

        CMOMapper cmoMapper = getCMOMapper();
        try {
            CMOSetting result = cmoMapper.getCurrentCMOSetting();
            if (result == null) {
                result = new CMOSetting();
                result.setMarketValueThreshold(defaultMarketValueThreshold);

            }
            Helper.logExit(LOGGER, signature, result);
            return result;

        } catch (org.apache.ibatis.exceptions.PersistenceException e) {
            throw Helper.logException(LOGGER, signature,
                    new PersistenceException("BE_DB_ERROR", e));
        }

    }

    /**
     * Set CMO setting.
     *
     * @param cmoSetting the CMO setting
     * @throws IllegalArgumentException if cmoSetting is null.
     * @throws PersistenceException if some error occurred while accessing the persistence.
     * @throws ServiceException if any other error occurs.
     */
    public void setCMOSetting(CMOSetting cmoSetting) throws ServiceException {
        final String signature = CLASS_NAME + "#setCMOSetting(CMOSetting)";
        // Log entrance
        Helper.logEntrance(LOGGER, signature, new String[] {"cmoSetting"}, new Object[] {cmoSetting});

        Helper.checkNull(cmoSetting, "cmoSetting");
        CMOMapper cmoMapper = getCMOMapper();
        try {
            cmoSetting.setTimestamp(new Date());
            cmoMapper.insertCMOSetting(cmoSetting);
            Helper.logExit(LOGGER, signature, null);
        } catch (org.apache.ibatis.exceptions.PersistenceException e) {
            throw Helper.logException(LOGGER, signature,
                    new PersistenceException("BE_DB_ERROR", e));
        }
    }
}

