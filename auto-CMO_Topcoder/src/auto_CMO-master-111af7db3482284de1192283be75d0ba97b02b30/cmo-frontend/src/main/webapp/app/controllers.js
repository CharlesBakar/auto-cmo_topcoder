/*
 * Copyright (C) 2016 TopCoder Inc., All Rights Reserved.
 *
 * The angular controller.
 * Change log 1.1:
 * - fix #66 After clicking "Expand All' it must be disabled; same for "Collapse" as well
 * - fix #32 Field value should be clear if apply filter after click on cusip
 * - fix #10 No option to reset the calender
 * - fix #47 "you did not specify the file" is displayed when we select invalid file in IE11
 * @author LOY, moulyg, bannie2492
 * @version 1.1
 */

/* global angular, $ */
(function () {
    'use strict';

    var appControllers = angular.module('CMO.controllers', ['CMO.services']);


    // dashboard page controller
    appControllers.controller('homeCtrl',
            function ($scope, CMOSettingService, BusinessDateService, SecurityService, DataImportService, FileDetailService, LookupService, UserService, $rootScope, $routeParams, $location, $http, config) {
                // page sizes
                $scope.pageSizes = config.PAGE_SIZES;
                // mock user corpId
                if ($routeParams.role === 'ReadOnly') {
                    $http.defaults.headers.common['CORPID'] = '003';
                } else if ($routeParams.role === 'Admin') {
                    $http.defaults.headers.common['CORPID'] = '001';
                } else {
                    $http.defaults.headers.common['CORPID'] = '002';
                }
                // load count
                $scope.loadCount = [0, 0];
                // items perpage for monitor tab
                $scope.itemsByPage = config.itemByPage;
                // items perpage for all security tab
                $scope.itemsByPageSecurity = config.itemByPage;
                // default search criteria
                $scope.searchCriteria = {};
                // search result for monitoring data
                $scope.monitoringResult = [''];
                // search result for all securities
                $scope.allResult = [];
                // the table state of monitoring tab
                $scope.monitoringTableState = {};
                // the table state of all securities
                $scope.allSecurityTableState = {};
                // file detail
                $scope.fileDetals = [];
                // security statuses read only
                $scope.securityStatuses = [];
                // editable security statuses
                $scope.editableStatuses = [];
                // last business date
                $scope.lastBusinessDate = '';
                // the import date
                $scope.importDate = '';
                // the configuration data of the top menu
                $scope.top = {};
                // the import file
                $scope.file = {};
                // the filename
                $scope.filename = '';
                // flag to open the comment data
                $rootScope.commentTable = true;
                // user data
                $scope.user = {};
                // flag indicating the monitor tab is open
                $scope.monitorTab = true;
                // the comment history data in the right panel
                $scope.comments = [];
                // current CUSIP shown in the right panel
                $scope.currentCusip = '';
                // current security detail shown in the right panel
                $scope.currentDetail = {};
                // flag to indicate the side bar is open
                $scope.sideBar = true;
                // the business dates
                $scope.businessDates = [];
                // the error messages displayed to the user.
                $scope.errorMsgs = ERROR_MSGS;
                
                // get all business dates
                function addZero(value) {
                    if (value < 10) {
                        value = '0' + value;
                    }
                    return value;
                }
                var today = new Date();
                var month = today.getMonth() + 1;
                var year = today.getFullYear();
                month++;
                if (month === 13) {
                    year++;
                    month = 1;
                }
                var lastDate = year + '/' + addZero(month) + '/01';
                var firstDate = '1990/01/01';
                BusinessDateService.getBusinessDates(firstDate, lastDate).then(function (data) {
                    $scope.businessDates = data;
                    $('a.ui-datepicker-prev').trigger('click');
                    $('a.ui-datepicker-next').trigger('click');
                }, errorHandler);

                // get current user information
                UserService.getCurrentUser().then(function (data) {
                    $scope.user = data;
                }, errorHandler);


                // load the security statuses
                $scope.getSecurityStatuses = function () {
                    LookupService.getAllSecurityStatuses().then(function (data) {
                        $scope.securityStatuses = data;
                        $scope.editableStatuses = angular.copy(data);
                        for (var i = 0; i < data.length; i++) {
                            if (data[i].defaultStatus) {
                                $scope.defaultStatus = i;
                                break;
                            }
                        }

                    }, errorHandler);
                };
                $scope.getSecurityStatuses();


                // get file detail
                FileDetailService.getFileDetail().then(function (data) {
                    $scope.fileDetails = data;
                }, errorHandler);
                // get current cmo setting
                $scope.getCurrentCMOSetting = function () {
                    CMOSettingService.getCurrentCMOSetting().then(function (data) {
                        $scope.cmoSetting = data;
                        $scope.editThreshold = angular.copy(data.marketValueThreshold);
                        $scope.cmoSettingTime = 'Default';
                        if ($scope.cmoSetting.timestamp) {
                            $scope.cmoSettingTime = helper.dateToStr($scope.cmoSetting.timestamp);
                        }
                    }, errorHandler);
                };
                $scope.getCurrentCMOSetting();
                // change search criteria
                $scope.updateCriteria = function () {
                    if (parseInt($scope.searchCriteria.marketValueFrom) > parseInt($scope.searchCriteria.marketValueTo)) {
                        $scope.showError(ERROR_MSGS.FILTER_MARKET_VALUE_RANGE_NOT_CORRECT);
                        return;
                    }
                    $scope.top.filter = false;
                    $scope.monitorTab = true;
                    $scope.monitoringTableState.pagination.start = 0;
                    $scope.allSecurityTableState.pagination.start = 0;
                    $scope.updateCalendarDate();
                };

                BusinessDateService.getLastBusinessDate().then(function (data) {
                    $scope.lastBusinessDate = helper.dateToStr(data);
                    $scope.importDate = $scope.lastBusinessDate;
                    $scope.top.calendardate = $scope.importDate;
                }, errorHandler);
                // update cmo setting
                $scope.updateLimit = function (threshold) {
                    CMOSettingService.setCMOSetting({
                        marketValueThreshold: threshold
                    }).then(function (data) {
                        $scope.getCurrentCMOSetting();
                    }, errorHandler);
                };

                // search security for monitoring
                $scope.searchMonitor = function (state) {
                    $scope.monitoringTableState = state;
                    var criteria = buildSearchCriteria(state, true);
                    if ($scope.top.calendardate) {
                        criteria.businessDate = $scope.top.calendardate;
                    }
                    SecurityService.search(criteria).then(function (data) {
                        $scope.monitoringResult = data.items;
                        if ($scope.loadCount[0] < 2) {
                            $scope.loadCount[0]++;
                            $scope.monitoringResult[0].expand = true;
                        } else {
                            $rootScope.collapsedAll();
                        }
                        state.pagination.numberOfPages = data.totalPages;
                        state.pagination.totalItemCount = data.totalRecords;
                        $scope.comments = [];
                        $scope.currentCusip = '';
                        $scope.currentDetail = {};
                    }, errorHandler);
                };

                // search security for all securities
                $scope.searchAll = function (state) {
                    $scope.allSecurityTableState = state;
                    var criteria = buildSearchCriteria(state, false);
                    if ($scope.top.calendardate) {
                        criteria.businessDate = $scope.top.calendardate;
                    }
                    SecurityService.search(criteria).then(function (data) {
                        $scope.allResult = data.items;
                        if ($scope.loadCount[1] < 2) {
                            $scope.loadCount[1]++;
                            $scope.allResult[0].expand = true;
                        } else {
                            $rootScope.collapsedAll();
                        }
                        state.pagination.numberOfPages = data.totalPages;
                        state.pagination.totalItemCount = data.totalRecords;
                    }, errorHandler);
                };
                // update comment
                $scope.updateComment = function (event, securityDailyValue, cusip) {
                    var text = $.trim($(event.target).parent('td').prev('td').find('input').val());
                    var p = $(event.target).parent('td').prev('td').find('p');
                    SecurityService.setComment(securityDailyValue.id, text).then(function () {
                        $scope.loadCommentData(securityDailyValue.securityId, securityDailyValue.businessDate, cusip);
                        p.html(text);
                    }, errorHandler);
                };

                // update security status
                $scope.updateSecurityStatus = function (id, statusId) {
                    SecurityService.updateSecurityStatus(id, {id: statusId}).then(function () {
                    }, errorHandler);
                };


                /* expand All */
                $rootScope.expandAll = function () {
                    if ($scope.monitorTab) {
                        angular.forEach($scope.monitoringResult, function (value) {
                            value.expand = true;
                        });
                        var tab = $('#monitor-tab');
                    } else {
                        angular.forEach($scope.allResult, function (value) {
                            value.expand = true;
                        });
                        var tab = $('#all-tab');
                    }
                    tab.find('.expand-all-link').hide();
                    tab.find('.expand-all-text').show().removeClass('hidden');
                    tab.find('.collapse-all-link').show();
                    tab.find('.collapse-all-text').hide();
                };

                /* collapsed All */
                $rootScope.collapsedAll = function () {
                    
                    if ($scope.monitorTab) {
                        angular.forEach($scope.monitoringResult, function (value) {
                            value.expand = false;
                        });
                        var tab = $('#monitor-tab');
                    } else {
                        angular.forEach($scope.allResult, function (value) {
                            value.expand = false;
                        });
                        var tab = $('#all-tab');
                    }
                    tab.find('.collapse-all-link').hide();
                    tab.find('.collapse-all-text').show().removeClass('hidden');
                    tab.find('.expand-all-link').show();
                    tab.find('.expand-all-text').hide();
                };
                
                // expand or collapse a single row
                $scope.expandSingle = function(row, result) {
                    row.expand = !row.expand;
                    var expandAll = true;
                    var collapseAll = true;
                    var found = false;
                    for (var i = 0; i < result.length; i++) {
                        if (result[i].expand) {
                            found = true;
                            break;
                        }
                    }
                    collapseAll = found;
                    var found2 = false;
                    for (var i = 0; i < result.length; i++) {
                        if (!result[i].expand) {
                            found2 = true;
                            break;
                        }
                    }
                    expandAll = found2;
                    if ($scope.monitorTab) {
                        var tab = $('#monitor-tab');
                    } else {
                        var tab = $('#all-tab');
                    }
                    if (expandAll) {
                        tab.find('.expand-all-link').show();
                        tab.find('.expand-all-text').hide();
                    } else {
                        tab.find('.expand-all-link').hide();
                        tab.find('.expand-all-text').show().removeClass('hidden');
                    }
                    if (collapseAll) {
                        tab.find('.collapse-all-link').show();
                        tab.find('.collapse-all-text').hide();
                    } else {
                        tab.find('.collapse-all-link').hide();
                        tab.find('.collapse-all-text').show().removeClass('hidden');
                    }
                };

                /* show status popup */
                $scope.showStatusPopup = function (e) {
                    e.stopPropagation();
                    $scope.statusModal = true;
                    $scope.popupStatuses = angular.copy($scope.statuses);
                    $scope.popupPrimaryStatus = angular.copy($scope.primaryStatus);
                    angular.forEach($scope.popupStatuses, function (value, key) {
                        value.edit = false;
                    });
                };
                
                $scope.resetCalendarDate = function() {
                    if ($scope.top.calendardate != $scope.lastBusinessDate) {
                        $scope.top.calendardate = $scope.lastBusinessDate;
                        $scope.updateCalendarDate();
                    } else {
                        $scope.top.calendar = false;
                    }
                };

                /* close status popup */
                $scope.closeStatusPopup = function () {
                    $scope.statusModal = false;
                    angular.forEach($scope.editableStatuses, function (value, key) {
                        value.edit = false;
                    });
                    $scope.editableStatuses = angular.copy($scope.securityStatuses);
                };

                /* save status */
                $scope.saveStatus = function () {
                    var found = false;
                    for (var i in $scope.editableStatuses) {
                        var status = $scope.editableStatuses[i];
                        if (!$.trim(status.name)) {
                            $scope.showError(ERROR_MSGS.EDIT_STATUS_NAME_EMPTY);
                            return;
                        }
                        if (status.defaultStatus) {
                            found = true;
                        }
                    }
                    if (!found) {
                        $scope.showError(ERROR_MSGS.EDIT_STATUS_DEFAULT_STATUS_MISSING);
                        return;
                    }
                    LookupService.updateSecurityStatuses($scope.editableStatuses).then(function () {
                        $scope.getSecurityStatuses();
                        angular.forEach($scope.editableStatuses, function (value, key) {
                            value.edit = false;
                        });
                        $scope.statusModal = false;
                        $scope.securityStatuses = angular.copy($scope.editableStatuses);
                        $scope.updateCalendarDate();
                    }, function (error) {
                        if (error.indexOf('UNIQUE_SECURITY_STATUS_NAME') !== -1) {
                            $scope.showError(ERROR_MSGS.EDIT_STATUS_DUPLICATE_NAME);
                        } else {
                            errorHandler(error);
                        }
                    });

                };

                /* add new status */
                $scope.addStatus = function () {
                    $scope.editableStatuses.push({
                        name: "",
                        edit: true,
                        defaultStatus: false
                    });
                };

                /* clicking on the primary status radio button */
                $scope.checkPrimary = function (status) {
                    for (var i in $scope.editableStatuses) {
                        $scope.editableStatuses[i].defaultStatus = false;
                    }
                    status.defaultStatus = true;
                };

                /* remove  status */
                $scope.removeStatus = function (statusId) {
                    if (!statusId) {
                        for (var i = 0; i < $scope.editableStatuses.length; i++) {
                            var status = $scope.editableStatuses[i];
                            if (status.id === statusId) {
                                $scope.editableStatuses.splice(i, 1);
                                return;
                            }
                        }
                    }
                    SecurityService.checkSecurityStatusUsed(statusId).then(function (data) {
                        if (data) {
                            $scope.showError(ERROR_MSGS.EDIT_STATUS_DELETE_WHEN_IN_USE);
                            return;
                        }
                        for (var i = 0; i < $scope.editableStatuses.length; i++) {
                            var status = $scope.editableStatuses[i];
                            if (status.id === statusId) {
                                $scope.editableStatuses.splice(i, 1);
                                break;
                            }
                        }
                    }, errorHandler);
                };

                /* load Side Bar Data */
                $scope.loadCommentData = function (securityId, date, cusip) {
                    $scope.sideBar = true;
                    $rootScope.commentTable = true;
                    $scope.currentCusip = cusip;
                    /* load data */
                    SecurityService.getSecurityComments(securityId, helper.dateToStr(date)).then(function (data) {
                        $scope.comments = data;
                    }, errorHandler);
                };

                /* loading security detail data when clicking on the CUSIP of the security detail row */
                $scope.loadDetailData = function (detail) {
                    $scope.sideBar = true;
                    $rootScope.commentTable = false;
                    
                    $scope.currentDetail = angular.copy(detail);
                };
                /* clicking on the view button of the calendar */
                $scope.updateCalendarDate = function () {
                    $scope.top.calendar = false;
                    $scope.searchMonitor($scope.monitoringTableState);
                    $scope.searchAll($scope.allSecurityTableState);
                };

                /* file upload */
                $scope.upload = function () {
                    $('#upload').trigger('click');
                };

                // check security data exist
                $scope.checkDataExist = function () {
                    if (!$scope.filename) {
                        $scope.showError(ERROR_MSGS.IMPORT_FILE_NOT_SPECIFIED);
                        return;
                    }
                    $scope.top.import = false;
                    var date = $scope.importDate;
                    BusinessDateService.getBusinessDates(date, date).then(function (data) {
                        if (data.length) {
                            $scope.top.warning = true;
                        } else {
                            $scope.submit();
                            $scope.businessDates.push((new Date(date)).getTime());
                        }

                    }, errorHandler);
                };
                /* when selecting a import file from the file system */
                $scope.uploadFile = function (files) {
                    
                    $scope.filename = '';
                    $scope.file = {};
                    if (files.length > 0) {
                        var parts = files[0].name.split('.');
                        if (angular.lowercase(parts[parts.length - 1]) !== 'csv') {
                            $scope.top.import = false;
                            $scope.showError(ERROR_MSGS.IMPORT_FILE_NOT_CSV_FILE);
                            $('#upload').val('');
                        } else {
                            $scope.filename = files[0].name;
                            $scope.file = files[0];
                        }
                        try {
                            $scope.$digest();
                        } catch (e) {
                            // ignore the error of when $apply is called on IE
                        }
                    }
                };
                /* submit the import file to the backend */
                $scope.submit = function () {
                    $scope.top.warning = false;
                    DataImportService.import($scope.importDate, $scope.file).then(function () {
                        $scope.filename = '';
                        $scope.file = {};
                        FileDetailService.getFileDetail().then(function (data) {
                            $scope.fileDetails = data;
                        }, errorHandler);
                        $scope.top.calendardate = $scope.importDate;
                        $scope.searchMonitor($scope.monitoringTableState);
                        $scope.searchAll($scope.allSecurityTableState);
                        $('a.ui-datepicker-prev').trigger('click');
                        $('a.ui-datepicker-next').trigger('click');
                        BusinessDateService.getLastBusinessDate().then(function (data) {
                            $scope.lastBusinessDate = helper.dateToStr(data);
                        }, errorHandler);
                    }, errorHandler);
                };

                // check if the role name is guest
                $scope.checkRoleGuest = function () {
                    try {
                        return $scope.user.role.name === config.guestRoleName;
                    } catch (e) {
                        // ignore when the user is not set yet
                    }
                };

                /* logout */
                $scope.logout = function () {
                    localStorage.removeItem('user');
                    $location.path('/login');
                };
                /* open the datepicker of the import file feature */
                $scope.datePickerTrigger = function () {
                    $('#importDate').datepicker("show");
                };

                /* show error dialog */
                $scope.showError = function (message) {
                    $scope.errorText = message;
                    $scope.error = true;
                };

                /* the common AJAX error handler */
                var errorHandler = function (error) {
                    if (error.length > 30) {
                        $scope.showError(ERROR_MSGS.GENERAL_ERROR_MESSAGE);
                    } else {
                        if (ERROR_MSGS[error]) {
                            $scope.showError(ERROR_MSGS[error]);
                        } else {
                            $scope.showError(error);
                        }
                    }
                };

                /**
                 * Build the search criteria/
                 * @param {Object} state the table state
                 * @param {Object} monitor flag indicating the monitor tab is used
                 * @returns {Object} the criteria object
                 */
                function buildSearchCriteria(state, monitor) {
                    var page = state.pagination;
                    var criteria = {
                        pageNumber: page.start / page.number + 1,
                        pageSize: page.number
                    };
                    if (!criteria.pageNumber) {
                        criteria = {
                            pageNumber: 1,
                            pageSize: 15
                        };
                    }
                    if (page.number === 99) {
                        criteria.pageNumber = 0;
                        criteria.pageSize = 0;
                    }
                    if (monitor) {
                        criteria.eligibleForMonitoring = true;
                    }
                    if (state.sort.predicate) {
                        criteria.sortBy = state.sort.predicate;
                        if (state.sort.reverse) {
                            criteria.sortType = 'DESCENDING';
                        }
                    }
                    if ($.trim($scope.searchCriteria.keyword)) {
                        criteria.keyword = $.trim($scope.searchCriteria.keyword);
                    }
                    if ($.trim($scope.searchCriteria.cusip)) {
                        criteria.cusip = $.trim($scope.searchCriteria.cusip);
                    }
                    if ($.trim($scope.searchCriteria.daysStale)) {
                        criteria.daysStale = $.trim($scope.searchCriteria.daysStale);
                    }
                    if ($.trim($scope.searchCriteria.marketValueFrom)) {
                        criteria.marketValueFrom = $.trim($scope.searchCriteria.marketValueFrom);
                    }
                    if ($.trim($scope.searchCriteria.marketValueTo)) {
                        criteria.marketValueTo = $.trim($scope.searchCriteria.marketValueTo);
                    }
                    var categories9920 = [];
                    if ($scope.searchCriteria.new9920) {
                        categories9920.push('NEW_9920');
                    }
                    if ($scope.searchCriteria.not9920) {
                        categories9920.push('NOT_9920');
                    }
                    if ($scope.searchCriteria.existing9920) {
                        categories9920.push('EXISTING_9920');
                    }
                    if (categories9920.length) {
                        criteria.categories9920 = categories9920;
                    }
                    return criteria;
                }
            });


    // login page controller
    appControllers.controller('loginCtrl',
            function ($scope, $location, $route) {
                /* normalUser */
                $scope.normalUser = function () {
                    $location.path('/dashboard/Normal');
                };
                /* adminUser */
                $scope.adminUser = function () {
                    $location.path('/dashboard/Admin');
                };

                /* readOnlyUser */
                $scope.readOnlyUser = function () {
                    $location.path('/dashboard/ReadOnly');
                };

            });


})();
