/*
 * Copyright (C) 2016 TopCoder Inc., All Rights Reserved.
 *
 * The services of the application.
 * @author LOY, bannie2492
 * @version 1.0
 */

/* global angular, $ */
(function () {
    'use strict';

    var appServices = angular.module('CMO.services', []);

    // perform GET request.
    function get($http, $q, url) {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: config.REST_SERVICE_BASE_URL + url
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject(data);
        });
        return deferred.promise;
    }
    // perform POST request.
    function post($http, $q, url, body) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: config.REST_SERVICE_BASE_URL + url,
            data: JSON.stringify(body)
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject(data);
        });
        return deferred.promise;
    }
    
    appServices.factory('DataImportService', ['$http', '$q', function ($http, $q) {

            return {
                import: function (date, file) {
                    var fd = new FormData();
                    fd.append('multipartFile', file);
                    var deferred = $q.defer();
                    $http.post(config.REST_SERVICE_BASE_URL + 'dataImport/import?businessDate=' + date, fd,
                            {
                                transformRequest: angular.identity,
                                headers: {
                                    'Content-Type': undefined
                                }
                            }
                    ).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                }
            };
        }]);

    appServices.factory('CMOSettingService', ['$http', '$q', function ($http, $q) {

            return {
                getCurrentCMOSetting: function () {
                    return get($http, $q, 'CMOSettings/current');
                },
                setCMOSetting: function(data) {
                    return post($http, $q, 'CMOSettings', data);
                }
            };
        }]);

    appServices.factory('SecurityService', ['$http', '$q', function ($http, $q) {
            return {
                search: function (criteria) {
                    return get($http, $q, 'securities?' + helper.serialize(criteria));
                },
                setComment: function(securityDailyValueId, text) {
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: config.REST_SERVICE_BASE_URL + 'SecurityDailyValues/' + securityDailyValueId + '/comments',
                        data: $.param({
                            comment: text
                        }), headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                    return deferred.promise;
                },
                updateSecurityStatus: function(securityDailyValueId, securityStatus) {
                    return post($http, $q, 'SecurityDailyValues/' + securityDailyValueId + '/securityStatus', securityStatus);
                }
                ,
                getSecurityComments: function(securityId, date) {
                    return get($http, $q, 'securities/' + securityId + '/commentHistory?businessDate=' + date);
                },
                checkSecurityStatusUsed: function(securityStatusId) {
                    return get($http, $q, 'SecurityDailyValues/' + securityStatusId + '/used');
                },
                checkSecurityDailyValueExists: function(date) {
                    return get($http, $q, 'SecurityDailyValues/exist?businessDate=' + date);
                }
            };
        }]);

    appServices.factory('BusinessDateService', ['$http', '$q', function ($http, $q) {

            return {
                getLastBusinessDate: function () {
                    return get($http, $q, 'businessDates/last');
                },
                getBusinessDates: function(startDate, endDate) {
                    return get($http, $q, 'businessDates?startDate=' + startDate + '&endDate=' + endDate);
                }
            };
        }]);
    appServices.factory('FileDetailService', ['$http', '$q', function ($http, $q) {

            return {
                getFileDetail: function () {
                    return get($http, $q, 'fileDetails');
                }
            };
        }]);

    appServices.factory('LookupService', ['$http', '$q', function ($http, $q) {
            return {
                getAllSecurityStatuses: function () {
                    return get($http, $q, 'lookup/securityStatuses');
                },
                updateSecurityStatuses: function(statuses) {
                    return post($http, $q, 'lookup/securityStatuses', statuses);
                }
            };
        }]);

    appServices.factory('UserService', ['$http', '$q', function ($http, $q) {

            return {
                getCurrentUser: function () {
                    return get($http, $q, 'users/current');
                }
            };
        }]);

})();
