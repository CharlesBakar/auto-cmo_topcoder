/*
 * Copyright (C) 2016 TopCoder Inc., All Rights Reserved.
 *
 * The configuration variables of the application.
 * @author TheKingOfWrong
 * @version 1.0
 */

var config = {};
// the REST service base URL
config.REST_SERVICE_BASE_URL = 'http://localhost:8080/cmo/';
// item per page
config.itemByPage = 15;
// the name of the role Guest
config.guestRoleName = 'Guest';
