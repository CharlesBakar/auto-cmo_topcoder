/*
 * Copyright (C) 2016 TopCoder Inc., All Rights Reserved.
 *
 * The angular main application.
 * @author LOY, moulyg, bannie2492
 * @version 1.0
 */

/* global angular */
(function () {
    'use strict';
    angular.module('CMO', [
        'ngRoute',
        'ngSanitize',
        'offClick',
        'smart-table',
        'CMO.directive',
        'CMO.controllers'
    ]).config(['$routeProvider', function ($routeProvider) {
            $routeProvider
                    .when('/dashboard/:role', {
                        templateUrl: 'partials/dashboard.html',
                        controller: 'homeCtrl'
                    })
                    .when('/login', {
                        templateUrl: 'partials/login.html',
                        controller: 'loginCtrl'
                    })
                    .otherwise({redirectTo: '/login'});
        }]).config(['$httpProvider', function ($httpProvider) {
            //initialize get if not there
            if (!$httpProvider.defaults.headers.get) {
                $httpProvider.defaults.headers.get = {};
            }
            //disable IE ajax request caching
            $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
            $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
            $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
        }]).constant('config', {
        PAGE_SIZES: [
            {
                "id": 5,
                "value": 5
            },
            {
                "id": 10,
                "value": 10
            },
            {
                "id": 15,
                "value": 15
            },
            {
                "id": 99,
                "value": "All"
            }
        ]
    });


})();
