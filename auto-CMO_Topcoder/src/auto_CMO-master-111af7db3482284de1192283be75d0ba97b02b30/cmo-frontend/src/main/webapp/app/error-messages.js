/*
 * Copyright (C) 2016 TopCoder Inc., All Rights Reserved.
 *
 * Define error messages.
 * @author bannie2492
 * @version 1.0
 */

var ERROR_MSGS = {
    IMPORT_DATA_EXIST1 : 'Data already present for the date you specified.',
    IMPORT_DATA_EXIST2 : 'Do you want to overwrite the data?',
    IMPORT_FILE_NOT_SPECIFIED: 'You did not specify the file.',
    IMPORT_FILE_NOT_CSV_FILE: 'Only CSV files are accepted.',
    NO_RESULT_FROM_SEARCH : 'There is no result from your search',
    FILTER_MARKET_VALUE_RANGE_NOT_CORRECT : 'Market value range is not correct.',
    EDIT_STATUS_NAME_EMPTY :'Name can not be empty.',
    EDIT_STATUS_DEFAULT_STATUS_MISSING : 'The default security status is missing.',
    EDIT_STATUS_DUPLICATE_NAME : 'Security status names should be unique.',
    EDIT_STATUS_DELETE_WHEN_IN_USE : 'This security status is in use and can not be deleted.',
    GENERAL_ERROR_MESSAGE: 'An error occured, please try again in a few minutes. <br/>If these issues persist please contact your system administrator.',
    BE_IMPORT_FILE_FAILED: 'Failed to import the file you specified.',
    BE_UNAUTHENTICATED: 'You are not authenticated to use the application.',
    BE_NOT_VALID_ROLE: "You don't have a valid role to use the application.",
    BE_ACTION_NOT_ALLOWED: 'According to your role, you are not allowed to use this feature of the application.',
    BE_DB_ERROR: 'Some error occurred to the database, please contact the administrator.',
    BE_BUSINESS_DATE_RANGE_ERROR: 'Start date should not be later than end date',
    BE_IMPORT_NO_VALID_RECORD: 'The file does not contain any valid record.',
    BE_FILE_IMPORT_ERROR: 'The file you specified for import is not a valid file in your system.',
    BE_DATA_LOSS: 'The data you are currently editing has been removed. <br/>Please contact the adminstrator.',
    BE_PAGE_NUMBER_NEGATIVE: 'The page number should not be negative'
};