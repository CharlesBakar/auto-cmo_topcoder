/*
 * Copyright (C) 2016 TopCoder Inc., All Rights Reserved.
 *
 * The directives used for this application.
 * Change log v1.1
 * - fix #21 Tool tip is not cleared
 * @author moulyg, bannie2492
 * @version 1.1
 */

/* global angular, $ */
(function () {
    'use strict';

    var directives = angular.module('CMO.directive', []);


    directives.directive('tooltip', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs, $timeout) {

                $(element).off('mouseenter').on('mouseenter',
                        function () {
                            var tooltip = $('<div></div>').addClass('tool-tip');
                            tooltip.append(attrs.title);
                            var offset = $(element).offset();
                            if (attrs.position === 'right') {
                                tooltip.addClass('right');
                                tooltip.css('top', offset.top);
                                tooltip.css('left', offset.left + 40);
                            } else {
                                tooltip.css('top', offset.top);
                                tooltip.css('left', offset.left - 235);
                            }
                            $('body').append(tooltip);
                        });
                $(element).off('mouseout').on('mouseout',
                        function () {
                            $('.tool-tip').remove();
                        });
                $(element).off('click').on('click',
                        function () {
                            $('.tool-tip').remove();
                            if (attrs.position !== 'right' || $(element).hasClass('ex-clps')) {
                                $(element).trigger('mouseenter');
                            }
                        });
            }
        };
    });

    directives.directive('inlineCalendar', ['BusinessDateService', function () {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                    $(element).datepicker({
                        numberOfMonths: 2,
                        beforeShowDay: function (date) {
                            var time = date.getTime();
                            for (var i = 0; i < scope.businessDates.length; i++) {
                                if (time == scope.businessDates[i]) {
                                    return [true, "yellow", ""];
                                }
                            }
                            var day = date.getDate();
                            return [true, (day < 10 ? "zero" : "")];

                        }, onSelect: function (date) {
                            scope.top.calendardate = date;
                        },
                        maxDate: '+0d',
                        showOtherMonths: true,
                        dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"]
                    });
                    scope.$watch('top.calendardate', function() {
                        element.datepicker('setDate', new Date(scope.top.calendardate));
                    });
                }
            };
        }]);

    directives.directive('datepicker', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {

                $(element).datepicker({
                    beforeShowDay: function (date) {
                        var day = date.getDate();
                        return [true, (day < 10 ? "zero" : "")];

                    },
                    maxDate: '+0d',
                    showOtherMonths: true,
                    dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"]
                });
            }
        };
    });

    directives.directive('pageSelect', function () {
        return {
            restrict: 'A',
            template: '<input type="text" data-numbers-only="" class="select-page" data-ng-model="inputPage">',
            link: function (scope, element, attrs) {
                scope.$watch('currentPage', function (c) {
                    scope.inputPage = c;
                });
            }
        };
    });

    directives.directive('numbersOnly', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attr, ngModelCtrl) {
                function fromUser(text) {
                    if (text) {
                        var transformedInput = text.replace(/[^0-9]/g, '');

                        if (transformedInput !== text) {
                            ngModelCtrl.$setViewValue(transformedInput);
                            ngModelCtrl.$render();
                        }
                        return transformedInput;
                    }
                    return undefined;
                }

                ngModelCtrl.$parsers.push(fromUser);
            }
        };
    });

    directives.directive('decimalOnly', function () {
        return {
            link: function (scope, element, attr) {
                element.keypress(function (event) {
                    var charCode = (event.which) ? event.which : event.keyCode

                    if (
                            (charCode != 45 || $(element).val().indexOf('-') != -1) && // “-” CHECK MINUS, AND ONLY ONE.
                            (charCode != 46 || $(element).val().indexOf('.') != -1) && // “.” CHECK DOT, AND ONLY ONE.
                            (charCode < 48 || charCode > 57))
                        return false;

                    return true;
                });
            }
        };
    });



    directives.directive('loading', ['$http', function ($http)
        {
            return {
                restrict: 'A',
                link: function (scope, elm, attrs)
                {
                    scope.isLoading = function () {
                        return $http.pendingRequests.length > 0;
                    };

                    scope.$watch(scope.isLoading, function (v)
                    {
                        if (v) {
                            elm.show();
                        } else {
                            scope.load = false;
                            elm.hide();
                        }
                    });
                }
            };

        }]);
})();
