/*
 * Copyright (C) 2016 TopCoder Inc., All Rights Reserved.
 *
 * The helper methods of the application.
 * @author bannie2492
 * @version 1.0
 */

var helper = {};

// serialize JSON objects to query strings.
helper.serialize = function(obj) {
    var result = [];
    for (var property in obj) {
        result.push(encodeURIComponent(property) + "=" + encodeURIComponent(obj[property]));
    }
    return result.join("&");
};

// convert the the timestamp to date string
helper.dateToStr = function(timestamp) {
    var d = new Date(timestamp);
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    if (month < 10) {
        month = '0' + month;
    }
    if (day < 10) {
        day = '0' + day;
    }
    return month + '/' + day + '/' + year;
};
